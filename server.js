const express = require('express')
const app = express()

var options = {
    root: __dirname
}

app.get('/', function (req, res) {
    res.sendFile('index.html', options)
})

app.use('/css', express.static('assets/css'))
app.use('/js', express.static('assets/js'))
app.use('/img', express.static('assets/img'))
app.use('/js', express.static('node_modules/jquery/dist'))
app.use('/js', express.static('node_modules/bootstrap-sass/assets/javascripts'))

app.listen(8888, function () {
    console.log('Example app listening on port 8888!')
})