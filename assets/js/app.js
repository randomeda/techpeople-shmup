(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

function EventEmitter() {
  this._events = this._events || {};
  this._maxListeners = this._maxListeners || undefined;
}
module.exports = EventEmitter;

// Backwards-compat with node 0.10.x
EventEmitter.EventEmitter = EventEmitter;

EventEmitter.prototype._events = undefined;
EventEmitter.prototype._maxListeners = undefined;

// By default EventEmitters will print a warning if more than 10 listeners are
// added to it. This is a useful default which helps finding memory leaks.
EventEmitter.defaultMaxListeners = 10;

// Obviously not all Emitters should be limited to 10. This function allows
// that to be increased. Set to zero for unlimited.
EventEmitter.prototype.setMaxListeners = function(n) {
  if (!isNumber(n) || n < 0 || isNaN(n))
    throw TypeError('n must be a positive number');
  this._maxListeners = n;
  return this;
};

EventEmitter.prototype.emit = function(type) {
  var er, handler, len, args, i, listeners;

  if (!this._events)
    this._events = {};

  // If there is no 'error' event listener then throw.
  if (type === 'error') {
    if (!this._events.error ||
        (isObject(this._events.error) && !this._events.error.length)) {
      er = arguments[1];
      if (er instanceof Error) {
        throw er; // Unhandled 'error' event
      } else {
        // At least give some kind of context to the user
        var err = new Error('Uncaught, unspecified "error" event. (' + er + ')');
        err.context = er;
        throw err;
      }
    }
  }

  handler = this._events[type];

  if (isUndefined(handler))
    return false;

  if (isFunction(handler)) {
    switch (arguments.length) {
      // fast cases
      case 1:
        handler.call(this);
        break;
      case 2:
        handler.call(this, arguments[1]);
        break;
      case 3:
        handler.call(this, arguments[1], arguments[2]);
        break;
      // slower
      default:
        args = Array.prototype.slice.call(arguments, 1);
        handler.apply(this, args);
    }
  } else if (isObject(handler)) {
    args = Array.prototype.slice.call(arguments, 1);
    listeners = handler.slice();
    len = listeners.length;
    for (i = 0; i < len; i++)
      listeners[i].apply(this, args);
  }

  return true;
};

EventEmitter.prototype.addListener = function(type, listener) {
  var m;

  if (!isFunction(listener))
    throw TypeError('listener must be a function');

  if (!this._events)
    this._events = {};

  // To avoid recursion in the case that type === "newListener"! Before
  // adding it to the listeners, first emit "newListener".
  if (this._events.newListener)
    this.emit('newListener', type,
              isFunction(listener.listener) ?
              listener.listener : listener);

  if (!this._events[type])
    // Optimize the case of one listener. Don't need the extra array object.
    this._events[type] = listener;
  else if (isObject(this._events[type]))
    // If we've already got an array, just append.
    this._events[type].push(listener);
  else
    // Adding the second element, need to change to array.
    this._events[type] = [this._events[type], listener];

  // Check for listener leak
  if (isObject(this._events[type]) && !this._events[type].warned) {
    if (!isUndefined(this._maxListeners)) {
      m = this._maxListeners;
    } else {
      m = EventEmitter.defaultMaxListeners;
    }

    if (m && m > 0 && this._events[type].length > m) {
      this._events[type].warned = true;
      console.error('(node) warning: possible EventEmitter memory ' +
                    'leak detected. %d listeners added. ' +
                    'Use emitter.setMaxListeners() to increase limit.',
                    this._events[type].length);
      if (typeof console.trace === 'function') {
        // not supported in IE 10
        console.trace();
      }
    }
  }

  return this;
};

EventEmitter.prototype.on = EventEmitter.prototype.addListener;

EventEmitter.prototype.once = function(type, listener) {
  if (!isFunction(listener))
    throw TypeError('listener must be a function');

  var fired = false;

  function g() {
    this.removeListener(type, g);

    if (!fired) {
      fired = true;
      listener.apply(this, arguments);
    }
  }

  g.listener = listener;
  this.on(type, g);

  return this;
};

// emits a 'removeListener' event iff the listener was removed
EventEmitter.prototype.removeListener = function(type, listener) {
  var list, position, length, i;

  if (!isFunction(listener))
    throw TypeError('listener must be a function');

  if (!this._events || !this._events[type])
    return this;

  list = this._events[type];
  length = list.length;
  position = -1;

  if (list === listener ||
      (isFunction(list.listener) && list.listener === listener)) {
    delete this._events[type];
    if (this._events.removeListener)
      this.emit('removeListener', type, listener);

  } else if (isObject(list)) {
    for (i = length; i-- > 0;) {
      if (list[i] === listener ||
          (list[i].listener && list[i].listener === listener)) {
        position = i;
        break;
      }
    }

    if (position < 0)
      return this;

    if (list.length === 1) {
      list.length = 0;
      delete this._events[type];
    } else {
      list.splice(position, 1);
    }

    if (this._events.removeListener)
      this.emit('removeListener', type, listener);
  }

  return this;
};

EventEmitter.prototype.removeAllListeners = function(type) {
  var key, listeners;

  if (!this._events)
    return this;

  // not listening for removeListener, no need to emit
  if (!this._events.removeListener) {
    if (arguments.length === 0)
      this._events = {};
    else if (this._events[type])
      delete this._events[type];
    return this;
  }

  // emit removeListener for all listeners on all events
  if (arguments.length === 0) {
    for (key in this._events) {
      if (key === 'removeListener') continue;
      this.removeAllListeners(key);
    }
    this.removeAllListeners('removeListener');
    this._events = {};
    return this;
  }

  listeners = this._events[type];

  if (isFunction(listeners)) {
    this.removeListener(type, listeners);
  } else if (listeners) {
    // LIFO order
    while (listeners.length)
      this.removeListener(type, listeners[listeners.length - 1]);
  }
  delete this._events[type];

  return this;
};

EventEmitter.prototype.listeners = function(type) {
  var ret;
  if (!this._events || !this._events[type])
    ret = [];
  else if (isFunction(this._events[type]))
    ret = [this._events[type]];
  else
    ret = this._events[type].slice();
  return ret;
};

EventEmitter.prototype.listenerCount = function(type) {
  if (this._events) {
    var evlistener = this._events[type];

    if (isFunction(evlistener))
      return 1;
    else if (evlistener)
      return evlistener.length;
  }
  return 0;
};

EventEmitter.listenerCount = function(emitter, type) {
  return emitter.listenerCount(type);
};

function isFunction(arg) {
  return typeof arg === 'function';
}

function isNumber(arg) {
  return typeof arg === 'number';
}

function isObject(arg) {
  return typeof arg === 'object' && arg !== null;
}

function isUndefined(arg) {
  return arg === void 0;
}

},{}],2:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Ticker_1 = require("./Ticker");
const Easings_1 = require("./Easings");
const events_1 = require("events");
/**
 * aaaa
 */
class Animation extends events_1.EventEmitter {
    /**
     * Animates object properties over time. Can loop if necessary.
     * @param duration duration of the animation in seconds
     * @param target target object whos properties will be animated
     * @param to property values the target's properties will be animated to
     * @param config additional configuration.
     * loop - loops the animation.
     * from - specifies starting properties. if not specified, the current property values will be used instead.
     * easing - easing funciton
     */
    constructor(duration, target, to, config = {}) {
        super();
        this.duration = duration;
        this.target = target;
        this.to = to;
        this.config = config;
        this._loop = false;
        this._easing = Easings_1.Easings.linear;
        this._updateProxy = () => this.update();
        this.reset();
        this._easing = config.easing || Easings_1.Easings.linear;
        this._duration = this.duration * 1000;
        this._loop = config.loop;
    }
    /**
     * Resets starting time and properties to the original starting values. If no from object is given, the current
     * values will be used to create the from object.
     */
    reset() {
        if (this.config.from) {
            this.from = this.config.from;
            for (const prop of Object.keys(this.from)) {
                if (typeof this.from[prop] === 'number') {
                    this.target[prop] = this.from[prop];
                }
            }
        }
        else {
            this.from = {};
            for (const prop of Object.keys(this.to)) {
                if (typeof this.target[prop] === 'number') {
                    this.from[prop] = this.target[prop];
                }
            }
        }
        this._startTime = Date.now();
    }
    play() {
        this.reset();
        Ticker_1.Ticker.on(Ticker_1.Events.UPDATE, this._updateProxy);
    }
    stop() {
        Ticker_1.Ticker.removeListener(Ticker_1.Events.UPDATE, this._updateProxy);
    }
    update() {
        let currentTime = Date.now() - this._startTime;
        for (const prop of Object.keys(this.to)) {
            this.target[prop] = this.from[prop] + (this.to[prop] - this.from[prop]) * this._easing(Math.min(currentTime / this._duration, 1));
        }
        if (currentTime >= this._duration) {
            if (this._loop) {
                this.reset();
            }
            else {
                this.stop();
                this.emit(Ticker_1.Events.COMPLETE);
            }
        }
    }
}
exports.default = Animation;
},{"./Easings":9,"./Ticker":27,"events":1}],3:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const GameObject_1 = require("./GameObject");
const GraphicImage_1 = require("./GraphicImage");
const Enemy_1 = require("./Enemy");
class Bullet extends GameObject_1.default {
    /**
     * Creates a bullet which travel along the x axis.
     * @param preloader Used to get the bullet image
     * @param player Used to get starting coordinates
     */
    constructor(preloader, player) {
        super();
        this.preloader = preloader;
        this.player = player;
        this.speed = 30;
        this.image = new GraphicImage_1.default(preloader.getByName('bullet').bitmap);
        this.bounds = this.image.bounds;
        this.addChild(this.image);
    }
    onCollision(go) {
        if (go instanceof Enemy_1.default) {
            this.destroy();
        }
    }
    reset() {
        this.opacity = 0;
        this.active = false;
        super.reset();
    }
    start() {
        this.opacity = 1;
        this.x = this.player.x;
        this.y = this.player.y;
        if (this.active)
            return;
        this.active = true;
        super.start();
    }
    update() {
        if (!this.active)
            return;
        this.x += this.speed;
        if (this.x > this.stage.bounds.width) {
            this.destroy();
        }
        super.update();
    }
    destroy() {
        this.reset();
        super.destroy();
    }
}
exports.default = Bullet;
},{"./Enemy":10,"./GameObject":14,"./GraphicImage":17}],4:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Rect_1 = require("./Rect");
const Text_1 = require("./Text");
const Container_1 = require("./Container");
const Ticker_1 = require("./Ticker");
class Button extends Container_1.default {
    /**
     * Creates a button with the given text
     * @param text Button text
     */
    constructor(text) {
        super();
        this.text = text;
        // adding button elements
        this._text = new Text_1.default(this.text, '#fff', '16px Orbitron', 'center');
        this._text.x = 0;
        this._text.y = 7;
        this._rect = new Rect_1.default('#000', 250, this._text.bounds.height + 15);
        this._rect.stroke = '#fff';
        this._rect.x = -this._rect.bounds.width / 2;
        this._rect.y = -this._rect.bounds.height / 2;
        this._rect.opacity = 0.3;
        this.addChild(this._rect);
        this.addChild(this._text);
        // mouse events
        this._rect.on(Ticker_1.Events.MOUSEENTER, (event) => { this.onMouseEnter(event); });
        this._rect.on(Ticker_1.Events.MOUSELEAVE, (event) => { this.onMouseLeave(event); });
        this._rect.on(Ticker_1.Events.MOUSEDOWN, (event) => { this.onMouseDown(event); });
        this._rect.on(Ticker_1.Events.MOUSEUP, (event) => { this.onMouseUp(); });
        this._rect.on(Ticker_1.Events.CLICK, (event) => { this.onClick(event); });
    }
    onMouseEnter(event) {
        this._rect.color = '#4286f4';
        this._rect.opacity = 0.5;
    }
    onMouseLeave(event) {
        this._rect.color = '#000';
        this._rect.opacity = 0.3;
    }
    onMouseDown(event) {
        this._rect.color = '#2e63ba';
    }
    onMouseUp() {
        this._rect.color = '#4286f4';
    }
    onClick(event) {
        this.emit(Ticker_1.Events.CLICK, event);
    }
    destroy() {
        this._rect.removeAllListeners(Ticker_1.Events.MOUSEENTER);
        this._rect.removeAllListeners(Ticker_1.Events.MOUSELEAVE);
        this.remove();
    }
}
exports.default = Button;
},{"./Container":7,"./Rect":22,"./Text":26,"./Ticker":27}],5:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const CollisionGrid_1 = require("./CollisionGrid");
const GameObject_1 = require("./GameObject");
const Ticker_1 = require("./Ticker");
class CollisionDetector {
    /**
     * Collision detection is costly for large number of objects. This grid based detection
     * will cause less calculation because objects will only be calculated if they are in
     * the same part of the screen. The size of these parts are determined by the size and
     * levels parameters.
     * @param levels number of grids. every higher level grid will have twice as alrge cells as the lower lever grid.
     * @param size base grid cell size in pixels. the first level will have this cell size. the second level will have twice a large, etc.
     * @param container Container object which the grid is applied to.
     */
    constructor(levels, size = 150, container) {
        this.levels = levels;
        this.size = size;
        this.container = container;
        this.grids = []; // instantiated grids placed here
        // create grids
        for (let i = 0; i < levels; i++) {
            // create grid
            let parent = i > 0 ? this.grids[i - 1] : null;
            this.grids.push(new CollisionGrid_1.default(size * Math.pow(2, i), parent));
        }
    }
    /**
     * Calculates objects to the grids, then checks for collisions.
     */
    update() {
        this.clear();
        let objects = this.container.children;
        for (let i = 0; i < objects.length; i++) {
            let go;
            go = objects[i];
            this.calculate(go);
        }
        this.checkCollisions();
    }
    /**
     * Calculates the corners of the object and registers it to the appropriate grid cells;
     * @param go object to be calculated
     */
    calculate(go) {
        let bounds = go.bounds;
        let currentGrid = this.grids[0];
        let size = this.grids[0].size;
        // CALCULATE GRID LEVEL
        // if grater than current grid, we switch to a greater grid (if exists)
        while ((bounds.width > size || bounds.height > size) && currentGrid.parent) {
            currentGrid = currentGrid.parent;
            size = currentGrid.size;
        }
        go.grid = currentGrid;
        // CALCULATE CELLS IN LEVEL GRID
        let left = Math.floor(go.x / currentGrid.size); // divide by grid size to make grid cells
        let right = Math.floor((go.x + bounds.width) / currentGrid.size);
        let top = Math.floor(go.y / currentGrid.size);
        let bottom = Math.floor((go.y + bounds.height) / currentGrid.size);
        let topLeft = { x: left, y: top };
        let topRight = { x: right, y: top };
        let bottomLeft = { x: left, y: bottom };
        let bottomRight = { x: right, y: bottom };
        go.positions = [];
        this.pushIfNotExists(go.positions, topLeft);
        this.pushIfNotExists(go.positions, topRight);
        this.pushIfNotExists(go.positions, bottomLeft);
        this.pushIfNotExists(go.positions, bottomRight);
        // PUT GameObject TO GRID
        currentGrid.addToCells(go);
    }
    /**
     * Puts the value in the array only if it does not exist.
     * This makes sure that if two or more corners of the bounding box are in the same grid cell,
     * then the object is only registered once in the cell, not twice or more.
     * @param array
     * @param value
     */
    pushIfNotExists(array, value) {
        for (let i = 0; i < array.length; i++) {
            if (array[i].x === value.x && array[i].y === value.y) {
                return;
            }
        }
        array.push(value);
    }
    /**
     * Clear all grids of objects
     */
    clear() {
        let currentGrid = this.grids[0];
        currentGrid.clear();
        while (currentGrid.parent) {
            currentGrid = currentGrid.parent;
            currentGrid.clear();
        }
    }
    /**
     * Checks collisions for all objects
     */
    checkCollisions() {
        let children = this.container.children;
        for (let go of this.container.children) {
            if (go instanceof GameObject_1.default) {
                for (let position of go.positions) {
                    for (let mayCollideWith of go.grid.getObjectsInCell(position)) {
                        if (mayCollideWith.constructor !== go.constructor) {
                            this.testCollision(go, mayCollideWith);
                        }
                    }
                }
            }
        }
    }
    /**
     * Tests if the two object's bounding box are overlapping
     * @param go1 One of the objects who's bounding box will be tested
     * @param go2 The other object which is tested
     */
    testCollision(go1, go2) {
        if (go2.x + go2.bounds.width >= go1.x &&
            go2.x <= go1.x + go1.bounds.width &&
            go2.y + go2.bounds.height >= go1.y &&
            go2.y <= go1.y + go1.bounds.height // go2 top less than go1 bottom
        ) {
            go2.emit(Ticker_1.Events.COLLISION, go1);
            go1.emit(Ticker_1.Events.COLLISION, go2);
        }
    }
}
exports.default = CollisionDetector;
},{"./CollisionGrid":6,"./GameObject":14,"./Ticker":27}],6:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Grid {
    /**
     * The grid is responsible for storing the objects in cells. Cells
     * represen bigger parts of the screen. If the objects are in the same cell,
     * their collisions will be calculated.
     * @param size size of the grid in pixels
     * @param parent if a higher level grid exists, this is it.
     */
    constructor(size, parent) {
        this.size = size;
        this.parent = parent;
        this.cells = [];
        //init
        this.clear();
    }
    /**
     * Calculates cell positions for the given object and stores it in the cells.
     * @param go
     */
    addToCells(go) {
        let positions = go.positions;
        for (let i = 0; i < positions.length; i++) {
            if (!this.cells[positions[i].x]) {
                this.cells[positions[i].x] = [];
            }
            if (!this.cells[positions[i].x][positions[i].y]) {
                this.cells[positions[i].x][positions[i].y] = [];
            }
            this.cells[positions[i].x][positions[i].y].push(go);
        }
        if (this.parent) {
            // add to parent cells too
            this.parent.addToCells(go);
        }
    }
    /**
     * Deletes all objects from the grid.
     */
    clear() {
        this.cells = [];
    }
    /**
     * Returns all the objects that are in the given cell.
     * @param pos gird cell coordinates
     */
    getObjectsInCell(pos) {
        let objects = [];
        if (this.parent) {
            let parentPos = { x: Math.floor(pos.x / 2), y: Math.floor(pos.y / 2) };
            objects = objects.concat(this.parent.getObjectsInCell(parentPos));
        }
        if (this.cells[pos.x] && this.cells[pos.x][pos.y]) {
            return objects.concat(this.cells[pos.x][pos.y]);
        }
        else {
            return [];
        }
    }
}
exports.default = Grid;
},{}],7:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const DisplayObject_1 = require("./DisplayObject");
class Container extends DisplayObject_1.default {
    constructor() {
        super(...arguments);
        this.children = [];
    }
    /**
     * Adds the object to the container. will not add the object twice.
     * @param displayObject object to be added
     */
    addChild(displayObject) {
        if (!this.children.find(child => child === displayObject)) {
            displayObject.parent = this;
            this.children.push(displayObject);
        }
    }
    /**
     * Removes the object from the container if exists
     * @param child object to be removed
     */
    removeChild(child) {
        child.parent = undefined;
        child.ctx = undefined;
        child.stage = undefined;
        this.children.splice(this.children.indexOf(child), 1);
    }
    /**
     * Updates all children
     */
    update() {
        if (!this.visible)
            return;
        this.children.forEach(gameObject => {
            if (gameObject.visible) {
                gameObject.update();
            }
        });
    }
    /**
     * Mouse events are passed down to all children for calculation
     * @param mouse mouse position
     */
    _onMouseDown(mouse) {
        super._onMouseDown(mouse);
        this.children.forEach(child => child._onMouseDown(mouse));
    }
    /**
     * Mouse events are passed down to all children for calculation
     */
    _onMouseUp() {
        super._onMouseUp();
        this.children.forEach(child => child._onMouseUp());
    }
    /**
     * Mouse events are passed down to all children for calculation
     * @param mouse mouse position
     */
    _onClick(mouse) {
        super._onClick(mouse);
        this.children.forEach(child => child._onClick(mouse));
    }
    /**
     * Mouse events are passed down to all children for calculation
     * @param mouse mouse position
     */
    _onMouseMove(mouse) {
        super._onMouseMove(mouse);
        this.children.forEach(child => child._onMouseMove(mouse));
    }
}
exports.default = Container;
},{"./DisplayObject":8}],8:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const events_1 = require("events");
const Ticker_1 = require("./Ticker");
class DisplayObject extends events_1.EventEmitter {
    constructor() {
        super(...arguments);
        this.localX = 0; // coordinates relative to the parent
        this.localY = 0; // coordinates relative to the parent
        this.localOpacity = 1; // own opacity value
        this.bounds = { width: 0, height: 0 };
        this.visible = true;
        this._mouseover = false;
        this._mousedown = false;
    }
    /**
     * Checks if pointer is within the bounds of this object
     * @param mosue pointer position
     */
    checkPointerPosition(mosue) {
        return mosue.x > this.x
            && mosue.x < this.x + this.bounds.width
            && mosue.y > this.y
            && mosue.y < this.y + this.bounds.height;
    }
    _onMouseDown(mouse) {
        if (!this._mousedown && this.checkPointerPosition(mouse)) {
            //pointer over gameobject
            this._mousedown = true;
            this.emit(Ticker_1.Events.MOUSEDOWN, mouse);
        }
    }
    _onMouseUp() {
        if (this._mousedown) {
            this.emit(Ticker_1.Events.MOUSEUP, event);
            this._mousedown = false;
        }
    }
    _onClick(mouse) {
        if (this.checkPointerPosition(mouse)) {
            //pointer over gameobject
            this.emit(Ticker_1.Events.CLICK, mouse);
        }
    }
    _onMouseMove(mouse) {
        if (this.checkPointerPosition(mouse)) {
            //pointer over gameobject
            this.emit(Ticker_1.Events.MOUSEMOVE, mouse);
            if (!this._mouseover) {
                this.emit(Ticker_1.Events.MOUSEENTER, mouse);
                this._mouseover = true;
            }
        }
        else {
            //pointer left gameobject
            if (this._mouseover) {
                this.emit(Ticker_1.Events.MOUSELEAVE, mouse);
                this._mouseover = false;
            }
        }
    }
    /**
     * Runs in every animation frame
     */
    update() {
        // override in child class
    }
    /**
     * Sends object to the top of all objects.
     */
    sendToFront() {
        let parent = this.parent;
        if (parent) {
            parent.removeChild(this);
            parent.addChild(this);
        }
    }
    /**
     * Removes this object from its container, so it wont be visible,
     * and makes it ready for garbage collection.
     */
    remove() {
        this.parent.removeChild(this);
    }
    /**
     * Gets the stage recursively from the topmost parent
     */
    get stage() {
        if (!this._stage && this.parent) {
            this._stage = this.parent.stage;
        }
        return this._stage;
    }
    set stage(stage) {
        this._stage = stage;
    }
    /**
     * final opacity is multiplied by all parents' opacities
     */
    get opacity() {
        if (this.parent) {
            return this.localOpacity * this.parent.opacity;
        }
        return this.localOpacity;
    }
    set opacity(opacity) {
        this.localOpacity = opacity;
    }
    /**
     * Final coordinates are added from all parents' coordinates
     */
    get x() {
        if (this.parent) {
            return this.localX + this.parent.x;
        }
        return this.localX;
    }
    set x(x) {
        this.localX = x;
    }
    /**
     * Final coordinates are added from all parents' coordinates
     */
    get y() {
        if (this.parent) {
            return this.localY + this.parent.y;
        }
        return this.localY;
    }
    set y(y) {
        this.localY = y;
    }
    /**
     * Rendering context is searched recursively in the parent chain until found.
     */
    get ctx() {
        if (!this._ctx && this.parent) {
            this._ctx = this.parent.ctx;
        }
        return this._ctx;
    }
    set ctx(ctx) {
        this._ctx = ctx;
    }
}
exports.default = DisplayObject;
},{"./Ticker":27,"events":1}],9:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Easing functions for animation purposes
 */
exports.Easings = {
    sin: function (t) { return Math.sin(Math.PI * 2 * t); },
    easeInSin: function (t) { return 1 + Math.sin(Math.PI / 2 * t - Math.PI / 2); },
    easeOutSin: function (t) { return Math.sin(Math.PI / 2 * t); },
    easeInOutSin: function (t) { return (1 + Math.sin(Math.PI * t - Math.PI / 2)) / 2; },
    // no easing, no acceleration
    linear: function (t) { return t; },
    // accelerating from zero velocity
    easeInQuad: function (t) { return t * t; },
    // decelerating to zero velocity
    easeOutQuad: function (t) { return t * (2 - t); },
    // acceleration until halfway, then deceleration
    easeInOutQuad: function (t) { return t < .5 ? 2 * t * t : -1 + (4 - 2 * t) * t; },
    // accelerating from zero velocity 
    easeInCubic: function (t) { return t * t * t; },
    // decelerating to zero velocity 
    easeOutCubic: function (t) { return (--t) * t * t + 1; },
    // acceleration until halfway, then deceleration 
    easeInOutCubic: function (t) { return t < .5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1; },
    // accelerating from zero velocity 
    easeInQuart: function (t) { return t * t * t * t; },
    // decelerating to zero velocity 
    easeOutQuart: function (t) { return 1 - (--t) * t * t * t; },
    // acceleration until halfway, then deceleration
    easeInOutQuart: function (t) { return t < .5 ? 8 * t * t * t * t : 1 - 8 * (--t) * t * t * t; },
    // accelerating from zero velocity
    easeInQuint: function (t) { return t * t * t * t * t; },
    // decelerating to zero velocity
    easeOutQuint: function (t) { return 1 + (--t) * t * t * t * t; },
    // acceleration until halfway, then deceleration 
    easeInOutQuint: function (t) { return t < .5 ? 16 * t * t * t * t * t : 1 + 16 * (--t) * t * t * t * t; }
};
},{}],10:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const GraphicImage_1 = require("./GraphicImage");
const GameObject_1 = require("./GameObject");
const Ticker_1 = require("./Ticker");
const Animation_1 = require("./Animation");
const Easings_1 = require("./Easings");
const Player_1 = require("./Player");
const Bullets_1 = require("./Bullets");
class Enemy extends GameObject_1.default {
    /**
     * Creates a randomly moving enemy
     * @param explosion Pool used to instantiate explosions
     * @param preloader used to get images
     */
    constructor(explosion, preloader) {
        super();
        this.explosion = explosion;
        this.preloader = preloader;
        this.active = false;
        this.moveY = 100;
        this._originalY = Math.random() * 500 + 50;
        this._originalX = 800;
        // set up visuals
        this.image = new GraphicImage_1.default(this.preloader.getByName('enemy').bitmap);
        this.image.x = 0;
        this.image.y = 0;
        this.addChild(this.image);
        this.bounds = this.image.bounds;
        // set starting point
        this.x = this._originalX;
        this.y = this._originalY;
        // moves in sine motion vertically
        this._moveYAnim = new Animation_1.default(2, this, { moveY: 100 }, { from: { moveY: 0 }, easing: Easings_1.Easings.sin, loop: true });
        // moves linearly from right to left
        this._moveXAnim = new Animation_1.default(6, this, { x: -this.bounds.width }, { from: { x: this._originalX }, easing: Easings_1.Easings.linear });
        this._moveXAnim.on(Ticker_1.Events.COMPLETE, () => this.reset());
    }
    onCollision(go) {
        if (go instanceof Bullets_1.default || go instanceof Player_1.default) {
            this.destroy();
        }
    }
    reset() {
        this.removeEvents();
        this.active = false;
        this.x = this._originalX;
        super.reset();
    }
    start() {
        if (this.active)
            return;
        this.addEvents();
        this.active = true;
        this._originalY = Math.random() * 500 + 50;
        super.start();
    }
    addEvents() {
        this._moveYAnim.play();
        this._moveXAnim.play();
    }
    removeEvents() {
        this._moveYAnim.stop();
        this._moveXAnim.stop();
    }
    update() {
        if (!this.active) {
            return;
        }
        this.y = this._originalY + this.moveY;
        super.update();
    }
    explode() {
        this.explosion.activateOne(this.x + this.bounds.width / 2, this.y + this.bounds.height / 2);
    }
    destroy() {
        this.explode();
        this.reset();
        super.destroy();
    }
}
exports.default = Enemy;
},{"./Animation":2,"./Bullets":3,"./Easings":9,"./GameObject":14,"./GraphicImage":17,"./Player":19,"./Ticker":27}],11:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const DisplayObject_1 = require("./DisplayObject");
const Animation_1 = require("./Animation");
const Ticker_1 = require("./Ticker");
class Explosion extends DisplayObject_1.default {
    /**
     * Creates an animated explosion. A white cricle will grow and fade out quickly.
     * @param _container explosions are added to this container
     */
    constructor(_container) {
        super();
        this._container = _container;
        this.active = true;
        this._startRadius = 20;
        this._endRadius = 150;
        this._startLineWidth = 20;
        this._endLineWidth = 0;
        this._anim = new Animation_1.default(0.2, this, { lineWidth: this._endLineWidth, radius: this._endRadius, opacity: 0 }, { from: { lineWidth: this._startLineWidth, radius: this._startRadius, opacity: 1 } });
        this._anim.on(Ticker_1.Events.COMPLETE, () => this.reset());
        this._container.addChild(this);
    }
    reset() {
        this.active = false;
        this._anim.stop();
    }
    /**
     * Plays the explosion at the given coordinates
     * @param x starting position
     * @param y starting position
     */
    start(x, y) {
        this.x = x;
        this.y = y;
        this.active = true;
        this._anim.play();
    }
    destroy() {
        this._anim.removeAllListeners();
        this._anim.stop();
        this.remove();
    }
    update() {
        this.ctx.beginPath();
        this.ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI, false);
        this.ctx.lineWidth = this.lineWidth;
        this.ctx.strokeStyle = '#fff';
        this.ctx.globalAlpha = this.opacity;
        this.ctx.stroke();
    }
}
exports.default = Explosion;
},{"./Animation":2,"./DisplayObject":8,"./Ticker":27}],12:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Rect_1 = require("./Rect");
const Animation_1 = require("./Animation");
const Ticker_1 = require("./Ticker");
const Easings_1 = require("./Easings");
class FadeScreen extends Rect_1.default {
    /**
     * Creates a black screen which is used to cover the canvas to create a fade in / fade out effect.
     * Will emit FADE_OUT and FADE_IN events when the animation is finished
     * @param stage
     */
    constructor(stage) {
        super('#000', stage.bounds.width, stage.bounds.height);
        this.stage = stage;
        this._onFadeOutProxy = () => {
            this.emit(Ticker_1.Events.FADE_OUT);
        };
        this._onFadeInProxy = () => {
            this.emit(Ticker_1.Events.FADE_IN);
        };
        this._fadeOutAnim = new Animation_1.default(0.7, this, { localOpacity: 0 }, { easing: Easings_1.Easings.easeInQuad });
        this._fadeInAnim = new Animation_1.default(0.7, this, { localOpacity: 1 }, { easing: Easings_1.Easings.easeOutQuad });
        this._fadeOutAnim.on(Ticker_1.Events.COMPLETE, this._onFadeOutProxy);
        this._fadeInAnim.on(Ticker_1.Events.COMPLETE, this._onFadeInProxy);
    }
    fadeIn() {
        if (this.opacity === 1) {
            this._onFadeInProxy();
            return;
        }
        this.sendToFront();
        this._fadeInAnim.play();
    }
    fadeOut() {
        if (this.opacity === 0) {
            this._onFadeInProxy();
            return;
        }
        this.sendToFront();
        this._fadeOutAnim.play();
    }
    stop() {
        this._fadeOutAnim.stop();
        this._fadeInAnim.stop();
    }
    destroy() {
        this.stop();
        this._fadeOutAnim.removeAllListeners();
        this._fadeInAnim.removeAllListeners();
        super.remove();
    }
}
exports.default = FadeScreen;
},{"./Animation":2,"./Easings":9,"./Rect":22,"./Ticker":27}],13:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Stage_1 = require("./Stage");
const Preloader_1 = require("./Preloader");
const SplashScreen_1 = require("./SplashScreen");
const MainScreen_1 = require("./MainScreen");
const Ticker_1 = require("./Ticker");
const GameScreen_1 = require("./GameScreen");
const GameOverScreen_1 = require("./GameOverScreen");
const FadeScreen_1 = require("./FadeScreen");
const StateMachine_1 = require("./StateMachine");
class GameController {
    /**
     * Loads all images and manages screens with a state machine.
     * @param canvas canvas
     */
    constructor(canvas) {
        this.canvas = canvas;
        //preload images
        let preloader = new Preloader_1.default([
            { name: 'splash', src: '/splash.jpg' },
            { name: 'main', src: '/main.jpg' },
            { name: 'logo', src: '/logo.png' },
            { name: 'blue comet', src: '/blue.png' },
            { name: 'purple comet', src: '/purple.png' },
            { name: 'stars', src: '/stars.png' },
            { name: 'nebula', src: '/nebula_darkblue.png' },
            { name: 'ship', src: '/ship.png' },
            { name: 'enemy', src: '/enemy_sm.png' },
            { name: 'bullet', src: '/bullet.png' },
        ], '/img');
        preloader.on(Ticker_1.Events.COMPLETE, () => {
            let stage = new Stage_1.default(canvas);
            Ticker_1.Ticker.on(Ticker_1.Events.UPDATE, () => stage.update());
            this.splash = new SplashScreen_1.default(stage, preloader);
            this.main = new MainScreen_1.default(stage, preloader);
            this.game = new GameScreen_1.default(stage, preloader);
            this.gameOver = new GameOverScreen_1.default(stage);
            this.fader = new FadeScreen_1.default(stage);
            this.splash.visible = false;
            this.main.visible = false;
            this.game.visible = false;
            this.gameOver.visible = false;
            this.fader.visible = true;
            stage.addChild(this.splash);
            stage.addChild(this.main);
            stage.addChild(this.game);
            stage.addChild(this.gameOver);
            stage.addChild(this.fader);
            this.start();
        });
    }
    start() {
        let states = {
            black: {
                next: {
                    beforeFadeIn: () => {
                        this.splash.visible = true;
                    },
                    delay: 2,
                    to: 'splash'
                }
            },
            splash: {
                next: {
                    beforeFadeIn: () => {
                        this.splash.visible = false;
                        this.main.show();
                    },
                    to: 'main'
                }
            },
            main: {
                loadGame: {
                    beforeFadeIn: () => {
                        this.main.hide();
                        this.game.show();
                    },
                    to: 'game'
                },
                exit: {
                    beforeFadeIn: () => {
                        this.main.hide();
                        this.splash.visible = true;
                    },
                    delay: 2,
                    to: 'splash'
                }
            },
            game: {
                gameOver: {
                    beforeFadeIn: () => {
                        this.game.hide();
                        this.gameOver.visible = true;
                    },
                    delay: 2,
                    to: 'gameOver'
                }
            },
            gameOver: {
                next: {
                    beforeFadeIn: () => {
                        this.gameOver.visible = false;
                        this.splash.visible = true;
                    },
                    delay: 2,
                    to: 'splash'
                }
            }
        };
        // set up transition events
        this.sm = new StateMachine_1.default(this, states, 'black');
        this.sm.transition('next'); // first transition
        this.sm.on(Ticker_1.Events.COMPLETE, () => this.sm.transition('next')); // general transition between scenes
        this.main.on(Ticker_1.Events.GAME_START, () => this.sm.transition('loadGame')); // clicked on a game button
        this.main.on(Ticker_1.Events.EXIT, () => this.sm.transition('exit')); // clicked on exit button
        this.game.on(Ticker_1.Events.GAME_OVER, () => this.sm.transition('gameOver')); // player died
    }
}
exports.default = GameController;
},{"./FadeScreen":12,"./GameOverScreen":15,"./GameScreen":16,"./MainScreen":18,"./Preloader":21,"./SplashScreen":23,"./Stage":24,"./StateMachine":25,"./Ticker":27}],14:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Container_1 = require("./Container");
const Ticker_1 = require("./Ticker");
/**
 * Base class for in-game objects. Turns collision detection on / off automatically.
 */
class GameObject extends Container_1.default {
    constructor() {
        super(...arguments);
        this._onCollisionProxy = (go) => {
            this.onCollision(go);
        };
    }
    onCollision(go) { }
    start() {
        this.on(Ticker_1.Events.COLLISION, this._onCollisionProxy);
    }
    reset() {
        this.removeListener(Ticker_1.Events.COLLISION, this._onCollisionProxy);
    }
    destroy() {
        this.parent.removeChild(this);
    }
}
exports.default = GameObject;
},{"./Container":7,"./Ticker":27}],15:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Text_1 = require("./Text");
const Container_1 = require("./Container");
class GameOverScreen extends Container_1.default {
    /**
     * Simple text in the center of the screen
     * @param stage stage
     */
    constructor(stage) {
        super();
        this.stage = stage;
        let text = new Text_1.default('GAME OVER', '#fff', '32px Orbitron', 'center');
        text.x = this.stage.canvas.width / 2;
        text.y = this.stage.canvas.height / 2;
        this.addChild(text);
    }
}
exports.default = GameOverScreen;
},{"./Container":7,"./Text":26}],16:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Container_1 = require("./Container");
const GraphicImage_1 = require("./GraphicImage");
const Ticker_1 = require("./Ticker");
const Player_1 = require("./Player");
const Enemy_1 = require("./Enemy");
const CollisionDetector_1 = require("./CollisionDetector");
const Pool_1 = require("./Pool");
const Explosion_1 = require("./Explosion");
const Bullets_1 = require("./Bullets");
class GameScreen extends Container_1.default {
    /**
     * Loads the game and controls the game mechanics.
     * Manages Bullets, Enemies and the Player object.
     * Moves the parallax background.
     * @param stage stage
     * @param preloader used to get loaded images
     */
    constructor(stage, preloader) {
        super();
        this.stage = stage;
        this.preloader = preloader;
        this.spawnTime = 1;
        this._fireTime = 0.15;
        // init container for explosions and backgrounds (we dont want to include them in collision detection)
        this.explosionContainer = new Container_1.default();
        this.starsContainer = new Container_1.default();
        this.nebulaContainer = new Container_1.default();
        this.hitContainer = new Container_1.default();
        this.nebulaContainer.opacity = 0.9;
        // craete pools
        this.explosionPool = new Pool_1.default(Explosion_1.default, this.explosionContainer);
        this.enemyPool = new Pool_1.default(Enemy_1.default, this.explosionPool, this.preloader);
        // init collision detection
        this.collisions = new CollisionDetector_1.default(1, 200, this.hitContainer);
        // init parallax backgrounds
        let stars1 = new GraphicImage_1.default(this.preloader.getByName('stars').bitmap);
        let stars2 = new GraphicImage_1.default(this.preloader.getByName('stars').bitmap);
        stars2.x = this.stage.bounds.width - 1;
        let nebula1 = new GraphicImage_1.default(this.preloader.getByName('nebula').bitmap);
        let nebula2 = new GraphicImage_1.default(this.preloader.getByName('nebula').bitmap);
        nebula2.x = this.stage.bounds.width - 1;
        this.starsContainer.addChild(stars1);
        this.starsContainer.addChild(stars2);
        this.nebulaContainer.addChild(nebula1);
        this.nebulaContainer.addChild(nebula2);
        // init player
        this.player = new Player_1.default(this.explosionPool, this.preloader);
        this.player.on(Ticker_1.Events.GAME_OVER, () => {
            this.emit(Ticker_1.Events.GAME_OVER);
        });
        // init bullet pool (requires player object)
        this.bulletPool = new Pool_1.default(Bullets_1.default, this.preloader, this.player);
        // add all to stage
        this.addChild(this.nebulaContainer);
        this.addChild(this.starsContainer);
        this.addChild(this.explosionContainer);
        this.addChild(this.hitContainer);
        this.hitContainer.addChild(this.player);
    }
    /**
     * Show the game screen, start player movement, spawns enemies and bullets
     */
    show() {
        this.visible = true;
        this.player.start();
        this._spawnInterval = window.setInterval(() => {
            this.hitContainer.addChild(this.enemyPool.activateOne());
        }, this.spawnTime * 1000);
        this._fireInterval = window.setInterval(() => {
            this.hitContainer.addChild(this.bulletPool.activateOne());
        }, this._fireTime * 1000);
    }
    /**
     * Stops all game mechanics and spawns
     */
    hide() {
        this.visible = false;
        this.player.reset();
        this.enemyPool.deactivateAll();
        window.clearInterval(this._fireInterval);
        clearInterval(this._spawnInterval);
    }
    /**
     * Moves the background, calculates collisions
     */
    update() {
        this.starsContainer.x += this.starsContainer.x - 1 <= -this.stage.bounds.width ? this.stage.bounds.width : -1;
        this.nebulaContainer.x += this.nebulaContainer.x - 3 <= -this.stage.bounds.width ? this.stage.bounds.width : -3;
        this.collisions.update();
        super.update();
    }
}
exports.default = GameScreen;
},{"./Bullets":3,"./CollisionDetector":5,"./Container":7,"./Enemy":10,"./Explosion":11,"./GraphicImage":17,"./Player":19,"./Pool":20,"./Ticker":27}],17:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const DisplayObject_1 = require("./DisplayObject");
class GraphicImage extends DisplayObject_1.default {
    /**
     * Creates an image object that can be used on the canvas.
     * @param image bitmap used on the canvas
     */
    constructor(image) {
        super();
        this.image = image;
        this.bounds = { width: this.image.width, height: this.image.height };
    }
    update() {
        this.ctx.globalAlpha = this.opacity;
        this.ctx.drawImage(this.image, this.x, this.y);
    }
}
exports.default = GraphicImage;
},{"./DisplayObject":8}],18:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Text_1 = require("./Text");
const Container_1 = require("./Container");
const GraphicImage_1 = require("./GraphicImage");
const Ticker_1 = require("./Ticker");
const Button_1 = require("./Button");
class MainScreen extends Container_1.default {
    /**
     * Creates the menu screen with 4 buttons, Moving background, logo, and title text.
     * @param stage stage
     * @param preloader used to get loaded images
     */
    constructor(stage, preloader) {
        super();
        this.stage = stage;
        this.preloader = preloader;
        this._comets = [];
        this._startGameProxy = () => { this.emit(Ticker_1.Events.GAME_START); };
        this._exitProxy = () => { this.emit(Ticker_1.Events.EXIT); };
        // gather images
        let bg = new GraphicImage_1.default(preloader.getByName('main').bitmap);
        this.addChild(bg);
        // create logo
        let logo = new GraphicImage_1.default(preloader.getByName('logo').bitmap);
        logo.x = this.stage.canvas.width / 2 - logo.image.width / 2;
        logo.y = this.stage.canvas.height / 3 - 20;
        this.addChild(logo);
        // create title text
        let titleText = new Text_1.default('ORBLASTER', '#fff', '32px Orbitron', 'center');
        titleText.x = this.stage.canvas.width / 2;
        titleText.y = this.stage.canvas.height / 4;
        this.addChild(titleText);
        // create buttons
        this.createButtons();
        // create comets
        this.createComets(5);
    }
    createButtons() {
        // create buttons
        this._button1 = new Button_1.default('GAME 1');
        this._button2 = new Button_1.default('GAME 2');
        this._button3 = new Button_1.default('GAME 3');
        this._button4 = new Button_1.default('EXIT');
        this._button1.x = this._button2.x = this._button3.x = this._button4.x = this.stage.canvas.width / 2;
        this._button1.y = this.stage.canvas.height / 2 + 75;
        this._button2.y = this._button1.y + 43;
        this._button3.y = this._button2.y + 43;
        this._button4.y = this._button3.y + 60;
        // add them to the stage
        this.addChild(this._button1);
        this.addChild(this._button2);
        this.addChild(this._button3);
        this.addChild(this._button4);
    }
    show() {
        this.visible = true;
        this._button1.on(Ticker_1.Events.CLICK, this._startGameProxy);
        this._button2.on(Ticker_1.Events.CLICK, this._startGameProxy);
        this._button3.on(Ticker_1.Events.CLICK, this._startGameProxy);
        this._button4.on(Ticker_1.Events.CLICK, this._exitProxy);
    }
    hide() {
        this.visible = false;
        this._button1.removeAllListeners(Ticker_1.Events.CLICK);
        this._button2.removeAllListeners(Ticker_1.Events.CLICK);
        this._button3.removeAllListeners(Ticker_1.Events.CLICK);
        this._button4.removeAllListeners(Ticker_1.Events.CLICK);
    }
    createComets(count) {
        let cometTypes = ['blue comet', 'purple comet'];
        for (let i = 0; i < count; i++) {
            let comet = new GraphicImage_1.default(this.preloader.getByName(cometTypes[Math.round(Math.random())]).bitmap);
            comet.x = Math.random() * this.stage.canvas.width - comet.image.width;
            comet.y = Math.random() * this.stage.canvas.height - comet.image.height;
            this.addChild(comet);
            this._comets.push(comet);
        }
    }
    /**
     * Moves the comets
     */
    update() {
        this._comets.forEach(comet => {
            comet.x = comet.localX + 1.9 * 2;
            comet.y = comet.localY + 1 * 2;
            if (comet.localX > this.stage.canvas.width) {
                comet.x = -comet.image.width;
            }
            if (comet.localY > this.stage.canvas.height) {
                comet.y = -comet.image.height;
            }
        });
        super.update();
    }
    destroy() {
        this.hide();
        this._comets = [];
        super.remove();
    }
}
exports.default = MainScreen;
},{"./Button":4,"./Container":7,"./GraphicImage":17,"./Text":26,"./Ticker":27}],19:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const GameObject_1 = require("./GameObject");
const GraphicImage_1 = require("./GraphicImage");
const Ticker_1 = require("./Ticker");
const Enemy_1 = require("./Enemy");
class Player extends GameObject_1.default {
    /**
     * Player ship that follows the pointer.
     * Fires GAME_OVER event when destroyed.
     * @param explosion pool used to handle explosions
     * @param preloader used to get loaded images
     */
    constructor(explosion, preloader) {
        super();
        this.explosion = explosion;
        this.preloader = preloader;
        this._followMouseProxy = (mouse) => {
            this.mouseX = mouse.x - this.image.bounds.width / 2;
            this.mouseY = mouse.y - this.image.bounds.height / 2;
        };
        this.image = new GraphicImage_1.default(this.preloader.getByName('ship').bitmap);
        this.image.x = 0;
        this.image.y = 0;
        this.x = this.mouseX = 50;
        this.y = this.mouseY = 300;
        this.addChild(this.image);
        this.bounds = this.image.bounds;
    }
    onCollision(go) {
        if (go instanceof Enemy_1.default) {
            this.emit(Ticker_1.Events.GAME_OVER);
            this.destroy();
        }
    }
    start() {
        this.visible = true;
        this.addEvents();
        super.start();
    }
    reset() {
        this.visible = false;
        this.removeEvents();
        super.reset();
    }
    addEvents() {
        this.stage.on(Ticker_1.Events.MOUSEMOVE, this._followMouseProxy);
    }
    removeEvents() {
        this.stage.removeListener(Ticker_1.Events.MOUSEMOVE, this._followMouseProxy);
    }
    update() {
        this.x = this.x - (this.x - this.mouseX) / 5;
        this.y = this.y - (this.y - this.mouseY) / 5;
        super.update();
    }
    destroy() {
        this.explosion.activateOne(this.x, this.y);
        this.reset();
    }
}
exports.default = Player;
},{"./Enemy":10,"./GameObject":14,"./GraphicImage":17,"./Ticker":27}],20:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Pool {
    /**
     * Generic object pool for saving processing power.
     * @param _class class to be instantiated
     * @param args constructor arguments for the class
     */
    constructor(_class, ...args) {
        this._class = _class;
        this._args = args;
        this._pool = [];
    }
    /**
     * Deactivates all pool objects
     */
    deactivateAll() {
        this._pool.forEach(item => item.reset());
    }
    /**
     * Finds an inactive object or creates one if there is none.
     * @param args arguments to the start function if has any
     */
    activateOne(...args) {
        let item = this._pool.find(item => item.active === false) || this.create();
        item.start(...args);
        return item;
    }
    /**
     * Instantiates the class
     */
    create() {
        let instance = new this._class(...this._args);
        this._pool.push(instance);
        return instance;
    }
}
exports.default = Pool;
},{}],21:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const events_1 = require("events");
const Ticker_1 = require("./Ticker");
class Preloader extends events_1.EventEmitter {
    /**
     * Preloads the images, then fires a COMPLETE event.
     * @param images images to be loaded
     * @param imgPath path to be prepended beore image names
     */
    constructor(images, imgPath) {
        super();
        this.resources = [];
        let promises = [];
        images.forEach(image => {
            let promise = new Promise((resolve, reject) => {
                let src = imgPath + image.src;
                let img = new Image();
                img.src = src;
                let splash;
                img.onload = () => {
                    createImageBitmap(img, 0, 0, img.naturalWidth, img.naturalHeight).then((imageBitmap) => {
                        this.resources.push({
                            name: image.name,
                            src: src,
                            width: imageBitmap.width,
                            height: imageBitmap.height,
                            bitmap: imageBitmap
                        });
                    });
                    resolve('image loaded');
                };
                img.onerror = function (e) {
                    reject(e);
                };
            });
            promises.push(promise);
        });
        Promise.all(promises).then(() => {
            this.emit(Ticker_1.Events.COMPLETE);
        });
    }
    /**
     * Get the resource by its name
     * @param name name of the resource
     */
    getByName(name) {
        return this.resources.find(resource => resource.name === name);
    }
}
exports.default = Preloader;
},{"./Ticker":27,"events":1}],22:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const DisplayObject_1 = require("./DisplayObject");
class Rect extends DisplayObject_1.default {
    /**
     * Creates a colored rectangle.
     * @param color color of the rectangle
     * @param width width of the rectangle
     * @param height height of the rectangle
     */
    constructor(color, width, height) {
        super();
        this.color = color;
        this.width = width;
        this.height = height;
        this.stroke = '';
        this.bounds = { width: this.width, height: this.height };
    }
    update() {
        this.ctx.globalAlpha = this.opacity;
        this.ctx.fillStyle = this.color;
        this.ctx.strokeStyle = this.stroke;
        this.ctx.lineWidth = 1;
        this.ctx.strokeRect(this.x, this.y, this.width, this.height);
        this.ctx.fillRect(this.x, this.y, this.width, this.height);
    }
}
exports.default = Rect;
},{"./DisplayObject":8}],23:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const GraphicImage_1 = require("./GraphicImage");
const Container_1 = require("./Container");
const Text_1 = require("./Text");
class SplashScreen extends Container_1.default {
    /**
     * Simple background with a title text.
     * @param stage stage
     * @param preloader used to get images
     */
    constructor(stage, preloader) {
        super();
        this.stage = stage;
        this.preloader = preloader;
        // gather images
        let bg = new GraphicImage_1.default(this.preloader.getByName('splash').bitmap);
        let text = new Text_1.default('ORBLASTER', '#fff', '40px Orbitron', 'center');
        text.x = this.stage.canvas.width / 2;
        text.y = this.stage.canvas.height / 3 * 2;
        this.addChild(bg);
        this.addChild(text);
    }
}
exports.default = SplashScreen;
},{"./Container":7,"./GraphicImage":17,"./Text":26}],24:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Container_1 = require("./Container");
const Rect_1 = require("./Rect");
class Stage extends Container_1.default {
    constructor(canvas) {
        super();
        this.canvas = canvas;
        /**
         * Manages drawing to the canvas. All Display objects must be added to the stage
         * to be drawn to the canvas. It has a solid black background.
         * Listens for mouse events and calculates mouse position on the canvas.
         */
        this.mouse = {
            x: 0,
            y: 0
        };
        this.mouseX = 0;
        this.mouseY = 0;
        this.ctx = canvas.getContext('2d');
        this._stage = this;
        this.bg = new Rect_1.default('#000', this.canvas.width, this.canvas.height);
        this.addChild(this.bg);
        this.canvas.addEventListener('mousemove', event => { this.calculateMousePos(event); this._onMouseMove(this.mouse); });
        this.canvas.addEventListener('mousedown', event => { this.calculateMousePos(event); this._onMouseDown(this.mouse); });
        this.canvas.addEventListener('mouseup', event => { this.calculateMousePos(event); this._onMouseUp(); });
        this.canvas.addEventListener('click', event => { this.calculateMousePos(event); this._onClick(this.mouse); });
        this.bounds = { width: this.canvas.width, height: this.canvas.height };
    }
    calculateMousePos(event) {
        let canvasBoundingRect = this.stage.canvas.getBoundingClientRect();
        this.mouse.x = event.pageX - canvasBoundingRect.left;
        this.mouse.y = event.pageY - canvasBoundingRect.top;
    }
    update() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        super.update();
    }
}
exports.default = Stage;
},{"./Container":7,"./Rect":22}],25:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const events_1 = require("events");
const Ticker_1 = require("./Ticker");
class StateMachine extends events_1.EventEmitter {
    /**
     * State-machine-like class which specializez in transitioning screens.
     * @param gc game controller object
     * @param states states the machine is allowed to transition to.
     * @param currentState stores the current state
     */
    constructor(gc, states, currentState) {
        super();
        this.gc = gc;
        this.states = states;
        this.currentState = currentState;
    }
    /**
     * If the current state has a transition given by the parameter,
     * then the state is changed to the new state, and the transition
     * funciton is played.
     * @param transition transition name
     */
    transition(transition) {
        let t = this.states[this.currentState][transition];
        if (t) {
            this.gc.fader.stop();
            window.clearTimeout(this._delayTimeout);
            this.currentState = t.to;
            this.play(t);
        }
    }
    /**
     * Fires a complete event
     */
    complete() {
        this.emit(Ticker_1.Events.COMPLETE);
    }
    /**
     * Runs the chosen transition and applies a delay between before the next transition if necessary
     * @param t tarnsition to be run
     */
    play(t) {
        // start prev fade out
        this.gc.fader.on(Ticker_1.Events.FADE_IN, () => {
            // on prev fade out
            this.gc.fader.removeAllListeners();
            // setup logic goes here
            t.beforeFadeIn();
            // start to fade in new screen
            this.gc.fader.on(Ticker_1.Events.FADE_OUT, () => {
                this.gc.fader.removeAllListeners();
                // wait for x seconds before next screen
                if (t.delay) {
                    this._delayTimeout = window.setTimeout(() => { this.complete(); }, t.delay * 1000);
                }
                else {
                    this.complete();
                }
            });
            this.gc.fader.fadeOut();
        });
        this.gc.fader.fadeIn();
    }
}
exports.default = StateMachine;
},{"./Ticker":27,"events":1}],26:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const DisplayObject_1 = require("./DisplayObject");
class Text extends DisplayObject_1.default {
    /**
     * Simple text object for use on the canvas
     * @param text text
     * @param color color
     * @param font CSS font definition
     * @param align text alignment
     */
    constructor(text, color, font, align) {
        super();
        this.text = text;
        this.color = color;
        this.font = font;
        this.align = align;
        let textHTML = document.createElement("div");
        textHTML.innerHTML = this.text;
        textHTML.style.position = 'absolute';
        textHTML.style.top = '-9999px';
        textHTML.style.left = '-9999px';
        textHTML.style.font = this.font;
        document.body.appendChild(textHTML);
        this.bounds = { width: textHTML.offsetWidth, height: textHTML.offsetHeight };
        document.body.removeChild(textHTML);
    }
    update() {
        this.ctx.globalAlpha = this.opacity;
        this.ctx.font = this.font;
        this.ctx.fillStyle = this.color;
        this.ctx.textAlign = this.align;
        this.ctx.fillText(this.text, this.x, this.y);
    }
}
exports.default = Text;
},{"./DisplayObject":8}],27:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const events_1 = require("events");
class _Ticker extends events_1.EventEmitter {
    /**
     * Handles RequestAnimationFrames. Can be used to listen for the UPDATE event for animation purposes.
     * Singleton for easy access everywhere in the app.
     */
    constructor() {
        super();
        let self = this;
        window.requestAnimationFrame(function gameLoop(timeFromStart) {
            window.requestAnimationFrame(gameLoop);
            self.emit(exports.Events.UPDATE);
        });
    }
    static get Instance() {
        // Do you need arguments? Make it a regular method instead.
        return this._instance || (this._instance = new this());
    }
}
exports.Events = {
    // mouse events
    UPDATE: Symbol('update event'),
    MOUSEMOVE: Symbol('mousemove event'),
    MOUSEENTER: Symbol('mouseenter event'),
    MOUSELEAVE: Symbol('mouseleave event'),
    MOUSEDOWN: Symbol('mousedown event'),
    MOUSEUP: Symbol('mousedown event'),
    CLICK: Symbol('click event'),
    // specific events
    COMPLETE: Symbol('oncomplete event'),
    FADE_IN: Symbol('fade in finished event'),
    FADE_OUT: Symbol('fade out finised event'),
    GAME_START: Symbol('game start event'),
    GAME_OVER: Symbol('game over event'),
    EXIT: Symbol('exit event'),
    COLLISION: Symbol('collision event'),
};
exports.Ticker = _Ticker.Instance;
exports.Ticker.setMaxListeners(30);
},{"events":1}],28:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const GameController_1 = require("./GameController");
// start application
const game = new GameController_1.default(document.getElementById('stage'));
},{"./GameController":13}]},{},[28])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJub2RlX21vZHVsZXMvZXZlbnRzL2V2ZW50cy5qcyIsInNyYy90cy9BbmltYXRpb24udHMiLCJzcmMvdHMvQnVsbGV0cy50cyIsInNyYy90cy9CdXR0b24udHMiLCJzcmMvdHMvQ29sbGlzaW9uRGV0ZWN0b3IudHMiLCJzcmMvdHMvQ29sbGlzaW9uR3JpZC50cyIsInNyYy90cy9Db250YWluZXIudHMiLCJzcmMvdHMvRGlzcGxheU9iamVjdC50cyIsInNyYy90cy9FYXNpbmdzLnRzIiwic3JjL3RzL0VuZW15LnRzIiwic3JjL3RzL0V4cGxvc2lvbi50cyIsInNyYy90cy9GYWRlU2NyZWVuLnRzIiwic3JjL3RzL0dhbWVDb250cm9sbGVyLnRzIiwic3JjL3RzL0dhbWVPYmplY3QudHMiLCJzcmMvdHMvR2FtZU92ZXJTY3JlZW4udHMiLCJzcmMvdHMvR2FtZVNjcmVlbi50cyIsInNyYy90cy9HcmFwaGljSW1hZ2UudHMiLCJzcmMvdHMvTWFpblNjcmVlbi50cyIsInNyYy90cy9QbGF5ZXIudHMiLCJzcmMvdHMvUG9vbC50cyIsInNyYy90cy9QcmVsb2FkZXIudHMiLCJzcmMvdHMvUmVjdC50cyIsInNyYy90cy9TcGxhc2hTY3JlZW4udHMiLCJzcmMvdHMvU3RhZ2UudHMiLCJzcmMvdHMvU3RhdGVNYWNoaW5lLnRzIiwic3JjL3RzL1RleHQudHMiLCJzcmMvdHMvVGlja2VyLnRzIiwic3JjL3RzL2FwcC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQzlTQSxxQ0FBMEM7QUFDMUMsdUNBQTRDO0FBQzVDLG1DQUFzQztBQUV0Qzs7R0FFRztBQUNILGVBQStCLFNBQVEscUJBQVk7SUFTL0M7Ozs7Ozs7OztPQVNHO0lBQ0gsWUFBbUIsUUFBZ0IsRUFBUyxNQUFjLEVBQVMsRUFBeUIsRUFBUyxTQUEwQixFQUFFO1FBQzdILEtBQUssRUFBRSxDQUFDO1FBRE8sYUFBUSxHQUFSLFFBQVEsQ0FBUTtRQUFTLFdBQU0sR0FBTixNQUFNLENBQVE7UUFBUyxPQUFFLEdBQUYsRUFBRSxDQUF1QjtRQUFTLFdBQU0sR0FBTixNQUFNLENBQXNCO1FBaEJ6SCxVQUFLLEdBQVksS0FBSyxDQUFDO1FBQ3ZCLFlBQU8sR0FBVyxpQkFBTyxDQUFDLE1BQU0sQ0FBQztRQUdqQyxpQkFBWSxHQUFHLE1BQU0sSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBY3ZDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNiLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLE1BQU0sSUFBSSxpQkFBTyxDQUFDLE1BQU0sQ0FBQztRQUMvQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ3RDLElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQztJQUM3QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsS0FBSztRQUNELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNuQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1lBQzdCLEdBQUcsQ0FBQyxDQUFDLE1BQU0sSUFBSSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDeEMsRUFBRSxDQUFDLENBQUMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQ3RDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDeEMsQ0FBQztZQUNMLENBQUM7UUFDTCxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztZQUNmLEdBQUcsQ0FBQyxDQUFDLE1BQU0sSUFBSSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDdEMsRUFBRSxDQUFDLENBQUMsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQ3hDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDeEMsQ0FBQztZQUNMLENBQUM7UUFDTCxDQUFDO1FBQ0QsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7SUFDakMsQ0FBQztJQUVELElBQUk7UUFDQSxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDYixlQUFNLENBQUMsRUFBRSxDQUFDLGVBQU0sQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCxJQUFJO1FBQ0EsZUFBTSxDQUFDLGNBQWMsQ0FBQyxlQUFNLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUM1RCxDQUFDO0lBRUQsTUFBTTtRQUNGLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQy9DLEdBQUcsQ0FBQyxDQUFDLE1BQU0sSUFBSSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN0QyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN0SSxDQUFDO1FBQ0QsRUFBRSxDQUFDLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ2hDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUNiLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQTtZQUNoQixDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUNaLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQy9CLENBQUM7UUFDTCxDQUFDO0lBQ0wsQ0FBQztDQUNKO0FBekVELDRCQXlFQzs7OztBQ2hGRCw2Q0FBc0M7QUFFdEMsaURBQTBDO0FBRTFDLG1DQUE0QjtBQUU1QixZQUE0QixTQUFRLG9CQUFVO0lBSTFDOzs7O09BSUc7SUFDSCxZQUFtQixTQUFvQixFQUFTLE1BQWM7UUFDMUQsS0FBSyxFQUFFLENBQUM7UUFETyxjQUFTLEdBQVQsU0FBUyxDQUFXO1FBQVMsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQVA5RCxVQUFLLEdBQUcsRUFBRSxDQUFDO1FBVVAsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLHNCQUFZLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNwRSxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO1FBRWhDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzlCLENBQUM7SUFFRCxXQUFXLENBQUMsRUFBYztRQUN0QixFQUFFLENBQUMsQ0FBQyxFQUFFLFlBQVksZUFBSyxDQUFDLENBQUMsQ0FBQztZQUN0QixJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDbkIsQ0FBQztJQUNMLENBQUM7SUFFRCxLQUFLO1FBQ0QsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7UUFDakIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ2xCLENBQUM7SUFFRCxLQUFLO1FBQ0QsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7UUFDakIsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztRQUN2QixJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1FBQ3ZCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7WUFBQyxNQUFNLENBQUM7UUFDeEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDbkIsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ2xCLENBQUM7SUFFRCxNQUFNO1FBQ0YsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQUMsTUFBTSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQztRQUNyQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDbkMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ25CLENBQUM7UUFDRCxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDbkIsQ0FBQztJQUVELE9BQU87UUFDSCxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDYixLQUFLLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDcEIsQ0FBQztDQUNKO0FBcERELHlCQW9EQzs7OztBQ3pERCxpQ0FBMEI7QUFDMUIsaUNBQTBCO0FBQzFCLDJDQUFvQztBQUNwQyxxQ0FBa0M7QUFFbEMsWUFBNEIsU0FBUSxtQkFBUztJQUl6Qzs7O09BR0c7SUFDSCxZQUFtQixJQUFZO1FBQzNCLEtBQUssRUFBRSxDQUFDO1FBRE8sU0FBSSxHQUFKLElBQUksQ0FBUTtRQUczQix5QkFBeUI7UUFDekIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLGNBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxlQUFlLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDcEUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2pCLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNqQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksY0FBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQ2xFLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUMzQixJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDNUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1FBQzdDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztRQUN6QixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMxQixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUUxQixlQUFlO1FBQ2YsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsZUFBTSxDQUFDLFVBQVUsRUFBRSxDQUFDLEtBQUssT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDMUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsZUFBTSxDQUFDLFVBQVUsRUFBRSxDQUFDLEtBQUssT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDMUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsZUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDLEtBQUssT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDeEUsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsZUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDLEtBQUssT0FBTyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMvRCxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxlQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsS0FBSyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNwRSxDQUFDO0lBRUQsWUFBWSxDQUFDLEtBQWlCO1FBQzFCLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLFNBQVMsQ0FBQztRQUM3QixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUM7SUFDN0IsQ0FBQztJQUVELFlBQVksQ0FBQyxLQUFpQjtRQUMxQixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUM7UUFDMUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO0lBQzdCLENBQUM7SUFFRCxXQUFXLENBQUMsS0FBaUI7UUFDekIsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsU0FBUyxDQUFDO0lBQ2pDLENBQUM7SUFFRCxTQUFTO1FBQ0wsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsU0FBUyxDQUFDO0lBQ2pDLENBQUM7SUFFRCxPQUFPLENBQUMsS0FBaUI7UUFDckIsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFNLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFRCxPQUFPO1FBQ0gsSUFBSSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxlQUFNLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxlQUFNLENBQUMsVUFBVSxDQUFDLENBQUM7UUFFakQsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ2xCLENBQUM7Q0FDSjtBQTNERCx5QkEyREM7Ozs7QUNqRUQsbURBQW1DO0FBRW5DLDZDQUFzQztBQUN0QyxxQ0FBa0M7QUFPbEM7SUFHSTs7Ozs7Ozs7T0FRRztJQUNILFlBQW1CLE1BQWMsRUFBUyxPQUFlLEdBQUcsRUFBUyxTQUFvQjtRQUF0RSxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQVMsU0FBSSxHQUFKLElBQUksQ0FBYztRQUFTLGNBQVMsR0FBVCxTQUFTLENBQVc7UUFYekYsVUFBSyxHQUFXLEVBQUUsQ0FBQyxDQUFDLGlDQUFpQztRQVlqRCxlQUFlO1FBQ2YsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUM5QixjQUFjO1lBQ2QsSUFBSSxNQUFNLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUM7WUFDOUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSx1QkFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQyxDQUFDO1FBQzdELENBQUM7SUFDTCxDQUFDO0lBRUQ7O09BRUc7SUFDSCxNQUFNO1FBQ0YsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2IsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUM7UUFDdEMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDdEMsSUFBSSxFQUFjLENBQUM7WUFDbkIsRUFBRSxHQUFlLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM1QixJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3ZCLENBQUM7UUFDRCxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7SUFDM0IsQ0FBQztJQUVEOzs7T0FHRztJQUNILFNBQVMsQ0FBQyxFQUFjO1FBQ3BCLElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUM7UUFDdkIsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNoQyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUU5Qix1QkFBdUI7UUFDdkIsdUVBQXVFO1FBQ3ZFLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksSUFBSSxNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUN6RSxXQUFXLEdBQUcsV0FBVyxDQUFDLE1BQU0sQ0FBQztZQUNqQyxJQUFJLEdBQUcsV0FBVyxDQUFDLElBQUksQ0FBQztRQUM1QixDQUFDO1FBQ0QsRUFBRSxDQUFDLElBQUksR0FBRyxXQUFXLENBQUM7UUFFdEIsZ0NBQWdDO1FBQ2hDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsR0FBRyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyx5Q0FBeUM7UUFDekYsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNqRSxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzlDLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbkUsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQztRQUNsQyxJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDO1FBQ3BDLElBQUksVUFBVSxHQUFHLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsTUFBTSxFQUFFLENBQUM7UUFDeEMsSUFBSSxXQUFXLEdBQUcsRUFBRSxDQUFDLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxNQUFNLEVBQUUsQ0FBQztRQUUxQyxFQUFFLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztRQUNsQixJQUFJLENBQUMsZUFBZSxDQUFDLEVBQUUsQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDNUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQzdDLElBQUksQ0FBQyxlQUFlLENBQUMsRUFBRSxDQUFDLFNBQVMsRUFBRSxVQUFVLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsZUFBZSxDQUFDLEVBQUUsQ0FBQyxTQUFTLEVBQUUsV0FBVyxDQUFDLENBQUM7UUFFaEQseUJBQXlCO1FBQ3pCLFdBQVcsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQUVEOzs7Ozs7T0FNRztJQUNILGVBQWUsQ0FBQyxLQUFxQixFQUFFLEtBQW1CO1FBQ3RELEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQ3BDLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssS0FBSyxDQUFDLENBQUMsSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNuRCxNQUFNLENBQUM7WUFDWCxDQUFDO1FBQ0wsQ0FBQztRQUNELEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDdEIsQ0FBQztJQUVEOztPQUVHO0lBQ0gsS0FBSztRQUNELElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDaEMsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3BCLE9BQU8sV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQ3hCLFdBQVcsR0FBRyxXQUFXLENBQUMsTUFBTSxDQUFDO1lBQ2pDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUN4QixDQUFDO0lBQ0wsQ0FBQztJQUVEOztPQUVHO0lBQ0gsZUFBZTtRQUNYLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDO1FBQ3ZDLEdBQUcsQ0FBQyxDQUFDLElBQUksRUFBRSxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUNyQyxFQUFFLENBQUMsQ0FBQyxFQUFFLFlBQVksb0JBQVUsQ0FBQyxDQUFDLENBQUM7Z0JBQzNCLEdBQUcsQ0FBQyxDQUFDLElBQUksUUFBUSxJQUFJLEVBQUUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO29CQUNoQyxHQUFHLENBQUMsQ0FBQyxJQUFJLGNBQWMsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDNUQsRUFBRSxDQUFDLENBQUMsY0FBYyxDQUFDLFdBQVcsS0FBSyxFQUFFLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQzs0QkFDaEQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUFFLEVBQUUsY0FBYyxDQUFDLENBQUM7d0JBQzNDLENBQUM7b0JBQ0wsQ0FBQztnQkFDTCxDQUFDO1lBQ0wsQ0FBQztRQUNMLENBQUM7SUFDTCxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNILGFBQWEsQ0FBQyxHQUFlLEVBQUUsR0FBZTtRQUMxQyxFQUFFLENBQUMsQ0FDQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxNQUFNLENBQUMsS0FBSyxJQUFJLEdBQUcsQ0FBQyxDQUFDO1lBQ2pDLEdBQUcsQ0FBQyxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDLEtBQUs7WUFDakMsR0FBRyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxHQUFHLENBQUMsQ0FBQztZQUNsQyxHQUFHLENBQUMsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsK0JBQStCO1FBQ3RFLENBQUMsQ0FBQyxDQUFDO1lBQ0MsR0FBRyxDQUFDLElBQUksQ0FBQyxlQUFNLENBQUMsU0FBUyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ2hDLEdBQUcsQ0FBQyxJQUFJLENBQUMsZUFBTSxDQUFDLFNBQVMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNwQyxDQUFDO0lBQ0wsQ0FBQztDQUVKO0FBdklELG9DQXVJQzs7OztBQzlJRDtJQUdJOzs7Ozs7T0FNRztJQUNILFlBQW1CLElBQVksRUFBUyxNQUFZO1FBQWpDLFNBQUksR0FBSixJQUFJLENBQVE7UUFBUyxXQUFNLEdBQU4sTUFBTSxDQUFNO1FBVHBELFVBQUssR0FBRyxFQUFFLENBQUM7UUFVUCxNQUFNO1FBQ04sSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ2pCLENBQUM7SUFFRDs7O09BR0c7SUFDSCxVQUFVLENBQUMsRUFBYztRQUNyQixJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUMsU0FBUyxDQUFDO1FBQzdCLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQ3hDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM5QixJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDcEMsQ0FBQztZQUNELEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDOUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUNwRCxDQUFDO1lBQ0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUN4RCxDQUFDO1FBQ0QsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDZCwwQkFBMEI7WUFDMUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDL0IsQ0FBQztJQUNMLENBQUM7SUFFRDs7T0FFRztJQUNILEtBQUs7UUFDRCxJQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztJQUNwQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsZ0JBQWdCLENBQUMsR0FBaUI7UUFDOUIsSUFBSSxPQUFPLEdBQWlCLEVBQUUsQ0FBQztRQUUvQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNkLElBQUksU0FBUyxHQUFpQixFQUFFLENBQUMsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDO1lBQ3JGLE9BQU8sR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztRQUN0RSxDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNoRCxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNwRCxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixNQUFNLENBQUMsRUFBRSxDQUFDO1FBQ2QsQ0FBQztJQUNMLENBQUM7Q0FDSjtBQTdERCx1QkE2REM7Ozs7QUNoRUQsbURBQTRDO0FBSTVDLGVBQStCLFNBQVEsdUJBQWE7SUFBcEQ7O1FBQ1csYUFBUSxHQUFvQixFQUFFLENBQUM7SUFzRTFDLENBQUM7SUFwRUc7OztPQUdHO0lBQ0gsUUFBUSxDQUFDLGFBQTRCO1FBQ2pDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLEtBQUssS0FBSyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDeEQsYUFBYSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDNUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDdEMsQ0FBQztJQUNMLENBQUM7SUFFRDs7O09BR0c7SUFDSCxXQUFXLENBQUMsS0FBb0I7UUFDNUIsS0FBSyxDQUFDLE1BQU0sR0FBRyxTQUFTLENBQUM7UUFDekIsS0FBSyxDQUFDLEdBQUcsR0FBRyxTQUFTLENBQUM7UUFDdEIsS0FBSyxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUM7UUFDeEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDMUQsQ0FBQztJQUVEOztPQUVHO0lBQ0gsTUFBTTtRQUNGLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztZQUFDLE1BQU0sQ0FBQztRQUMxQixJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFVO1lBQzVCLEVBQUUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUNyQixVQUFVLENBQUMsTUFBTSxFQUFFLENBQUM7WUFDeEIsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVEOzs7T0FHRztJQUNILFlBQVksQ0FBQyxLQUFzQjtRQUMvQixLQUFLLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzFCLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUVEOztPQUVHO0lBQ0gsVUFBVTtRQUNOLEtBQUssQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUNuQixJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLFVBQVUsRUFBRSxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUVEOzs7T0FHRztJQUNILFFBQVEsQ0FBQyxLQUFzQjtRQUMzQixLQUFLLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDMUQsQ0FBQztJQUVEOzs7T0FHRztJQUNILFlBQVksQ0FBQyxLQUFzQjtRQUMvQixLQUFLLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzFCLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDOUQsQ0FBQztDQUNKO0FBdkVELDRCQXVFQzs7OztBQzNFRCxtQ0FBc0M7QUFFdEMscUNBQWtDO0FBR2xDLG1CQUFtQyxTQUFRLHFCQUFZO0lBQXZEOztRQUNJLFdBQU0sR0FBVyxDQUFDLENBQUMsQ0FBQyxxQ0FBcUM7UUFDekQsV0FBTSxHQUFXLENBQUMsQ0FBQyxDQUFDLHFDQUFxQztRQUN6RCxpQkFBWSxHQUFXLENBQUMsQ0FBQyxDQUFDLG9CQUFvQjtRQUU5QyxXQUFNLEdBQVcsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLE1BQU0sRUFBRSxDQUFDLEVBQUUsQ0FBQztRQUN6QyxZQUFPLEdBQVksSUFBSSxDQUFDO1FBS2hCLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFDbkIsZUFBVSxHQUFHLEtBQUssQ0FBQztJQW1KL0IsQ0FBQztJQWpKRzs7O09BR0c7SUFDSCxvQkFBb0IsQ0FBQyxLQUFzQjtRQUN2QyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQztlQUNoQixLQUFLLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLO2VBQ3BDLEtBQUssQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUM7ZUFDaEIsS0FBSyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFBO0lBQ2hELENBQUM7SUFFRCxZQUFZLENBQUMsS0FBc0I7UUFDL0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdkQseUJBQXlCO1lBQ3pCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBTSxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUN2QyxDQUFDO0lBQ0wsQ0FBQztJQUVELFVBQVU7UUFDTixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUNsQixJQUFJLENBQUMsSUFBSSxDQUFDLGVBQU0sQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDakMsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7UUFDNUIsQ0FBQztJQUNMLENBQUM7SUFFRCxRQUFRLENBQUMsS0FBc0I7UUFDM0IsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNuQyx5QkFBeUI7WUFDekIsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFNLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ25DLENBQUM7SUFDTCxDQUFDO0lBRUQsWUFBWSxDQUFDLEtBQXNCO1FBQy9CLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbkMseUJBQXlCO1lBQ3pCLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBTSxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNuQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO2dCQUNuQixJQUFJLENBQUMsSUFBSSxDQUFDLGVBQU0sQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBQ3BDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1lBQzNCLENBQUM7UUFDTCxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSix5QkFBeUI7WUFDekIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xCLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBTSxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQztnQkFDcEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7WUFDNUIsQ0FBQztRQUNMLENBQUM7SUFDTCxDQUFDO0lBRUQ7O09BRUc7SUFDSCxNQUFNO1FBQ0YsMEJBQTBCO0lBQzlCLENBQUM7SUFFRDs7T0FFRztJQUNILFdBQVc7UUFDUCxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3pCLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDVCxNQUFNLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3pCLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUIsQ0FBQztJQUNMLENBQUM7SUFFRDs7O09BR0c7SUFDSCxNQUFNO1FBQ0YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQUVEOztPQUVHO0lBQ0gsSUFBSSxLQUFLO1FBQ0wsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQzlCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDcEMsQ0FBQztRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQ3ZCLENBQUM7SUFFRCxJQUFJLEtBQUssQ0FBQyxLQUFZO1FBQ2xCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQ3hCLENBQUM7SUFFRDs7T0FFRztJQUNILElBQUksT0FBTztRQUNQLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ2QsTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUM7UUFDbkQsQ0FBQztRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDO0lBQzdCLENBQUM7SUFFRCxJQUFJLE9BQU8sQ0FBQyxPQUFlO1FBQ3ZCLElBQUksQ0FBQyxZQUFZLEdBQUcsT0FBTyxDQUFDO0lBQ2hDLENBQUM7SUFFRDs7T0FFRztJQUNILElBQUksQ0FBQztRQUNELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ2QsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7UUFDdkMsQ0FBQztRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQ3ZCLENBQUM7SUFFRCxJQUFJLENBQUMsQ0FBQyxDQUFTO1FBQ1gsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7SUFDcEIsQ0FBQztJQUVEOztPQUVHO0lBQ0gsSUFBSSxDQUFDO1FBQ0QsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDZCxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztRQUN2QyxDQUFDO1FBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDdkIsQ0FBQztJQUVELElBQUksQ0FBQyxDQUFDLENBQVM7UUFDWCxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztJQUNwQixDQUFDO0lBRUQ7O09BRUc7SUFDSCxJQUFJLEdBQUc7UUFDSCxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDNUIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQztRQUNoQyxDQUFDO1FBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDckIsQ0FBQztJQUVELElBQUksR0FBRyxDQUFDLEdBQTZCO1FBQ2pDLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDO0lBQ3BCLENBQUM7Q0FDSjtBQS9KRCxnQ0ErSkM7Ozs7QUNsS0Q7O0dBRUc7QUFDVSxRQUFBLE9BQU8sR0FBRztJQUNuQixHQUFHLEVBQUUsVUFBVSxDQUFDLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3ZELFNBQVMsRUFBRSxVQUFVLENBQUMsSUFBSSxNQUFNLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQy9FLFVBQVUsRUFBRSxVQUFVLENBQUMsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDOUQsWUFBWSxFQUFFLFVBQVUsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3BGLDZCQUE2QjtJQUM3QixNQUFNLEVBQUUsVUFBVSxDQUFDLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQSxDQUFDLENBQUM7SUFDakMsa0NBQWtDO0lBQ2xDLFVBQVUsRUFBRSxVQUFVLENBQUMsSUFBSSxNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQSxDQUFDLENBQUM7SUFDekMsZ0NBQWdDO0lBQ2hDLFdBQVcsRUFBRSxVQUFVLENBQUMsSUFBSSxNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFBLENBQUMsQ0FBQztJQUNoRCxnREFBZ0Q7SUFDaEQsYUFBYSxFQUFFLFVBQVUsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUEsQ0FBQyxDQUFDO0lBQ2hGLG1DQUFtQztJQUNuQyxXQUFXLEVBQUUsVUFBVSxDQUFDLElBQUksTUFBTSxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFBLENBQUMsQ0FBQztJQUM5QyxpQ0FBaUM7SUFDakMsWUFBWSxFQUFFLFVBQVUsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUEsQ0FBQyxDQUFDO0lBQ3ZELGlEQUFpRDtJQUNqRCxjQUFjLEVBQUUsVUFBVSxDQUFDLElBQUksTUFBTSxDQUFDLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFBLENBQUMsQ0FBQztJQUN4RyxtQ0FBbUM7SUFDbkMsV0FBVyxFQUFFLFVBQVUsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUEsQ0FBQyxDQUFDO0lBQ2xELGlDQUFpQztJQUNqQyxZQUFZLEVBQUUsVUFBVSxDQUFDLElBQUksTUFBTSxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUEsQ0FBQyxDQUFDO0lBQzNELGdEQUFnRDtJQUNoRCxjQUFjLEVBQUUsVUFBVSxDQUFDLElBQUksTUFBTSxDQUFDLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQSxDQUFDLENBQUM7SUFDOUYsa0NBQWtDO0lBQ2xDLFdBQVcsRUFBRSxVQUFVLENBQUMsSUFBSSxNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQSxDQUFDLENBQUM7SUFDdEQsZ0NBQWdDO0lBQ2hDLFlBQVksRUFBRSxVQUFVLENBQUMsSUFBSSxNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUEsQ0FBQyxDQUFDO0lBQy9ELGlEQUFpRDtJQUNqRCxjQUFjLEVBQUUsVUFBVSxDQUFDLElBQUksTUFBTSxDQUFDLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFBLENBQUMsQ0FBQztDQUMzRyxDQUFBOzs7O0FDcENELGlEQUEwQztBQUUxQyw2Q0FBc0M7QUFDdEMscUNBQWtDO0FBQ2xDLDJDQUFvQztBQUNwQyx1Q0FBb0M7QUFDcEMscUNBQThCO0FBQzlCLHVDQUErQjtBQUkvQixXQUEyQixTQUFRLG9CQUFVO0lBVXpDOzs7O09BSUc7SUFDSCxZQUFtQixTQUEwQixFQUFTLFNBQW9CO1FBQ3RFLEtBQUssRUFBRSxDQUFDO1FBRE8sY0FBUyxHQUFULFNBQVMsQ0FBaUI7UUFBUyxjQUFTLEdBQVQsU0FBUyxDQUFXO1FBYjFFLFdBQU0sR0FBWSxLQUFLLENBQUM7UUFDeEIsVUFBSyxHQUFHLEdBQUcsQ0FBQztRQUVKLGVBQVUsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsR0FBRyxHQUFHLEVBQUUsQ0FBQztRQUN0QyxlQUFVLEdBQUcsR0FBRyxDQUFDO1FBWXJCLGlCQUFpQjtRQUNqQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksc0JBQVksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN4RSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDakIsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2pCLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzFCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7UUFFaEMscUJBQXFCO1FBQ3JCLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUN6QixJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7UUFFekIsa0NBQWtDO1FBQ2xDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxtQkFBUyxDQUFDLENBQUMsRUFBRSxJQUFJLEVBQUUsRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLEVBQUUsTUFBTSxFQUFFLGlCQUFPLENBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1FBRWxILG9DQUFvQztRQUNwQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksbUJBQVMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxFQUFFLENBQUMsRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUUsTUFBTSxFQUFFLGlCQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztRQUM5SCxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxlQUFNLENBQUMsUUFBUSxFQUFFLE1BQU0sSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUM7SUFDNUQsQ0FBQztJQUVELFdBQVcsQ0FBQyxFQUFjO1FBQ3RCLEVBQUUsQ0FBQyxDQUFDLEVBQUUsWUFBWSxpQkFBTSxJQUFJLEVBQUUsWUFBWSxnQkFBTSxDQUFDLENBQUMsQ0FBQztZQUMvQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDbkIsQ0FBQztJQUNMLENBQUM7SUFFRCxLQUFLO1FBQ0QsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUN6QixLQUFLLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDbEIsQ0FBQztJQUVELEtBQUs7UUFDRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQUMsTUFBTSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNqQixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUNuQixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxHQUFHLEdBQUcsRUFBRSxDQUFDO1FBQzNDLEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUNsQixDQUFDO0lBRUQsU0FBUztRQUNMLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMzQixDQUFDO0lBRUQsWUFBWTtRQUNSLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMzQixDQUFDO0lBRUQsTUFBTTtRQUNGLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDZixNQUFNLENBQUM7UUFDWCxDQUFDO1FBQ0QsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDdEMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ25CLENBQUM7SUFFRCxPQUFPO1FBQ0gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztJQUNoRyxDQUFDO0lBRUQsT0FBTztRQUNILElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUNmLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNiLEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNwQixDQUFDO0NBQ0o7QUFyRkQsd0JBcUZDOzs7O0FDaEdELG1EQUE0QztBQUM1QywyQ0FBb0M7QUFDcEMscUNBQWtDO0FBSWxDLGVBQStCLFNBQVEsdUJBQWE7SUFXaEQ7OztPQUdHO0lBQ0gsWUFBb0IsVUFBcUI7UUFDckMsS0FBSyxFQUFFLENBQUM7UUFEUSxlQUFVLEdBQVYsVUFBVSxDQUFXO1FBWmxDLFdBQU0sR0FBRyxJQUFJLENBQUM7UUFHYixpQkFBWSxHQUFHLEVBQUUsQ0FBQztRQUNsQixlQUFVLEdBQUcsR0FBRyxDQUFDO1FBQ2pCLG9CQUFlLEdBQUcsRUFBRSxDQUFDO1FBQ3JCLGtCQUFhLEdBQUcsQ0FBQyxDQUFDO1FBU3RCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxtQkFBUyxDQUN0QixHQUFHLEVBQ0gsSUFBSSxFQUNKLEVBQUUsU0FBUyxFQUFFLElBQUksQ0FBQyxhQUFhLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsT0FBTyxFQUFFLENBQUMsRUFBRSxFQUN0RSxFQUFFLElBQUksRUFBRSxFQUFFLFNBQVMsRUFBRSxJQUFJLENBQUMsZUFBZSxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsWUFBWSxFQUFFLE9BQU8sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUN2RixDQUFDO1FBRUYsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsZUFBTSxDQUFDLFFBQVEsRUFBRSxNQUFNLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO1FBQ25ELElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFRCxLQUFLO1FBQ0QsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN0QixDQUFDO0lBRUQ7Ozs7T0FJRztJQUNILEtBQUssQ0FBQyxDQUFTLEVBQUUsQ0FBUztRQUN0QixJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNYLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ1gsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDbkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN0QixDQUFDO0lBRUQsT0FBTztRQUNILElBQUksQ0FBQyxLQUFLLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUNoQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ2xCLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUNsQixDQUFDO0lBRUQsTUFBTTtRQUNGLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ2pFLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDcEMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDO1FBQzlCLElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDcEMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUN0QixDQUFDO0NBQ0o7QUE1REQsNEJBNERDOzs7O0FDbEVELGlDQUEwQjtBQUUxQiwyQ0FBb0M7QUFFcEMscUNBQWtDO0FBQ2xDLHVDQUFvQztBQUVwQyxnQkFBZ0MsU0FBUSxjQUFJO0lBVXhDOzs7O09BSUc7SUFDSCxZQUFtQixLQUFZO1FBQzNCLEtBQUssQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUR4QyxVQUFLLEdBQUwsS0FBSyxDQUFPO1FBWnZCLG9CQUFlLEdBQUc7WUFDdEIsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDL0IsQ0FBQyxDQUFDO1FBQ00sbUJBQWMsR0FBRztZQUNyQixJQUFJLENBQUMsSUFBSSxDQUFDLGVBQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUM5QixDQUFDLENBQUM7UUFVRSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksbUJBQVMsQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLEVBQUUsWUFBWSxFQUFFLENBQUMsRUFBRSxFQUFFLEVBQUUsTUFBTSxFQUFFLGlCQUFPLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQztRQUNsRyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksbUJBQVMsQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLEVBQUUsWUFBWSxFQUFFLENBQUMsRUFBRSxFQUFFLEVBQUUsTUFBTSxFQUFFLGlCQUFPLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztRQUVsRyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxlQUFNLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUM1RCxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxlQUFNLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBRUQsTUFBTTtRQUNGLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNyQixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDdEIsTUFBTSxDQUFDO1FBQ1gsQ0FBQztRQUNELElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNuQixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzVCLENBQUM7SUFFRCxPQUFPO1FBQ0gsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3JCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN0QixNQUFNLENBQUM7UUFDWCxDQUFDO1FBQ0QsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ25CLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDN0IsQ0FBQztJQUVELElBQUk7UUFDQSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUVELE9BQU87UUFDSCxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDWixJQUFJLENBQUMsWUFBWSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDdkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQ3RDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUNuQixDQUFDO0NBQ0o7QUF0REQsNkJBc0RDOzs7O0FDN0RELG1DQUE0QjtBQUM1QiwyQ0FBb0M7QUFDcEMsaURBQTBDO0FBQzFDLDZDQUFzQztBQUN0QyxxQ0FBMEM7QUFDMUMsNkNBQXNDO0FBQ3RDLHFEQUE4QztBQUM5Qyw2Q0FBc0M7QUFDdEMsaURBQWtFO0FBRWxFO0lBUUk7OztPQUdHO0lBQ0gsWUFBbUIsTUFBeUI7UUFBekIsV0FBTSxHQUFOLE1BQU0sQ0FBbUI7UUFDeEMsZ0JBQWdCO1FBQ2hCLElBQUksU0FBUyxHQUFHLElBQUksbUJBQVMsQ0FBQztZQUMxQixFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsR0FBRyxFQUFFLGFBQWEsRUFBRTtZQUN0QyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLFdBQVcsRUFBRTtZQUNsQyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLFdBQVcsRUFBRTtZQUNsQyxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsR0FBRyxFQUFFLFdBQVcsRUFBRTtZQUN4QyxFQUFFLElBQUksRUFBRSxjQUFjLEVBQUUsR0FBRyxFQUFFLGFBQWEsRUFBRTtZQUM1QyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsR0FBRyxFQUFFLFlBQVksRUFBRTtZQUNwQyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsR0FBRyxFQUFFLHNCQUFzQixFQUFFO1lBQy9DLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsV0FBVyxFQUFFO1lBQ2xDLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxHQUFHLEVBQUUsZUFBZSxFQUFFO1lBQ3ZDLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxHQUFHLEVBQUUsYUFBYSxFQUFFO1NBQ3pDLEVBQUUsTUFBTSxDQUFDLENBQUE7UUFFVixTQUFTLENBQUMsRUFBRSxDQUFDLGVBQU0sQ0FBQyxRQUFRLEVBQUU7WUFDMUIsSUFBSSxLQUFLLEdBQUcsSUFBSSxlQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDOUIsZUFBTSxDQUFDLEVBQUUsQ0FBQyxlQUFNLENBQUMsTUFBTSxFQUFFLE1BQU0sS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7WUFFL0MsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLHNCQUFZLENBQUMsS0FBSyxFQUFFLFNBQVMsQ0FBQyxDQUFDO1lBQ2pELElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxvQkFBVSxDQUFDLEtBQUssRUFBRSxTQUFTLENBQUMsQ0FBQztZQUM3QyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksb0JBQVUsQ0FBQyxLQUFLLEVBQUUsU0FBUyxDQUFDLENBQUM7WUFDN0MsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLHdCQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLG9CQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7WUFFbkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBQzVCLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztZQUMxQixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7WUFDMUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBQzlCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztZQUUxQixLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUM1QixLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMxQixLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMxQixLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUM5QixLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUUzQixJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDakIsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsS0FBSztRQUVELElBQUksTUFBTSxHQUF1QjtZQUM3QixLQUFLLEVBQUU7Z0JBQ0gsSUFBSSxFQUFFO29CQUNGLFlBQVksRUFBRTt3QkFDVixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7b0JBQy9CLENBQUM7b0JBQ0QsS0FBSyxFQUFFLENBQUM7b0JBQ1IsRUFBRSxFQUFFLFFBQVE7aUJBQ2Y7YUFDSjtZQUNELE1BQU0sRUFBRTtnQkFDSixJQUFJLEVBQUU7b0JBQ0YsWUFBWSxFQUFFO3dCQUNWLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQzt3QkFDNUIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDckIsQ0FBQztvQkFDRCxFQUFFLEVBQUUsTUFBTTtpQkFDYjthQUNKO1lBQ0QsSUFBSSxFQUFFO2dCQUNGLFFBQVEsRUFBRTtvQkFDTixZQUFZLEVBQUU7d0JBQ1YsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDakIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDckIsQ0FBQztvQkFDRCxFQUFFLEVBQUUsTUFBTTtpQkFDYjtnQkFDRCxJQUFJLEVBQUU7b0JBQ0YsWUFBWSxFQUFFO3dCQUNWLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7d0JBQ2pCLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztvQkFDL0IsQ0FBQztvQkFDRCxLQUFLLEVBQUUsQ0FBQztvQkFDUixFQUFFLEVBQUUsUUFBUTtpQkFDZjthQUNKO1lBQ0QsSUFBSSxFQUFFO2dCQUNGLFFBQVEsRUFBRTtvQkFDTixZQUFZLEVBQUU7d0JBQ1YsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDakIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO29CQUNqQyxDQUFDO29CQUNELEtBQUssRUFBRSxDQUFDO29CQUNSLEVBQUUsRUFBRSxVQUFVO2lCQUNqQjthQUNKO1lBQ0QsUUFBUSxFQUFFO2dCQUNOLElBQUksRUFBRTtvQkFDRixZQUFZLEVBQUU7d0JBQ1YsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO3dCQUM5QixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7b0JBQy9CLENBQUM7b0JBQ0QsS0FBSyxFQUFFLENBQUM7b0JBQ1IsRUFBRSxFQUFFLFFBQVE7aUJBQ2Y7YUFDSjtTQUNKLENBQUE7UUFFRCwyQkFBMkI7UUFDM0IsSUFBSSxDQUFDLEVBQUUsR0FBRyxJQUFJLHNCQUFZLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQztRQUNsRCxJQUFJLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLG1CQUFtQjtRQUMvQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxlQUFNLENBQUMsUUFBUSxFQUFFLE1BQU0sSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLG9DQUFvQztRQUNuRyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxlQUFNLENBQUMsVUFBVSxFQUFFLE1BQU0sSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLDJCQUEyQjtRQUNsRyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxlQUFNLENBQUMsSUFBSSxFQUFFLE1BQU0sSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLHlCQUF5QjtRQUN0RixJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxlQUFNLENBQUMsU0FBUyxFQUFFLE1BQU0sSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLGNBQWM7SUFDeEYsQ0FBQztDQUNKO0FBekhELGlDQXlIQzs7OztBQ25JRCwyQ0FBb0M7QUFHcEMscUNBQWtDO0FBR2xDOztHQUVHO0FBQ0gsZ0JBQWdDLFNBQVEsbUJBQVM7SUFBakQ7O1FBS1ksc0JBQWlCLEdBQUcsQ0FBQyxFQUFjO1lBQ3ZDLElBQUksQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUE7UUFDeEIsQ0FBQyxDQUFDO0lBZU4sQ0FBQztJQWJHLFdBQVcsQ0FBQyxFQUFjLElBQUksQ0FBQztJQUUvQixLQUFLO1FBQ0QsSUFBSSxDQUFDLEVBQUUsQ0FBQyxlQUFNLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0lBQ3RELENBQUM7SUFFRCxLQUFLO1FBQ0QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFNLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0lBQ2xFLENBQUM7SUFFRCxPQUFPO1FBQ0gsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDbEMsQ0FBQztDQUNKO0FBdEJELDZCQXNCQzs7OztBQzlCRCxpQ0FBMEI7QUFDMUIsMkNBQW9DO0FBRXBDLG9CQUFvQyxTQUFRLG1CQUFTO0lBQ2pEOzs7T0FHRztJQUNILFlBQW1CLEtBQVk7UUFDM0IsS0FBSyxFQUFFLENBQUM7UUFETyxVQUFLLEdBQUwsS0FBSyxDQUFPO1FBRTNCLElBQUksSUFBSSxHQUFHLElBQUksY0FBSSxDQUFDLFdBQVcsRUFBRSxNQUFNLEVBQUUsZUFBZSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQ3BFLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUNyQyxJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN4QixDQUFDO0NBQ0o7QUFaRCxpQ0FZQzs7OztBQ2RELDJDQUFvQztBQUNwQyxpREFBMEM7QUFDMUMscUNBQTBDO0FBQzFDLHFDQUE4QjtBQUM5QixtQ0FBNEI7QUFDNUIsMkRBQW9EO0FBQ3BELGlDQUEwQjtBQUMxQiwyQ0FBb0M7QUFDcEMsdUNBQStCO0FBRS9CLGdCQUFnQyxTQUFRLG1CQUFTO0lBZ0I3Qzs7Ozs7O09BTUc7SUFDSCxZQUFtQixLQUFZLEVBQVMsU0FBb0I7UUFDeEQsS0FBSyxFQUFFLENBQUM7UUFETyxVQUFLLEdBQUwsS0FBSyxDQUFPO1FBQVMsY0FBUyxHQUFULFNBQVMsQ0FBVztRQWhCNUQsY0FBUyxHQUFXLENBQUMsQ0FBQztRQU9kLGNBQVMsR0FBVyxJQUFJLENBQUM7UUFZN0Isc0dBQXNHO1FBQ3RHLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLG1CQUFTLEVBQUUsQ0FBQztRQUMxQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksbUJBQVMsRUFBRSxDQUFDO1FBQ3RDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxtQkFBUyxFQUFFLENBQUM7UUFDdkMsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLG1CQUFTLEVBQUUsQ0FBQztRQUNwQyxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUM7UUFFbkMsZUFBZTtRQUNmLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxjQUFJLENBQVksbUJBQVMsRUFBRSxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQztRQUM3RSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksY0FBSSxDQUFRLGVBQUssRUFBRSxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUU1RSwyQkFBMkI7UUFDM0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLDJCQUFpQixDQUFDLENBQUMsRUFBRSxHQUFHLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBRW5FLDRCQUE0QjtRQUM1QixJQUFJLE1BQU0sR0FBRyxJQUFJLHNCQUFZLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDeEUsSUFBSSxNQUFNLEdBQUcsSUFBSSxzQkFBWSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3hFLE1BQU0sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUN2QyxJQUFJLE9BQU8sR0FBRyxJQUFJLHNCQUFZLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDMUUsSUFBSSxPQUFPLEdBQUcsSUFBSSxzQkFBWSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzFFLE9BQU8sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUV4QyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNyQyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUVyQyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN2QyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUV2QyxjQUFjO1FBQ2QsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLGdCQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDN0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsZUFBTSxDQUFDLFNBQVMsRUFBRTtZQUM3QixJQUFJLENBQUMsSUFBSSxDQUFDLGVBQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNoQyxDQUFDLENBQUMsQ0FBQztRQUNILDRDQUE0QztRQUM1QyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksY0FBSSxDQUFTLGlCQUFNLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFeEUsbUJBQW1CO1FBQ25CLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ25DLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7UUFDdkMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDakMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFFRDs7T0FFRztJQUNILElBQUk7UUFDQSxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNwQixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBRXBCLElBQUksQ0FBQyxjQUFjLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQztZQUNyQyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7UUFDN0QsQ0FBQyxFQUFFLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLENBQUM7UUFFMUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUMsV0FBVyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztRQUM5RCxDQUFDLEVBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsQ0FBQztJQUM5QixDQUFDO0lBRUQ7O09BRUc7SUFDSCxJQUFJO1FBQ0EsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNwQixJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQy9CLE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ3pDLGFBQWEsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVEOztPQUVHO0lBQ0gsTUFBTTtRQUNGLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDOUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQztRQUNoSCxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ3pCLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUNuQixDQUFDO0NBQ0o7QUExR0QsNkJBMEdDOzs7O0FDdEhELG1EQUF3RDtBQUV4RCxrQkFBa0MsU0FBUSx1QkFBYTtJQUVuRDs7O09BR0c7SUFDSCxZQUFtQixLQUFrQjtRQUNqQyxLQUFLLEVBQUUsQ0FBQztRQURPLFVBQUssR0FBTCxLQUFLLENBQWE7UUFFakMsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUN6RSxDQUFDO0lBRUQsTUFBTTtRQUNGLElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDcEMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNuRCxDQUFDO0NBQ0o7QUFmRCwrQkFlQzs7OztBQ2ZELGlDQUEwQjtBQUMxQiwyQ0FBb0M7QUFDcEMsaURBQTBDO0FBRTFDLHFDQUEwQztBQUMxQyxxQ0FBOEI7QUFFOUIsZ0JBQWdDLFNBQVEsbUJBQVM7SUFXN0M7Ozs7T0FJRztJQUNILFlBQW1CLEtBQVksRUFBUyxTQUFvQjtRQUN4RCxLQUFLLEVBQUUsQ0FBQztRQURPLFVBQUssR0FBTCxLQUFLLENBQU87UUFBUyxjQUFTLEdBQVQsU0FBUyxDQUFXO1FBZnBELFlBQU8sR0FBbUIsRUFBRSxDQUFDO1FBQzdCLG9CQUFlLEdBQUcsUUFBUSxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQSxDQUFDLENBQUMsQ0FBQztRQUN6RCxlQUFVLEdBQUcsUUFBUSxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQSxDQUFDLENBQUMsQ0FBQztRQWdCbEQsZ0JBQWdCO1FBQ2hCLElBQUksRUFBRSxHQUFHLElBQUksc0JBQVksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzlELElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7UUFFbEIsY0FBYztRQUNkLElBQUksSUFBSSxHQUFHLElBQUksc0JBQVksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2hFLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDNUQsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUMzQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRXBCLG9CQUFvQjtRQUNwQixJQUFJLFNBQVMsR0FBRyxJQUFJLGNBQUksQ0FBQyxXQUFXLEVBQUUsTUFBTSxFQUFFLGVBQWUsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUN6RSxTQUFTLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDMUMsU0FBUyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7UUFFekIsaUJBQWlCO1FBQ2pCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUVyQixnQkFBZ0I7UUFDaEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUN6QixDQUFDO0lBRUQsYUFBYTtRQUNULGlCQUFpQjtRQUNqQixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksZ0JBQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNyQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksZ0JBQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNyQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksZ0JBQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNyQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksZ0JBQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ3BHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQ3BELElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUN2QyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7UUFDdkMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBRXZDLHdCQUF3QjtRQUN4QixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUM3QixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUM3QixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUM3QixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUNqQyxDQUFDO0lBRUQsSUFBSTtRQUNBLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLGVBQU0sQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ3JELElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLGVBQU0sQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ3JELElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLGVBQU0sQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ3JELElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLGVBQU0sQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFRCxJQUFJO1FBQ0EsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDckIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxlQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0MsSUFBSSxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxlQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0MsSUFBSSxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxlQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0MsSUFBSSxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxlQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbkQsQ0FBQztJQUVELFlBQVksQ0FBQyxLQUFhO1FBQ3RCLElBQUksVUFBVSxHQUFhLENBQUMsWUFBWSxFQUFFLGNBQWMsQ0FBQyxDQUFDO1FBQzFELEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDN0IsSUFBSSxLQUFLLEdBQUcsSUFBSSxzQkFBWSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNyRyxLQUFLLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7WUFDdEUsS0FBSyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO1lBQ3hFLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDckIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDN0IsQ0FBQztJQUNMLENBQUM7SUFFRDs7T0FFRztJQUNILE1BQU07UUFDRixJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLO1lBQ3RCLEtBQUssQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sR0FBRyxHQUFHLEdBQUcsQ0FBQyxDQUFDO1lBQ2pDLEtBQUssQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQy9CLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDekMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO1lBQ2pDLENBQUM7WUFDRCxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQzFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztZQUNsQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDbkIsQ0FBQztJQUVELE9BQU87UUFDSCxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDWixJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUNsQixLQUFLLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDbkIsQ0FBQztDQUNKO0FBOUdELDZCQThHQzs7OztBQ3ZIRCw2Q0FBc0M7QUFDdEMsaURBQTBDO0FBRTFDLHFDQUFrQztBQUVsQyxtQ0FBNEI7QUFJNUIsWUFBNEIsU0FBUSxvQkFBVTtJQVUxQzs7Ozs7T0FLRztJQUNILFlBQW1CLFNBQTBCLEVBQVMsU0FBb0I7UUFDdEUsS0FBSyxFQUFFLENBQUM7UUFETyxjQUFTLEdBQVQsU0FBUyxDQUFpQjtRQUFTLGNBQVMsR0FBVCxTQUFTLENBQVc7UUFYbEUsc0JBQWlCLEdBQUcsQ0FBQyxLQUFzQjtZQUMvQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztZQUNwRCxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztRQUN6RCxDQUFDLENBQUE7UUFXRyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksc0JBQVksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN2RSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDakIsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2pCLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQztRQUUzQixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMxQixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO0lBQ3BDLENBQUM7SUFFRCxXQUFXLENBQUMsRUFBYztRQUN0QixFQUFFLENBQUMsQ0FBQyxFQUFFLFlBQVksZUFBSyxDQUFDLENBQUMsQ0FBQztZQUN0QixJQUFJLENBQUMsSUFBSSxDQUFDLGVBQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUM1QixJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDbkIsQ0FBQztJQUNMLENBQUM7SUFFRCxLQUFLO1FBQ0QsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDcEIsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ2pCLEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUNsQixDQUFDO0lBRUQsS0FBSztRQUNELElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ3JCLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNwQixLQUFLLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDbEIsQ0FBQztJQUVELFNBQVM7UUFDTCxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxlQUFNLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFFRCxZQUFZO1FBQ1IsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsZUFBTSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztJQUN4RSxDQUFDO0lBRUQsTUFBTTtRQUNGLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUM3QyxJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDN0MsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ25CLENBQUM7SUFFRCxPQUFPO1FBQ0gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDM0MsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ2pCLENBQUM7Q0FDSjtBQWxFRCx5QkFrRUM7Ozs7QUNsRUQ7SUFJSTs7OztPQUlHO0lBQ0gsWUFBb0IsTUFBbUMsRUFBRSxHQUFHLElBQVc7UUFBbkQsV0FBTSxHQUFOLE1BQU0sQ0FBNkI7UUFDbkQsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDbEIsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7SUFDcEIsQ0FBQztJQUVEOztPQUVHO0lBQ0gsYUFBYTtRQUNULElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztJQUM3QyxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsV0FBVyxDQUFDLEdBQUcsSUFBVztRQUN0QixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxLQUFLLENBQUMsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDM0UsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDO1FBQ3BCLE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVEOztPQUVHO0lBQ0gsTUFBTTtRQUNGLElBQUksUUFBUSxHQUFHLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMxQixNQUFNLENBQUMsUUFBUSxDQUFDO0lBQ3BCLENBQUM7Q0FDSjtBQXZDRCx1QkF1Q0M7Ozs7QUNoREQsbUNBQXNDO0FBQ3RDLHFDQUFrQztBQUVsQyxlQUErQixTQUFRLHFCQUFZO0lBRy9DOzs7O09BSUc7SUFDSCxZQUFZLE1BQThCLEVBQUUsT0FBZTtRQUN2RCxLQUFLLEVBQUUsQ0FBQztRQVJMLGNBQVMsR0FBNkIsRUFBRSxDQUFDO1FBVTVDLElBQUksUUFBUSxHQUFzQixFQUFFLENBQUM7UUFDckMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLO1lBQ2hCLElBQUksT0FBTyxHQUFHLElBQUksT0FBTyxDQUFTLENBQUMsT0FBTyxFQUFFLE1BQU07Z0JBQzlDLElBQUksR0FBRyxHQUFHLE9BQU8sR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDO2dCQUM5QixJQUFJLEdBQUcsR0FBRyxJQUFJLEtBQUssRUFBRSxDQUFDO2dCQUN0QixHQUFHLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztnQkFDZCxJQUFJLE1BQW1CLENBQUM7Z0JBQ3hCLEdBQUcsQ0FBQyxNQUFNLEdBQUc7b0JBQ1QsaUJBQWlCLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsR0FBRyxDQUFDLFlBQVksRUFBRSxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsV0FBVzt3QkFDL0UsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUM7NEJBQ2hCLElBQUksRUFBRSxLQUFLLENBQUMsSUFBSTs0QkFDaEIsR0FBRyxFQUFFLEdBQUc7NEJBQ1IsS0FBSyxFQUFFLFdBQVcsQ0FBQyxLQUFLOzRCQUN4QixNQUFNLEVBQUUsV0FBVyxDQUFDLE1BQU07NEJBQzFCLE1BQU0sRUFBRSxXQUFXO3lCQUN0QixDQUFDLENBQUM7b0JBQ1AsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUM1QixDQUFDLENBQUE7Z0JBQ0QsR0FBRyxDQUFDLE9BQU8sR0FBRyxVQUFVLENBQUM7b0JBQ3JCLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDZCxDQUFDLENBQUM7WUFDTixDQUFDLENBQUMsQ0FBQztZQUNILFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDM0IsQ0FBQyxDQUFDLENBQUM7UUFFSCxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQztZQUN2QixJQUFJLENBQUMsSUFBSSxDQUFDLGVBQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMvQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRDs7O09BR0c7SUFDSSxTQUFTLENBQUMsSUFBWTtRQUN6QixNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLFFBQVEsQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLENBQUM7SUFDbkUsQ0FBQztDQUNKO0FBakRELDRCQWlEQzs7OztBQ3BERCxtREFBNEM7QUFFNUMsVUFBMEIsU0FBUSx1QkFBYTtJQUczQzs7Ozs7T0FLRztJQUNILFlBQW1CLEtBQWEsRUFBUyxLQUFhLEVBQVMsTUFBYztRQUN6RSxLQUFLLEVBQUUsQ0FBQztRQURPLFVBQUssR0FBTCxLQUFLLENBQVE7UUFBUyxVQUFLLEdBQUwsS0FBSyxDQUFRO1FBQVMsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQVJ0RSxXQUFNLEdBQUcsRUFBRSxDQUFDO1FBV2YsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDN0QsQ0FBQztJQUVELE1BQU07UUFDRixJQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDaEMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUNuQyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUM7UUFDdkIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzdELElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUMvRCxDQUFDO0NBQ0o7QUF2QkQsdUJBdUJDOzs7O0FDdkJELGlEQUEwQztBQUMxQywyQ0FBb0M7QUFDcEMsaUNBQTBCO0FBRTFCLGtCQUFrQyxTQUFRLG1CQUFTO0lBQy9DOzs7O09BSUc7SUFDSCxZQUFtQixLQUFZLEVBQVMsU0FBb0I7UUFDeEQsS0FBSyxFQUFFLENBQUM7UUFETyxVQUFLLEdBQUwsS0FBSyxDQUFPO1FBQVMsY0FBUyxHQUFULFNBQVMsQ0FBVztRQUd4RCxnQkFBZ0I7UUFDaEIsSUFBSSxFQUFFLEdBQUcsSUFBSSxzQkFBWSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3JFLElBQUksSUFBSSxHQUFHLElBQUksY0FBSSxDQUFDLFdBQVcsRUFBRSxNQUFNLEVBQUUsZUFBZSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQ3BFLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUNyQyxJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRTFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDbEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN4QixDQUFDO0NBQ0o7QUFsQkQsK0JBa0JDOzs7O0FDeEJELDJDQUFvQztBQUNwQyxpQ0FBMEI7QUFPMUIsV0FBMkIsU0FBUSxtQkFBUztJQWV4QyxZQUFtQixNQUF5QjtRQUN4QyxLQUFLLEVBQUUsQ0FBQztRQURPLFdBQU0sR0FBTixNQUFNLENBQW1CO1FBZDVDOzs7O1dBSUc7UUFDSSxVQUFLLEdBQW9CO1lBQzVCLENBQUMsRUFBRSxDQUFDO1lBQ0osQ0FBQyxFQUFFLENBQUM7U0FDUCxDQUFBO1FBQ00sV0FBTSxHQUFXLENBQUMsQ0FBQztRQUNuQixXQUFNLEdBQVcsQ0FBQyxDQUFDO1FBTXRCLElBQUksQ0FBQyxHQUFHLEdBQUcsTUFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUNuQixJQUFJLENBQUMsRUFBRSxHQUFHLElBQUksY0FBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2xFLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLEtBQUssTUFBTSxJQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3RILElBQUksQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLEtBQUssTUFBTSxJQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3RILElBQUksQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxFQUFFLEtBQUssTUFBTSxJQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN4RyxJQUFJLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxLQUFLLE1BQU0sSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUU5RyxJQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQzNFLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxLQUFpQjtRQUMvQixJQUFJLGtCQUFrQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLHFCQUFxQixFQUFFLENBQUM7UUFDbkUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLEtBQUssR0FBRyxrQkFBa0IsQ0FBQyxJQUFJLENBQUM7UUFDckQsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLEtBQUssR0FBRyxrQkFBa0IsQ0FBQyxHQUFHLENBQUM7SUFDeEQsQ0FBQztJQUVELE1BQU07UUFDRixJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDaEUsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ25CLENBQUM7Q0FDSjtBQXZDRCx3QkF1Q0M7Ozs7QUMvQ0QsbUNBQXNDO0FBQ3RDLHFDQUFrQztBQWlCbEMsa0JBQWtDLFNBQVEscUJBQVk7SUFHbEQ7Ozs7O09BS0c7SUFDSCxZQUFtQixFQUFrQixFQUFTLE1BQTBCLEVBQVMsWUFBb0I7UUFDakcsS0FBSyxFQUFFLENBQUM7UUFETyxPQUFFLEdBQUYsRUFBRSxDQUFnQjtRQUFTLFdBQU0sR0FBTixNQUFNLENBQW9CO1FBQVMsaUJBQVksR0FBWixZQUFZLENBQVE7SUFFckcsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0gsVUFBVSxDQUFDLFVBQWtCO1FBQ3pCLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ25ELEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDSixJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNyQixNQUFNLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUN4QyxJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDekIsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNqQixDQUFDO0lBQ0wsQ0FBQztJQUVEOztPQUVHO0lBQ0gsUUFBUTtRQUNKLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFFRDs7O09BR0c7SUFDSCxJQUFJLENBQUMsQ0FBYTtRQUNkLHNCQUFzQjtRQUN0QixJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsZUFBTSxDQUFDLE9BQU8sRUFBRTtZQUM3QixtQkFBbUI7WUFDbkIsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztZQUNuQyx3QkFBd0I7WUFDeEIsQ0FBQyxDQUFDLFlBQVksRUFBRSxDQUFDO1lBRWpCLDhCQUE4QjtZQUM5QixJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsZUFBTSxDQUFDLFFBQVEsRUFBRTtnQkFDOUIsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztnQkFDbkMsd0NBQXdDO2dCQUN4QyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztvQkFDVixJQUFJLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUMsUUFBUSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsQ0FBQztnQkFDdEYsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3BCLENBQUM7WUFDTCxDQUFDLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQzVCLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDM0IsQ0FBQztDQUNKO0FBOURELCtCQThEQzs7OztBQ2hGRCxtREFBd0Q7QUFFeEQsVUFBMEIsU0FBUSx1QkFBYTtJQUMzQzs7Ozs7O09BTUc7SUFDSCxZQUFtQixJQUFZLEVBQVMsS0FBYSxFQUFTLElBQVksRUFBUyxLQUFhO1FBQzVGLEtBQUssRUFBRSxDQUFDO1FBRE8sU0FBSSxHQUFKLElBQUksQ0FBUTtRQUFTLFVBQUssR0FBTCxLQUFLLENBQVE7UUFBUyxTQUFJLEdBQUosSUFBSSxDQUFRO1FBQVMsVUFBSyxHQUFMLEtBQUssQ0FBUTtRQUc1RixJQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdDLFFBQVEsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztRQUMvQixRQUFRLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUM7UUFDckMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsU0FBUyxDQUFDO1FBQy9CLFFBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLFNBQVMsQ0FBQztRQUNoQyxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ2hDLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxNQUFNLEdBQUcsRUFBRSxLQUFLLEVBQUUsUUFBUSxDQUFDLFdBQVcsRUFBRSxNQUFNLEVBQUUsUUFBUSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQzdFLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3hDLENBQUM7SUFFRCxNQUFNO1FBQ0YsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUNwQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQzFCLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDaEMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUNoQyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ2pELENBQUM7Q0FDSjtBQTdCRCx1QkE2QkM7Ozs7QUMvQkQsbUNBQXNDO0FBRXRDLGFBQWMsU0FBUSxxQkFBWTtJQUc5Qjs7O09BR0c7SUFDSDtRQUNJLEtBQUssRUFBRSxDQUFDO1FBQ1IsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxrQkFBa0IsYUFBcUI7WUFDaEUsTUFBTSxDQUFDLHFCQUFxQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzdCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVNLE1BQU0sS0FBSyxRQUFRO1FBQ3RCLDJEQUEyRDtRQUMzRCxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQzNELENBQUM7Q0FDSjtBQUVZLFFBQUEsTUFBTSxHQUFHO0lBQ2xCLGVBQWU7SUFDZixNQUFNLEVBQUUsTUFBTSxDQUFDLGNBQWMsQ0FBQztJQUM5QixTQUFTLEVBQUUsTUFBTSxDQUFDLGlCQUFpQixDQUFDO0lBQ3BDLFVBQVUsRUFBRSxNQUFNLENBQUMsa0JBQWtCLENBQUM7SUFDdEMsVUFBVSxFQUFFLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQztJQUN0QyxTQUFTLEVBQUUsTUFBTSxDQUFDLGlCQUFpQixDQUFDO0lBQ3BDLE9BQU8sRUFBRSxNQUFNLENBQUMsaUJBQWlCLENBQUM7SUFDbEMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxhQUFhLENBQUM7SUFFNUIsa0JBQWtCO0lBQ2xCLFFBQVEsRUFBRSxNQUFNLENBQUMsa0JBQWtCLENBQUM7SUFDcEMsT0FBTyxFQUFFLE1BQU0sQ0FBQyx3QkFBd0IsQ0FBQztJQUN6QyxRQUFRLEVBQUUsTUFBTSxDQUFDLHdCQUF3QixDQUFDO0lBQzFDLFVBQVUsRUFBRSxNQUFNLENBQUMsa0JBQWtCLENBQUM7SUFDdEMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQztJQUNwQyxJQUFJLEVBQUUsTUFBTSxDQUFDLFlBQVksQ0FBQztJQUMxQixTQUFTLEVBQUUsTUFBTSxDQUFDLGlCQUFpQixDQUFDO0NBQ3ZDLENBQUE7QUFFWSxRQUFBLE1BQU0sR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDO0FBQ3ZDLGNBQU0sQ0FBQyxlQUFlLENBQUMsRUFBRSxDQUFDLENBQUM7Ozs7QUM3QzNCLHFEQUE4QztBQUc5QyxvQkFBb0I7QUFDcEIsTUFBTSxJQUFJLEdBQUcsSUFBSSx3QkFBYyxDQUFvQixRQUFRLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiLy8gQ29weXJpZ2h0IEpveWVudCwgSW5jLiBhbmQgb3RoZXIgTm9kZSBjb250cmlidXRvcnMuXG4vL1xuLy8gUGVybWlzc2lvbiBpcyBoZXJlYnkgZ3JhbnRlZCwgZnJlZSBvZiBjaGFyZ2UsIHRvIGFueSBwZXJzb24gb2J0YWluaW5nIGFcbi8vIGNvcHkgb2YgdGhpcyBzb2Z0d2FyZSBhbmQgYXNzb2NpYXRlZCBkb2N1bWVudGF0aW9uIGZpbGVzICh0aGVcbi8vIFwiU29mdHdhcmVcIiksIHRvIGRlYWwgaW4gdGhlIFNvZnR3YXJlIHdpdGhvdXQgcmVzdHJpY3Rpb24sIGluY2x1ZGluZ1xuLy8gd2l0aG91dCBsaW1pdGF0aW9uIHRoZSByaWdodHMgdG8gdXNlLCBjb3B5LCBtb2RpZnksIG1lcmdlLCBwdWJsaXNoLFxuLy8gZGlzdHJpYnV0ZSwgc3VibGljZW5zZSwgYW5kL29yIHNlbGwgY29waWVzIG9mIHRoZSBTb2Z0d2FyZSwgYW5kIHRvIHBlcm1pdFxuLy8gcGVyc29ucyB0byB3aG9tIHRoZSBTb2Z0d2FyZSBpcyBmdXJuaXNoZWQgdG8gZG8gc28sIHN1YmplY3QgdG8gdGhlXG4vLyBmb2xsb3dpbmcgY29uZGl0aW9uczpcbi8vXG4vLyBUaGUgYWJvdmUgY29weXJpZ2h0IG5vdGljZSBhbmQgdGhpcyBwZXJtaXNzaW9uIG5vdGljZSBzaGFsbCBiZSBpbmNsdWRlZFxuLy8gaW4gYWxsIGNvcGllcyBvciBzdWJzdGFudGlhbCBwb3J0aW9ucyBvZiB0aGUgU29mdHdhcmUuXG4vL1xuLy8gVEhFIFNPRlRXQVJFIElTIFBST1ZJREVEIFwiQVMgSVNcIiwgV0lUSE9VVCBXQVJSQU5UWSBPRiBBTlkgS0lORCwgRVhQUkVTU1xuLy8gT1IgSU1QTElFRCwgSU5DTFVESU5HIEJVVCBOT1QgTElNSVRFRCBUTyBUSEUgV0FSUkFOVElFUyBPRlxuLy8gTUVSQ0hBTlRBQklMSVRZLCBGSVRORVNTIEZPUiBBIFBBUlRJQ1VMQVIgUFVSUE9TRSBBTkQgTk9OSU5GUklOR0VNRU5ULiBJTlxuLy8gTk8gRVZFTlQgU0hBTEwgVEhFIEFVVEhPUlMgT1IgQ09QWVJJR0hUIEhPTERFUlMgQkUgTElBQkxFIEZPUiBBTlkgQ0xBSU0sXG4vLyBEQU1BR0VTIE9SIE9USEVSIExJQUJJTElUWSwgV0hFVEhFUiBJTiBBTiBBQ1RJT04gT0YgQ09OVFJBQ1QsIFRPUlQgT1Jcbi8vIE9USEVSV0lTRSwgQVJJU0lORyBGUk9NLCBPVVQgT0YgT1IgSU4gQ09OTkVDVElPTiBXSVRIIFRIRSBTT0ZUV0FSRSBPUiBUSEVcbi8vIFVTRSBPUiBPVEhFUiBERUFMSU5HUyBJTiBUSEUgU09GVFdBUkUuXG5cbmZ1bmN0aW9uIEV2ZW50RW1pdHRlcigpIHtcbiAgdGhpcy5fZXZlbnRzID0gdGhpcy5fZXZlbnRzIHx8IHt9O1xuICB0aGlzLl9tYXhMaXN0ZW5lcnMgPSB0aGlzLl9tYXhMaXN0ZW5lcnMgfHwgdW5kZWZpbmVkO1xufVxubW9kdWxlLmV4cG9ydHMgPSBFdmVudEVtaXR0ZXI7XG5cbi8vIEJhY2t3YXJkcy1jb21wYXQgd2l0aCBub2RlIDAuMTAueFxuRXZlbnRFbWl0dGVyLkV2ZW50RW1pdHRlciA9IEV2ZW50RW1pdHRlcjtcblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5fZXZlbnRzID0gdW5kZWZpbmVkO1xuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5fbWF4TGlzdGVuZXJzID0gdW5kZWZpbmVkO1xuXG4vLyBCeSBkZWZhdWx0IEV2ZW50RW1pdHRlcnMgd2lsbCBwcmludCBhIHdhcm5pbmcgaWYgbW9yZSB0aGFuIDEwIGxpc3RlbmVycyBhcmVcbi8vIGFkZGVkIHRvIGl0LiBUaGlzIGlzIGEgdXNlZnVsIGRlZmF1bHQgd2hpY2ggaGVscHMgZmluZGluZyBtZW1vcnkgbGVha3MuXG5FdmVudEVtaXR0ZXIuZGVmYXVsdE1heExpc3RlbmVycyA9IDEwO1xuXG4vLyBPYnZpb3VzbHkgbm90IGFsbCBFbWl0dGVycyBzaG91bGQgYmUgbGltaXRlZCB0byAxMC4gVGhpcyBmdW5jdGlvbiBhbGxvd3Ncbi8vIHRoYXQgdG8gYmUgaW5jcmVhc2VkLiBTZXQgdG8gemVybyBmb3IgdW5saW1pdGVkLlxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5zZXRNYXhMaXN0ZW5lcnMgPSBmdW5jdGlvbihuKSB7XG4gIGlmICghaXNOdW1iZXIobikgfHwgbiA8IDAgfHwgaXNOYU4obikpXG4gICAgdGhyb3cgVHlwZUVycm9yKCduIG11c3QgYmUgYSBwb3NpdGl2ZSBudW1iZXInKTtcbiAgdGhpcy5fbWF4TGlzdGVuZXJzID0gbjtcbiAgcmV0dXJuIHRoaXM7XG59O1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLmVtaXQgPSBmdW5jdGlvbih0eXBlKSB7XG4gIHZhciBlciwgaGFuZGxlciwgbGVuLCBhcmdzLCBpLCBsaXN0ZW5lcnM7XG5cbiAgaWYgKCF0aGlzLl9ldmVudHMpXG4gICAgdGhpcy5fZXZlbnRzID0ge307XG5cbiAgLy8gSWYgdGhlcmUgaXMgbm8gJ2Vycm9yJyBldmVudCBsaXN0ZW5lciB0aGVuIHRocm93LlxuICBpZiAodHlwZSA9PT0gJ2Vycm9yJykge1xuICAgIGlmICghdGhpcy5fZXZlbnRzLmVycm9yIHx8XG4gICAgICAgIChpc09iamVjdCh0aGlzLl9ldmVudHMuZXJyb3IpICYmICF0aGlzLl9ldmVudHMuZXJyb3IubGVuZ3RoKSkge1xuICAgICAgZXIgPSBhcmd1bWVudHNbMV07XG4gICAgICBpZiAoZXIgaW5zdGFuY2VvZiBFcnJvcikge1xuICAgICAgICB0aHJvdyBlcjsgLy8gVW5oYW5kbGVkICdlcnJvcicgZXZlbnRcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIC8vIEF0IGxlYXN0IGdpdmUgc29tZSBraW5kIG9mIGNvbnRleHQgdG8gdGhlIHVzZXJcbiAgICAgICAgdmFyIGVyciA9IG5ldyBFcnJvcignVW5jYXVnaHQsIHVuc3BlY2lmaWVkIFwiZXJyb3JcIiBldmVudC4gKCcgKyBlciArICcpJyk7XG4gICAgICAgIGVyci5jb250ZXh0ID0gZXI7XG4gICAgICAgIHRocm93IGVycjtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBoYW5kbGVyID0gdGhpcy5fZXZlbnRzW3R5cGVdO1xuXG4gIGlmIChpc1VuZGVmaW5lZChoYW5kbGVyKSlcbiAgICByZXR1cm4gZmFsc2U7XG5cbiAgaWYgKGlzRnVuY3Rpb24oaGFuZGxlcikpIHtcbiAgICBzd2l0Y2ggKGFyZ3VtZW50cy5sZW5ndGgpIHtcbiAgICAgIC8vIGZhc3QgY2FzZXNcbiAgICAgIGNhc2UgMTpcbiAgICAgICAgaGFuZGxlci5jYWxsKHRoaXMpO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgMjpcbiAgICAgICAgaGFuZGxlci5jYWxsKHRoaXMsIGFyZ3VtZW50c1sxXSk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAzOlxuICAgICAgICBoYW5kbGVyLmNhbGwodGhpcywgYXJndW1lbnRzWzFdLCBhcmd1bWVudHNbMl0pO1xuICAgICAgICBicmVhaztcbiAgICAgIC8vIHNsb3dlclxuICAgICAgZGVmYXVsdDpcbiAgICAgICAgYXJncyA9IEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMSk7XG4gICAgICAgIGhhbmRsZXIuYXBwbHkodGhpcywgYXJncyk7XG4gICAgfVxuICB9IGVsc2UgaWYgKGlzT2JqZWN0KGhhbmRsZXIpKSB7XG4gICAgYXJncyA9IEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMSk7XG4gICAgbGlzdGVuZXJzID0gaGFuZGxlci5zbGljZSgpO1xuICAgIGxlbiA9IGxpc3RlbmVycy5sZW5ndGg7XG4gICAgZm9yIChpID0gMDsgaSA8IGxlbjsgaSsrKVxuICAgICAgbGlzdGVuZXJzW2ldLmFwcGx5KHRoaXMsIGFyZ3MpO1xuICB9XG5cbiAgcmV0dXJuIHRydWU7XG59O1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLmFkZExpc3RlbmVyID0gZnVuY3Rpb24odHlwZSwgbGlzdGVuZXIpIHtcbiAgdmFyIG07XG5cbiAgaWYgKCFpc0Z1bmN0aW9uKGxpc3RlbmVyKSlcbiAgICB0aHJvdyBUeXBlRXJyb3IoJ2xpc3RlbmVyIG11c3QgYmUgYSBmdW5jdGlvbicpO1xuXG4gIGlmICghdGhpcy5fZXZlbnRzKVxuICAgIHRoaXMuX2V2ZW50cyA9IHt9O1xuXG4gIC8vIFRvIGF2b2lkIHJlY3Vyc2lvbiBpbiB0aGUgY2FzZSB0aGF0IHR5cGUgPT09IFwibmV3TGlzdGVuZXJcIiEgQmVmb3JlXG4gIC8vIGFkZGluZyBpdCB0byB0aGUgbGlzdGVuZXJzLCBmaXJzdCBlbWl0IFwibmV3TGlzdGVuZXJcIi5cbiAgaWYgKHRoaXMuX2V2ZW50cy5uZXdMaXN0ZW5lcilcbiAgICB0aGlzLmVtaXQoJ25ld0xpc3RlbmVyJywgdHlwZSxcbiAgICAgICAgICAgICAgaXNGdW5jdGlvbihsaXN0ZW5lci5saXN0ZW5lcikgP1xuICAgICAgICAgICAgICBsaXN0ZW5lci5saXN0ZW5lciA6IGxpc3RlbmVyKTtcblxuICBpZiAoIXRoaXMuX2V2ZW50c1t0eXBlXSlcbiAgICAvLyBPcHRpbWl6ZSB0aGUgY2FzZSBvZiBvbmUgbGlzdGVuZXIuIERvbid0IG5lZWQgdGhlIGV4dHJhIGFycmF5IG9iamVjdC5cbiAgICB0aGlzLl9ldmVudHNbdHlwZV0gPSBsaXN0ZW5lcjtcbiAgZWxzZSBpZiAoaXNPYmplY3QodGhpcy5fZXZlbnRzW3R5cGVdKSlcbiAgICAvLyBJZiB3ZSd2ZSBhbHJlYWR5IGdvdCBhbiBhcnJheSwganVzdCBhcHBlbmQuXG4gICAgdGhpcy5fZXZlbnRzW3R5cGVdLnB1c2gobGlzdGVuZXIpO1xuICBlbHNlXG4gICAgLy8gQWRkaW5nIHRoZSBzZWNvbmQgZWxlbWVudCwgbmVlZCB0byBjaGFuZ2UgdG8gYXJyYXkuXG4gICAgdGhpcy5fZXZlbnRzW3R5cGVdID0gW3RoaXMuX2V2ZW50c1t0eXBlXSwgbGlzdGVuZXJdO1xuXG4gIC8vIENoZWNrIGZvciBsaXN0ZW5lciBsZWFrXG4gIGlmIChpc09iamVjdCh0aGlzLl9ldmVudHNbdHlwZV0pICYmICF0aGlzLl9ldmVudHNbdHlwZV0ud2FybmVkKSB7XG4gICAgaWYgKCFpc1VuZGVmaW5lZCh0aGlzLl9tYXhMaXN0ZW5lcnMpKSB7XG4gICAgICBtID0gdGhpcy5fbWF4TGlzdGVuZXJzO1xuICAgIH0gZWxzZSB7XG4gICAgICBtID0gRXZlbnRFbWl0dGVyLmRlZmF1bHRNYXhMaXN0ZW5lcnM7XG4gICAgfVxuXG4gICAgaWYgKG0gJiYgbSA+IDAgJiYgdGhpcy5fZXZlbnRzW3R5cGVdLmxlbmd0aCA+IG0pIHtcbiAgICAgIHRoaXMuX2V2ZW50c1t0eXBlXS53YXJuZWQgPSB0cnVlO1xuICAgICAgY29uc29sZS5lcnJvcignKG5vZGUpIHdhcm5pbmc6IHBvc3NpYmxlIEV2ZW50RW1pdHRlciBtZW1vcnkgJyArXG4gICAgICAgICAgICAgICAgICAgICdsZWFrIGRldGVjdGVkLiAlZCBsaXN0ZW5lcnMgYWRkZWQuICcgK1xuICAgICAgICAgICAgICAgICAgICAnVXNlIGVtaXR0ZXIuc2V0TWF4TGlzdGVuZXJzKCkgdG8gaW5jcmVhc2UgbGltaXQuJyxcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fZXZlbnRzW3R5cGVdLmxlbmd0aCk7XG4gICAgICBpZiAodHlwZW9mIGNvbnNvbGUudHJhY2UgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgLy8gbm90IHN1cHBvcnRlZCBpbiBJRSAxMFxuICAgICAgICBjb25zb2xlLnRyYWNlKCk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHRoaXM7XG59O1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLm9uID0gRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5hZGRMaXN0ZW5lcjtcblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5vbmNlID0gZnVuY3Rpb24odHlwZSwgbGlzdGVuZXIpIHtcbiAgaWYgKCFpc0Z1bmN0aW9uKGxpc3RlbmVyKSlcbiAgICB0aHJvdyBUeXBlRXJyb3IoJ2xpc3RlbmVyIG11c3QgYmUgYSBmdW5jdGlvbicpO1xuXG4gIHZhciBmaXJlZCA9IGZhbHNlO1xuXG4gIGZ1bmN0aW9uIGcoKSB7XG4gICAgdGhpcy5yZW1vdmVMaXN0ZW5lcih0eXBlLCBnKTtcblxuICAgIGlmICghZmlyZWQpIHtcbiAgICAgIGZpcmVkID0gdHJ1ZTtcbiAgICAgIGxpc3RlbmVyLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG4gICAgfVxuICB9XG5cbiAgZy5saXN0ZW5lciA9IGxpc3RlbmVyO1xuICB0aGlzLm9uKHR5cGUsIGcpO1xuXG4gIHJldHVybiB0aGlzO1xufTtcblxuLy8gZW1pdHMgYSAncmVtb3ZlTGlzdGVuZXInIGV2ZW50IGlmZiB0aGUgbGlzdGVuZXIgd2FzIHJlbW92ZWRcbkV2ZW50RW1pdHRlci5wcm90b3R5cGUucmVtb3ZlTGlzdGVuZXIgPSBmdW5jdGlvbih0eXBlLCBsaXN0ZW5lcikge1xuICB2YXIgbGlzdCwgcG9zaXRpb24sIGxlbmd0aCwgaTtcblxuICBpZiAoIWlzRnVuY3Rpb24obGlzdGVuZXIpKVxuICAgIHRocm93IFR5cGVFcnJvcignbGlzdGVuZXIgbXVzdCBiZSBhIGZ1bmN0aW9uJyk7XG5cbiAgaWYgKCF0aGlzLl9ldmVudHMgfHwgIXRoaXMuX2V2ZW50c1t0eXBlXSlcbiAgICByZXR1cm4gdGhpcztcblxuICBsaXN0ID0gdGhpcy5fZXZlbnRzW3R5cGVdO1xuICBsZW5ndGggPSBsaXN0Lmxlbmd0aDtcbiAgcG9zaXRpb24gPSAtMTtcblxuICBpZiAobGlzdCA9PT0gbGlzdGVuZXIgfHxcbiAgICAgIChpc0Z1bmN0aW9uKGxpc3QubGlzdGVuZXIpICYmIGxpc3QubGlzdGVuZXIgPT09IGxpc3RlbmVyKSkge1xuICAgIGRlbGV0ZSB0aGlzLl9ldmVudHNbdHlwZV07XG4gICAgaWYgKHRoaXMuX2V2ZW50cy5yZW1vdmVMaXN0ZW5lcilcbiAgICAgIHRoaXMuZW1pdCgncmVtb3ZlTGlzdGVuZXInLCB0eXBlLCBsaXN0ZW5lcik7XG5cbiAgfSBlbHNlIGlmIChpc09iamVjdChsaXN0KSkge1xuICAgIGZvciAoaSA9IGxlbmd0aDsgaS0tID4gMDspIHtcbiAgICAgIGlmIChsaXN0W2ldID09PSBsaXN0ZW5lciB8fFxuICAgICAgICAgIChsaXN0W2ldLmxpc3RlbmVyICYmIGxpc3RbaV0ubGlzdGVuZXIgPT09IGxpc3RlbmVyKSkge1xuICAgICAgICBwb3NpdGlvbiA9IGk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH1cblxuICAgIGlmIChwb3NpdGlvbiA8IDApXG4gICAgICByZXR1cm4gdGhpcztcblxuICAgIGlmIChsaXN0Lmxlbmd0aCA9PT0gMSkge1xuICAgICAgbGlzdC5sZW5ndGggPSAwO1xuICAgICAgZGVsZXRlIHRoaXMuX2V2ZW50c1t0eXBlXTtcbiAgICB9IGVsc2Uge1xuICAgICAgbGlzdC5zcGxpY2UocG9zaXRpb24sIDEpO1xuICAgIH1cblxuICAgIGlmICh0aGlzLl9ldmVudHMucmVtb3ZlTGlzdGVuZXIpXG4gICAgICB0aGlzLmVtaXQoJ3JlbW92ZUxpc3RlbmVyJywgdHlwZSwgbGlzdGVuZXIpO1xuICB9XG5cbiAgcmV0dXJuIHRoaXM7XG59O1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLnJlbW92ZUFsbExpc3RlbmVycyA9IGZ1bmN0aW9uKHR5cGUpIHtcbiAgdmFyIGtleSwgbGlzdGVuZXJzO1xuXG4gIGlmICghdGhpcy5fZXZlbnRzKVxuICAgIHJldHVybiB0aGlzO1xuXG4gIC8vIG5vdCBsaXN0ZW5pbmcgZm9yIHJlbW92ZUxpc3RlbmVyLCBubyBuZWVkIHRvIGVtaXRcbiAgaWYgKCF0aGlzLl9ldmVudHMucmVtb3ZlTGlzdGVuZXIpIHtcbiAgICBpZiAoYXJndW1lbnRzLmxlbmd0aCA9PT0gMClcbiAgICAgIHRoaXMuX2V2ZW50cyA9IHt9O1xuICAgIGVsc2UgaWYgKHRoaXMuX2V2ZW50c1t0eXBlXSlcbiAgICAgIGRlbGV0ZSB0aGlzLl9ldmVudHNbdHlwZV07XG4gICAgcmV0dXJuIHRoaXM7XG4gIH1cblxuICAvLyBlbWl0IHJlbW92ZUxpc3RlbmVyIGZvciBhbGwgbGlzdGVuZXJzIG9uIGFsbCBldmVudHNcbiAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPT09IDApIHtcbiAgICBmb3IgKGtleSBpbiB0aGlzLl9ldmVudHMpIHtcbiAgICAgIGlmIChrZXkgPT09ICdyZW1vdmVMaXN0ZW5lcicpIGNvbnRpbnVlO1xuICAgICAgdGhpcy5yZW1vdmVBbGxMaXN0ZW5lcnMoa2V5KTtcbiAgICB9XG4gICAgdGhpcy5yZW1vdmVBbGxMaXN0ZW5lcnMoJ3JlbW92ZUxpc3RlbmVyJyk7XG4gICAgdGhpcy5fZXZlbnRzID0ge307XG4gICAgcmV0dXJuIHRoaXM7XG4gIH1cblxuICBsaXN0ZW5lcnMgPSB0aGlzLl9ldmVudHNbdHlwZV07XG5cbiAgaWYgKGlzRnVuY3Rpb24obGlzdGVuZXJzKSkge1xuICAgIHRoaXMucmVtb3ZlTGlzdGVuZXIodHlwZSwgbGlzdGVuZXJzKTtcbiAgfSBlbHNlIGlmIChsaXN0ZW5lcnMpIHtcbiAgICAvLyBMSUZPIG9yZGVyXG4gICAgd2hpbGUgKGxpc3RlbmVycy5sZW5ndGgpXG4gICAgICB0aGlzLnJlbW92ZUxpc3RlbmVyKHR5cGUsIGxpc3RlbmVyc1tsaXN0ZW5lcnMubGVuZ3RoIC0gMV0pO1xuICB9XG4gIGRlbGV0ZSB0aGlzLl9ldmVudHNbdHlwZV07XG5cbiAgcmV0dXJuIHRoaXM7XG59O1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLmxpc3RlbmVycyA9IGZ1bmN0aW9uKHR5cGUpIHtcbiAgdmFyIHJldDtcbiAgaWYgKCF0aGlzLl9ldmVudHMgfHwgIXRoaXMuX2V2ZW50c1t0eXBlXSlcbiAgICByZXQgPSBbXTtcbiAgZWxzZSBpZiAoaXNGdW5jdGlvbih0aGlzLl9ldmVudHNbdHlwZV0pKVxuICAgIHJldCA9IFt0aGlzLl9ldmVudHNbdHlwZV1dO1xuICBlbHNlXG4gICAgcmV0ID0gdGhpcy5fZXZlbnRzW3R5cGVdLnNsaWNlKCk7XG4gIHJldHVybiByZXQ7XG59O1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLmxpc3RlbmVyQ291bnQgPSBmdW5jdGlvbih0eXBlKSB7XG4gIGlmICh0aGlzLl9ldmVudHMpIHtcbiAgICB2YXIgZXZsaXN0ZW5lciA9IHRoaXMuX2V2ZW50c1t0eXBlXTtcblxuICAgIGlmIChpc0Z1bmN0aW9uKGV2bGlzdGVuZXIpKVxuICAgICAgcmV0dXJuIDE7XG4gICAgZWxzZSBpZiAoZXZsaXN0ZW5lcilcbiAgICAgIHJldHVybiBldmxpc3RlbmVyLmxlbmd0aDtcbiAgfVxuICByZXR1cm4gMDtcbn07XG5cbkV2ZW50RW1pdHRlci5saXN0ZW5lckNvdW50ID0gZnVuY3Rpb24oZW1pdHRlciwgdHlwZSkge1xuICByZXR1cm4gZW1pdHRlci5saXN0ZW5lckNvdW50KHR5cGUpO1xufTtcblxuZnVuY3Rpb24gaXNGdW5jdGlvbihhcmcpIHtcbiAgcmV0dXJuIHR5cGVvZiBhcmcgPT09ICdmdW5jdGlvbic7XG59XG5cbmZ1bmN0aW9uIGlzTnVtYmVyKGFyZykge1xuICByZXR1cm4gdHlwZW9mIGFyZyA9PT0gJ251bWJlcic7XG59XG5cbmZ1bmN0aW9uIGlzT2JqZWN0KGFyZykge1xuICByZXR1cm4gdHlwZW9mIGFyZyA9PT0gJ29iamVjdCcgJiYgYXJnICE9PSBudWxsO1xufVxuXG5mdW5jdGlvbiBpc1VuZGVmaW5lZChhcmcpIHtcbiAgcmV0dXJuIGFyZyA9PT0gdm9pZCAwO1xufVxuIiwiaW1wb3J0IHsgVGlja2VyLCBFdmVudHMgfSBmcm9tIFwiLi9UaWNrZXJcIjtcclxuaW1wb3J0IHsgRWFzaW5nLCBFYXNpbmdzIH0gZnJvbSBcIi4vRWFzaW5nc1wiO1xyXG5pbXBvcnQgeyBFdmVudEVtaXR0ZXIgfSBmcm9tIFwiZXZlbnRzXCI7XHJcblxyXG4vKipcclxuICogYWFhYVxyXG4gKi9cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQW5pbWF0aW9uIGV4dGVuZHMgRXZlbnRFbWl0dGVyIHtcclxuICAgIGZyb206IE51bWVyaWNQcm9wZXJ0eU9iamVjdDtcclxuXHJcbiAgICBwcml2YXRlIF9sb29wOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBwcml2YXRlIF9lYXNpbmc6IEVhc2luZyA9IEVhc2luZ3MubGluZWFyO1xyXG4gICAgcHJpdmF0ZSBfc3RhcnRUaW1lOiBudW1iZXI7XHJcbiAgICBwcml2YXRlIF9kdXJhdGlvbjogbnVtYmVyO1xyXG4gICAgcHJpdmF0ZSBfdXBkYXRlUHJveHkgPSAoKSA9PiB0aGlzLnVwZGF0ZSgpO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQW5pbWF0ZXMgb2JqZWN0IHByb3BlcnRpZXMgb3ZlciB0aW1lLiBDYW4gbG9vcCBpZiBuZWNlc3NhcnkuXHJcbiAgICAgKiBAcGFyYW0gZHVyYXRpb24gZHVyYXRpb24gb2YgdGhlIGFuaW1hdGlvbiBpbiBzZWNvbmRzXHJcbiAgICAgKiBAcGFyYW0gdGFyZ2V0IHRhcmdldCBvYmplY3Qgd2hvcyBwcm9wZXJ0aWVzIHdpbGwgYmUgYW5pbWF0ZWRcclxuICAgICAqIEBwYXJhbSB0byBwcm9wZXJ0eSB2YWx1ZXMgdGhlIHRhcmdldCdzIHByb3BlcnRpZXMgd2lsbCBiZSBhbmltYXRlZCB0b1xyXG4gICAgICogQHBhcmFtIGNvbmZpZyBhZGRpdGlvbmFsIGNvbmZpZ3VyYXRpb24uXHJcbiAgICAgKiBsb29wIC0gbG9vcHMgdGhlIGFuaW1hdGlvbi5cclxuICAgICAqIGZyb20gLSBzcGVjaWZpZXMgc3RhcnRpbmcgcHJvcGVydGllcy4gaWYgbm90IHNwZWNpZmllZCwgdGhlIGN1cnJlbnQgcHJvcGVydHkgdmFsdWVzIHdpbGwgYmUgdXNlZCBpbnN0ZWFkLlxyXG4gICAgICogZWFzaW5nIC0gZWFzaW5nIGZ1bmNpdG9uXHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBkdXJhdGlvbjogbnVtYmVyLCBwdWJsaWMgdGFyZ2V0OiBPYmplY3QsIHB1YmxpYyB0bzogTnVtZXJpY1Byb3BlcnR5T2JqZWN0LCBwdWJsaWMgY29uZmlnOiBBbmltYXRpb25Db25maWcgPSB7fSkge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICAgICAgdGhpcy5yZXNldCgpO1xyXG4gICAgICAgIHRoaXMuX2Vhc2luZyA9IGNvbmZpZy5lYXNpbmcgfHwgRWFzaW5ncy5saW5lYXI7XHJcbiAgICAgICAgdGhpcy5fZHVyYXRpb24gPSB0aGlzLmR1cmF0aW9uICogMTAwMDtcclxuICAgICAgICB0aGlzLl9sb29wID0gY29uZmlnLmxvb3A7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXNldHMgc3RhcnRpbmcgdGltZSBhbmQgcHJvcGVydGllcyB0byB0aGUgb3JpZ2luYWwgc3RhcnRpbmcgdmFsdWVzLiBJZiBubyBmcm9tIG9iamVjdCBpcyBnaXZlbiwgdGhlIGN1cnJlbnRcclxuICAgICAqIHZhbHVlcyB3aWxsIGJlIHVzZWQgdG8gY3JlYXRlIHRoZSBmcm9tIG9iamVjdC5cclxuICAgICAqL1xyXG4gICAgcmVzZXQoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY29uZmlnLmZyb20pIHtcclxuICAgICAgICAgICAgdGhpcy5mcm9tID0gdGhpcy5jb25maWcuZnJvbTtcclxuICAgICAgICAgICAgZm9yIChjb25zdCBwcm9wIG9mIE9iamVjdC5rZXlzKHRoaXMuZnJvbSkpIHtcclxuICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgdGhpcy5mcm9tW3Byb3BdID09PSAnbnVtYmVyJykge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGFyZ2V0W3Byb3BdID0gdGhpcy5mcm9tW3Byb3BdO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5mcm9tID0ge307XHJcbiAgICAgICAgICAgIGZvciAoY29uc3QgcHJvcCBvZiBPYmplY3Qua2V5cyh0aGlzLnRvKSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHR5cGVvZiB0aGlzLnRhcmdldFtwcm9wXSA9PT0gJ251bWJlcicpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmZyb21bcHJvcF0gPSB0aGlzLnRhcmdldFtwcm9wXTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLl9zdGFydFRpbWUgPSBEYXRlLm5vdygpO1xyXG4gICAgfVxyXG5cclxuICAgIHBsYXkoKSB7XHJcbiAgICAgICAgdGhpcy5yZXNldCgpO1xyXG4gICAgICAgIFRpY2tlci5vbihFdmVudHMuVVBEQVRFLCB0aGlzLl91cGRhdGVQcm94eSk7XHJcbiAgICB9XHJcblxyXG4gICAgc3RvcCgpIHtcclxuICAgICAgICBUaWNrZXIucmVtb3ZlTGlzdGVuZXIoRXZlbnRzLlVQREFURSwgdGhpcy5fdXBkYXRlUHJveHkpO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZSgpIHtcclxuICAgICAgICBsZXQgY3VycmVudFRpbWUgPSBEYXRlLm5vdygpIC0gdGhpcy5fc3RhcnRUaW1lO1xyXG4gICAgICAgIGZvciAoY29uc3QgcHJvcCBvZiBPYmplY3Qua2V5cyh0aGlzLnRvKSkge1xyXG4gICAgICAgICAgICB0aGlzLnRhcmdldFtwcm9wXSA9IHRoaXMuZnJvbVtwcm9wXSArICh0aGlzLnRvW3Byb3BdIC0gdGhpcy5mcm9tW3Byb3BdKSAqIHRoaXMuX2Vhc2luZyhNYXRoLm1pbihjdXJyZW50VGltZSAvIHRoaXMuX2R1cmF0aW9uLCAxKSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChjdXJyZW50VGltZSA+PSB0aGlzLl9kdXJhdGlvbikge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5fbG9vcCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yZXNldCgpXHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnN0b3AoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZW1pdChFdmVudHMuQ09NUExFVEUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4vKipcclxuICogRW5zdXJlcyB0aGF0IG9ubHkgbnVtZXJpYyBwcm9wZXJ0aWVzIGFyZSBhbmltYXRlZFxyXG4gKi9cclxuaW50ZXJmYWNlIE51bWVyaWNQcm9wZXJ0eU9iamVjdCB7XHJcbiAgICBbaW5kZXg6IHN0cmluZ106IG51bWJlclxyXG59XHJcblxyXG4vKipcclxuICogRGVmaW5lcyB0aGUgY29uZmlnIHBhcmFtZXRlcyBmb3IgdGhlIEFuaW1hdGlvbiBjbGFzc1xyXG4gKi9cclxuaW50ZXJmYWNlIEFuaW1hdGlvbkNvbmZpZyB7XHJcbiAgICBmcm9tPzogTnVtZXJpY1Byb3BlcnR5T2JqZWN0XHJcbiAgICBsb29wPzogYm9vbGVhblxyXG4gICAgZWFzaW5nPzogRWFzaW5nXHJcbn0iLCJpbXBvcnQgR2FtZU9iamVjdCBmcm9tIFwiLi9HYW1lT2JqZWN0XCI7XHJcbmltcG9ydCBQcmVsb2FkZXIgZnJvbSBcIi4vUHJlbG9hZGVyXCI7XHJcbmltcG9ydCBHcmFwaGljSW1hZ2UgZnJvbSBcIi4vR3JhcGhpY0ltYWdlXCI7XHJcbmltcG9ydCBQbGF5ZXIgZnJvbSBcIi4vUGxheWVyXCI7XHJcbmltcG9ydCBFbmVteSBmcm9tIFwiLi9FbmVteVwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQnVsbGV0IGV4dGVuZHMgR2FtZU9iamVjdCB7XHJcbiAgICBpbWFnZTogR3JhcGhpY0ltYWdlO1xyXG4gICAgc3BlZWQgPSAzMDtcclxuXHJcbiAgICAvKipcclxuICAgICAqIENyZWF0ZXMgYSBidWxsZXQgd2hpY2ggdHJhdmVsIGFsb25nIHRoZSB4IGF4aXMuXHJcbiAgICAgKiBAcGFyYW0gcHJlbG9hZGVyIFVzZWQgdG8gZ2V0IHRoZSBidWxsZXQgaW1hZ2VcclxuICAgICAqIEBwYXJhbSBwbGF5ZXIgVXNlZCB0byBnZXQgc3RhcnRpbmcgY29vcmRpbmF0ZXNcclxuICAgICAqL1xyXG4gICAgY29uc3RydWN0b3IocHVibGljIHByZWxvYWRlcjogUHJlbG9hZGVyLCBwdWJsaWMgcGxheWVyOiBQbGF5ZXIpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG5cclxuICAgICAgICB0aGlzLmltYWdlID0gbmV3IEdyYXBoaWNJbWFnZShwcmVsb2FkZXIuZ2V0QnlOYW1lKCdidWxsZXQnKS5iaXRtYXApO1xyXG4gICAgICAgIHRoaXMuYm91bmRzID0gdGhpcy5pbWFnZS5ib3VuZHM7XHJcblxyXG4gICAgICAgIHRoaXMuYWRkQ2hpbGQodGhpcy5pbWFnZSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25Db2xsaXNpb24oZ286IEdhbWVPYmplY3QpIHtcclxuICAgICAgICBpZiAoZ28gaW5zdGFuY2VvZiBFbmVteSkge1xyXG4gICAgICAgICAgICB0aGlzLmRlc3Ryb3koKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcmVzZXQoKSB7XHJcbiAgICAgICAgdGhpcy5vcGFjaXR5ID0gMDtcclxuICAgICAgICB0aGlzLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgIHN1cGVyLnJlc2V0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhcnQoKSB7XHJcbiAgICAgICAgdGhpcy5vcGFjaXR5ID0gMTtcclxuICAgICAgICB0aGlzLnggPSB0aGlzLnBsYXllci54O1xyXG4gICAgICAgIHRoaXMueSA9IHRoaXMucGxheWVyLnk7XHJcbiAgICAgICAgaWYgKHRoaXMuYWN0aXZlKSByZXR1cm47XHJcbiAgICAgICAgdGhpcy5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIHN1cGVyLnN0YXJ0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlKCkge1xyXG4gICAgICAgIGlmICghdGhpcy5hY3RpdmUpIHJldHVybjtcclxuICAgICAgICB0aGlzLnggKz0gdGhpcy5zcGVlZDtcclxuICAgICAgICBpZiAodGhpcy54ID4gdGhpcy5zdGFnZS5ib3VuZHMud2lkdGgpIHtcclxuICAgICAgICAgICAgdGhpcy5kZXN0cm95KCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHN1cGVyLnVwZGF0ZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGRlc3Ryb3koKSB7XHJcbiAgICAgICAgdGhpcy5yZXNldCgpO1xyXG4gICAgICAgIHN1cGVyLmRlc3Ryb3koKTtcclxuICAgIH1cclxufSIsImltcG9ydCBEaXNwbGF5T2JqZWN0IGZyb20gXCIuL0Rpc3BsYXlPYmplY3RcIjtcclxuaW1wb3J0IFJlY3QgZnJvbSBcIi4vUmVjdFwiO1xyXG5pbXBvcnQgVGV4dCBmcm9tIFwiLi9UZXh0XCI7XHJcbmltcG9ydCBDb250YWluZXIgZnJvbSBcIi4vQ29udGFpbmVyXCI7XHJcbmltcG9ydCB7IEV2ZW50cyB9IGZyb20gXCIuL1RpY2tlclwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQnV0dG9uIGV4dGVuZHMgQ29udGFpbmVyIHtcclxuICAgIHByaXZhdGUgX3JlY3Q6IFJlY3Q7XHJcbiAgICBwcml2YXRlIF90ZXh0OiBUZXh0O1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ3JlYXRlcyBhIGJ1dHRvbiB3aXRoIHRoZSBnaXZlbiB0ZXh0XHJcbiAgICAgKiBAcGFyYW0gdGV4dCBCdXR0b24gdGV4dFxyXG4gICAgICovXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgdGV4dDogc3RyaW5nKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuXHJcbiAgICAgICAgLy8gYWRkaW5nIGJ1dHRvbiBlbGVtZW50c1xyXG4gICAgICAgIHRoaXMuX3RleHQgPSBuZXcgVGV4dCh0aGlzLnRleHQsICcjZmZmJywgJzE2cHggT3JiaXRyb24nLCAnY2VudGVyJyk7XHJcbiAgICAgICAgdGhpcy5fdGV4dC54ID0gMDtcclxuICAgICAgICB0aGlzLl90ZXh0LnkgPSA3O1xyXG4gICAgICAgIHRoaXMuX3JlY3QgPSBuZXcgUmVjdCgnIzAwMCcsIDI1MCwgdGhpcy5fdGV4dC5ib3VuZHMuaGVpZ2h0ICsgMTUpO1xyXG4gICAgICAgIHRoaXMuX3JlY3Quc3Ryb2tlID0gJyNmZmYnO1xyXG4gICAgICAgIHRoaXMuX3JlY3QueCA9IC10aGlzLl9yZWN0LmJvdW5kcy53aWR0aCAvIDI7XHJcbiAgICAgICAgdGhpcy5fcmVjdC55ID0gLXRoaXMuX3JlY3QuYm91bmRzLmhlaWdodCAvIDI7XHJcbiAgICAgICAgdGhpcy5fcmVjdC5vcGFjaXR5ID0gMC4zO1xyXG4gICAgICAgIHRoaXMuYWRkQ2hpbGQodGhpcy5fcmVjdCk7XHJcbiAgICAgICAgdGhpcy5hZGRDaGlsZCh0aGlzLl90ZXh0KTtcclxuXHJcbiAgICAgICAgLy8gbW91c2UgZXZlbnRzXHJcbiAgICAgICAgdGhpcy5fcmVjdC5vbihFdmVudHMuTU9VU0VFTlRFUiwgKGV2ZW50KSA9PiB7IHRoaXMub25Nb3VzZUVudGVyKGV2ZW50KSB9KTtcclxuICAgICAgICB0aGlzLl9yZWN0Lm9uKEV2ZW50cy5NT1VTRUxFQVZFLCAoZXZlbnQpID0+IHsgdGhpcy5vbk1vdXNlTGVhdmUoZXZlbnQpIH0pO1xyXG4gICAgICAgIHRoaXMuX3JlY3Qub24oRXZlbnRzLk1PVVNFRE9XTiwgKGV2ZW50KSA9PiB7IHRoaXMub25Nb3VzZURvd24oZXZlbnQpIH0pO1xyXG4gICAgICAgIHRoaXMuX3JlY3Qub24oRXZlbnRzLk1PVVNFVVAsIChldmVudCkgPT4geyB0aGlzLm9uTW91c2VVcCgpIH0pO1xyXG4gICAgICAgIHRoaXMuX3JlY3Qub24oRXZlbnRzLkNMSUNLLCAoZXZlbnQpID0+IHsgdGhpcy5vbkNsaWNrKGV2ZW50KSB9KTtcclxuICAgIH1cclxuXHJcbiAgICBvbk1vdXNlRW50ZXIoZXZlbnQ6IE1vdXNlRXZlbnQpIHtcclxuICAgICAgICB0aGlzLl9yZWN0LmNvbG9yID0gJyM0Mjg2ZjQnO1xyXG4gICAgICAgIHRoaXMuX3JlY3Qub3BhY2l0eSA9IDAuNTtcclxuICAgIH1cclxuXHJcbiAgICBvbk1vdXNlTGVhdmUoZXZlbnQ6IE1vdXNlRXZlbnQpIHtcclxuICAgICAgICB0aGlzLl9yZWN0LmNvbG9yID0gJyMwMDAnO1xyXG4gICAgICAgIHRoaXMuX3JlY3Qub3BhY2l0eSA9IDAuMztcclxuICAgIH1cclxuXHJcbiAgICBvbk1vdXNlRG93bihldmVudDogTW91c2VFdmVudCkge1xyXG4gICAgICAgIHRoaXMuX3JlY3QuY29sb3IgPSAnIzJlNjNiYSc7XHJcbiAgICB9XHJcblxyXG4gICAgb25Nb3VzZVVwKCkge1xyXG4gICAgICAgIHRoaXMuX3JlY3QuY29sb3IgPSAnIzQyODZmNCc7XHJcbiAgICB9XHJcblxyXG4gICAgb25DbGljayhldmVudDogTW91c2VFdmVudCkge1xyXG4gICAgICAgIHRoaXMuZW1pdChFdmVudHMuQ0xJQ0ssIGV2ZW50KTtcclxuICAgIH1cclxuXHJcbiAgICBkZXN0cm95KCkge1xyXG4gICAgICAgIHRoaXMuX3JlY3QucmVtb3ZlQWxsTGlzdGVuZXJzKEV2ZW50cy5NT1VTRUVOVEVSKTtcclxuICAgICAgICB0aGlzLl9yZWN0LnJlbW92ZUFsbExpc3RlbmVycyhFdmVudHMuTU9VU0VMRUFWRSk7XHJcblxyXG4gICAgICAgIHRoaXMucmVtb3ZlKCk7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQgR3JpZCBmcm9tIFwiLi9Db2xsaXNpb25HcmlkXCI7XHJcbmltcG9ydCBDb250YWluZXIgZnJvbSBcIi4vQ29udGFpbmVyXCI7XHJcbmltcG9ydCBHYW1lT2JqZWN0IGZyb20gXCIuL0dhbWVPYmplY3RcIjtcclxuaW1wb3J0IHsgRXZlbnRzIH0gZnJvbSBcIi4vVGlja2VyXCI7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIEdyaWRQb3NpdGlvbiB7XHJcbiAgICB4OiBudW1iZXIsXHJcbiAgICB5OiBudW1iZXJcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ29sbGlzaW9uRGV0ZWN0b3Ige1xyXG4gICAgZ3JpZHM6IEdyaWRbXSA9IFtdOyAvLyBpbnN0YW50aWF0ZWQgZ3JpZHMgcGxhY2VkIGhlcmVcclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbGxpc2lvbiBkZXRlY3Rpb24gaXMgY29zdGx5IGZvciBsYXJnZSBudW1iZXIgb2Ygb2JqZWN0cy4gVGhpcyBncmlkIGJhc2VkIGRldGVjdGlvblxyXG4gICAgICogd2lsbCBjYXVzZSBsZXNzIGNhbGN1bGF0aW9uIGJlY2F1c2Ugb2JqZWN0cyB3aWxsIG9ubHkgYmUgY2FsY3VsYXRlZCBpZiB0aGV5IGFyZSBpblxyXG4gICAgICogdGhlIHNhbWUgcGFydCBvZiB0aGUgc2NyZWVuLiBUaGUgc2l6ZSBvZiB0aGVzZSBwYXJ0cyBhcmUgZGV0ZXJtaW5lZCBieSB0aGUgc2l6ZSBhbmRcclxuICAgICAqIGxldmVscyBwYXJhbWV0ZXJzLlxyXG4gICAgICogQHBhcmFtIGxldmVscyBudW1iZXIgb2YgZ3JpZHMuIGV2ZXJ5IGhpZ2hlciBsZXZlbCBncmlkIHdpbGwgaGF2ZSB0d2ljZSBhcyBhbHJnZSBjZWxscyBhcyB0aGUgbG93ZXIgbGV2ZXIgZ3JpZC5cclxuICAgICAqIEBwYXJhbSBzaXplIGJhc2UgZ3JpZCBjZWxsIHNpemUgaW4gcGl4ZWxzLiB0aGUgZmlyc3QgbGV2ZWwgd2lsbCBoYXZlIHRoaXMgY2VsbCBzaXplLiB0aGUgc2Vjb25kIGxldmVsIHdpbGwgaGF2ZSB0d2ljZSBhIGxhcmdlLCBldGMuXHJcbiAgICAgKiBAcGFyYW0gY29udGFpbmVyIENvbnRhaW5lciBvYmplY3Qgd2hpY2ggdGhlIGdyaWQgaXMgYXBwbGllZCB0by5cclxuICAgICAqL1xyXG4gICAgY29uc3RydWN0b3IocHVibGljIGxldmVsczogbnVtYmVyLCBwdWJsaWMgc2l6ZTogbnVtYmVyID0gMTUwLCBwdWJsaWMgY29udGFpbmVyOiBDb250YWluZXIpIHtcclxuICAgICAgICAvLyBjcmVhdGUgZ3JpZHNcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGxldmVsczsgaSsrKSB7XHJcbiAgICAgICAgICAgIC8vIGNyZWF0ZSBncmlkXHJcbiAgICAgICAgICAgIGxldCBwYXJlbnQgPSBpID4gMCA/IHRoaXMuZ3JpZHNbaSAtIDFdIDogbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5ncmlkcy5wdXNoKG5ldyBHcmlkKHNpemUgKiBNYXRoLnBvdygyLCBpKSwgcGFyZW50KSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2FsY3VsYXRlcyBvYmplY3RzIHRvIHRoZSBncmlkcywgdGhlbiBjaGVja3MgZm9yIGNvbGxpc2lvbnMuXHJcbiAgICAgKi9cclxuICAgIHVwZGF0ZSgpIHtcclxuICAgICAgICB0aGlzLmNsZWFyKCk7XHJcbiAgICAgICAgbGV0IG9iamVjdHMgPSB0aGlzLmNvbnRhaW5lci5jaGlsZHJlbjtcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IG9iamVjdHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgbGV0IGdvOiBHYW1lT2JqZWN0O1xyXG4gICAgICAgICAgICBnbyA9IDxHYW1lT2JqZWN0Pm9iamVjdHNbaV07XHJcbiAgICAgICAgICAgIHRoaXMuY2FsY3VsYXRlKGdvKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5jaGVja0NvbGxpc2lvbnMoKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENhbGN1bGF0ZXMgdGhlIGNvcm5lcnMgb2YgdGhlIG9iamVjdCBhbmQgcmVnaXN0ZXJzIGl0IHRvIHRoZSBhcHByb3ByaWF0ZSBncmlkIGNlbGxzO1xyXG4gICAgICogQHBhcmFtIGdvIG9iamVjdCB0byBiZSBjYWxjdWxhdGVkXHJcbiAgICAgKi9cclxuICAgIGNhbGN1bGF0ZShnbzogR2FtZU9iamVjdCkge1xyXG4gICAgICAgIGxldCBib3VuZHMgPSBnby5ib3VuZHM7XHJcbiAgICAgICAgbGV0IGN1cnJlbnRHcmlkID0gdGhpcy5ncmlkc1swXTtcclxuICAgICAgICBsZXQgc2l6ZSA9IHRoaXMuZ3JpZHNbMF0uc2l6ZTtcclxuXHJcbiAgICAgICAgLy8gQ0FMQ1VMQVRFIEdSSUQgTEVWRUxcclxuICAgICAgICAvLyBpZiBncmF0ZXIgdGhhbiBjdXJyZW50IGdyaWQsIHdlIHN3aXRjaCB0byBhIGdyZWF0ZXIgZ3JpZCAoaWYgZXhpc3RzKVxyXG4gICAgICAgIHdoaWxlICgoYm91bmRzLndpZHRoID4gc2l6ZSB8fCBib3VuZHMuaGVpZ2h0ID4gc2l6ZSkgJiYgY3VycmVudEdyaWQucGFyZW50KSB7XHJcbiAgICAgICAgICAgIGN1cnJlbnRHcmlkID0gY3VycmVudEdyaWQucGFyZW50O1xyXG4gICAgICAgICAgICBzaXplID0gY3VycmVudEdyaWQuc2l6ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZ28uZ3JpZCA9IGN1cnJlbnRHcmlkO1xyXG5cclxuICAgICAgICAvLyBDQUxDVUxBVEUgQ0VMTFMgSU4gTEVWRUwgR1JJRFxyXG4gICAgICAgIGxldCBsZWZ0ID0gTWF0aC5mbG9vcihnby54IC8gY3VycmVudEdyaWQuc2l6ZSk7IC8vIGRpdmlkZSBieSBncmlkIHNpemUgdG8gbWFrZSBncmlkIGNlbGxzXHJcbiAgICAgICAgbGV0IHJpZ2h0ID0gTWF0aC5mbG9vcigoZ28ueCArIGJvdW5kcy53aWR0aCkgLyBjdXJyZW50R3JpZC5zaXplKTtcclxuICAgICAgICBsZXQgdG9wID0gTWF0aC5mbG9vcihnby55IC8gY3VycmVudEdyaWQuc2l6ZSk7XHJcbiAgICAgICAgbGV0IGJvdHRvbSA9IE1hdGguZmxvb3IoKGdvLnkgKyBib3VuZHMuaGVpZ2h0KSAvIGN1cnJlbnRHcmlkLnNpemUpO1xyXG4gICAgICAgIGxldCB0b3BMZWZ0ID0geyB4OiBsZWZ0LCB5OiB0b3AgfTtcclxuICAgICAgICBsZXQgdG9wUmlnaHQgPSB7IHg6IHJpZ2h0LCB5OiB0b3AgfTtcclxuICAgICAgICBsZXQgYm90dG9tTGVmdCA9IHsgeDogbGVmdCwgeTogYm90dG9tIH07XHJcbiAgICAgICAgbGV0IGJvdHRvbVJpZ2h0ID0geyB4OiByaWdodCwgeTogYm90dG9tIH07XHJcblxyXG4gICAgICAgIGdvLnBvc2l0aW9ucyA9IFtdO1xyXG4gICAgICAgIHRoaXMucHVzaElmTm90RXhpc3RzKGdvLnBvc2l0aW9ucywgdG9wTGVmdCk7XHJcbiAgICAgICAgdGhpcy5wdXNoSWZOb3RFeGlzdHMoZ28ucG9zaXRpb25zLCB0b3BSaWdodCk7XHJcbiAgICAgICAgdGhpcy5wdXNoSWZOb3RFeGlzdHMoZ28ucG9zaXRpb25zLCBib3R0b21MZWZ0KTtcclxuICAgICAgICB0aGlzLnB1c2hJZk5vdEV4aXN0cyhnby5wb3NpdGlvbnMsIGJvdHRvbVJpZ2h0KTtcclxuXHJcbiAgICAgICAgLy8gUFVUIEdhbWVPYmplY3QgVE8gR1JJRFxyXG4gICAgICAgIGN1cnJlbnRHcmlkLmFkZFRvQ2VsbHMoZ28pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUHV0cyB0aGUgdmFsdWUgaW4gdGhlIGFycmF5IG9ubHkgaWYgaXQgZG9lcyBub3QgZXhpc3QuXHJcbiAgICAgKiBUaGlzIG1ha2VzIHN1cmUgdGhhdCBpZiB0d28gb3IgbW9yZSBjb3JuZXJzIG9mIHRoZSBib3VuZGluZyBib3ggYXJlIGluIHRoZSBzYW1lIGdyaWQgY2VsbCxcclxuICAgICAqIHRoZW4gdGhlIG9iamVjdCBpcyBvbmx5IHJlZ2lzdGVyZWQgb25jZSBpbiB0aGUgY2VsbCwgbm90IHR3aWNlIG9yIG1vcmUuXHJcbiAgICAgKiBAcGFyYW0gYXJyYXkgXHJcbiAgICAgKiBAcGFyYW0gdmFsdWUgXHJcbiAgICAgKi9cclxuICAgIHB1c2hJZk5vdEV4aXN0cyhhcnJheTogR3JpZFBvc2l0aW9uW10sIHZhbHVlOiBHcmlkUG9zaXRpb24pIHtcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGFycmF5Lmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGlmIChhcnJheVtpXS54ID09PSB2YWx1ZS54ICYmIGFycmF5W2ldLnkgPT09IHZhbHVlLnkpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBhcnJheS5wdXNoKHZhbHVlKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENsZWFyIGFsbCBncmlkcyBvZiBvYmplY3RzXHJcbiAgICAgKi9cclxuICAgIGNsZWFyKCkge1xyXG4gICAgICAgIGxldCBjdXJyZW50R3JpZCA9IHRoaXMuZ3JpZHNbMF07XHJcbiAgICAgICAgY3VycmVudEdyaWQuY2xlYXIoKTtcclxuICAgICAgICB3aGlsZSAoY3VycmVudEdyaWQucGFyZW50KSB7XHJcbiAgICAgICAgICAgIGN1cnJlbnRHcmlkID0gY3VycmVudEdyaWQucGFyZW50O1xyXG4gICAgICAgICAgICBjdXJyZW50R3JpZC5jbGVhcigpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrcyBjb2xsaXNpb25zIGZvciBhbGwgb2JqZWN0c1xyXG4gICAgICovXHJcbiAgICBjaGVja0NvbGxpc2lvbnMoKTogdm9pZCB7XHJcbiAgICAgICAgbGV0IGNoaWxkcmVuID0gdGhpcy5jb250YWluZXIuY2hpbGRyZW47XHJcbiAgICAgICAgZm9yIChsZXQgZ28gb2YgdGhpcy5jb250YWluZXIuY2hpbGRyZW4pIHtcclxuICAgICAgICAgICAgaWYgKGdvIGluc3RhbmNlb2YgR2FtZU9iamVjdCkge1xyXG4gICAgICAgICAgICAgICAgZm9yIChsZXQgcG9zaXRpb24gb2YgZ28ucG9zaXRpb25zKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9yIChsZXQgbWF5Q29sbGlkZVdpdGggb2YgZ28uZ3JpZC5nZXRPYmplY3RzSW5DZWxsKHBvc2l0aW9uKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAobWF5Q29sbGlkZVdpdGguY29uc3RydWN0b3IgIT09IGdvLmNvbnN0cnVjdG9yKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRlc3RDb2xsaXNpb24oZ28sIG1heUNvbGxpZGVXaXRoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFRlc3RzIGlmIHRoZSB0d28gb2JqZWN0J3MgYm91bmRpbmcgYm94IGFyZSBvdmVybGFwcGluZ1xyXG4gICAgICogQHBhcmFtIGdvMSBPbmUgb2YgdGhlIG9iamVjdHMgd2hvJ3MgYm91bmRpbmcgYm94IHdpbGwgYmUgdGVzdGVkXHJcbiAgICAgKiBAcGFyYW0gZ28yIFRoZSBvdGhlciBvYmplY3Qgd2hpY2ggaXMgdGVzdGVkXHJcbiAgICAgKi9cclxuICAgIHRlc3RDb2xsaXNpb24oZ28xOiBHYW1lT2JqZWN0LCBnbzI6IEdhbWVPYmplY3QpIHtcclxuICAgICAgICBpZiAoXHJcbiAgICAgICAgICAgIGdvMi54ICsgZ28yLmJvdW5kcy53aWR0aCA+PSBnbzEueCAmJiAvLyBnbzIgcmlnaHQgZ3JlYXRlciB0aGFuIGdvMSBsZWZ0XHJcbiAgICAgICAgICAgIGdvMi54IDw9IGdvMS54ICsgZ28xLmJvdW5kcy53aWR0aCAmJiAvLyBnbzIgbGVmdCBsZXNzIHRoYW4gZ28xIHJpZ2h0XHJcbiAgICAgICAgICAgIGdvMi55ICsgZ28yLmJvdW5kcy5oZWlnaHQgPj0gZ28xLnkgJiYgLy8gZ28yIGJvdHRvbSBncmVhdGVyIHRoYW4gZ28xIHRvcFxyXG4gICAgICAgICAgICBnbzIueSA8PSBnbzEueSArIGdvMS5ib3VuZHMuaGVpZ2h0IC8vIGdvMiB0b3AgbGVzcyB0aGFuIGdvMSBib3R0b21cclxuICAgICAgICApIHtcclxuICAgICAgICAgICAgZ28yLmVtaXQoRXZlbnRzLkNPTExJU0lPTiwgZ28xKTtcclxuICAgICAgICAgICAgZ28xLmVtaXQoRXZlbnRzLkNPTExJU0lPTiwgZ28yKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcbiIsImltcG9ydCBHYW1lT2JqZWN0IGZyb20gXCIuL0dhbWVPYmplY3RcIjtcclxuaW1wb3J0IHsgR3JpZFBvc2l0aW9uIH0gZnJvbSBcIi4vQ29sbGlzaW9uRGV0ZWN0b3JcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEdyaWQge1xyXG4gICAgY2VsbHMgPSBbXTtcclxuXHJcbiAgICAvKipcclxuICAgICAqIFRoZSBncmlkIGlzIHJlc3BvbnNpYmxlIGZvciBzdG9yaW5nIHRoZSBvYmplY3RzIGluIGNlbGxzLiBDZWxsc1xyXG4gICAgICogcmVwcmVzZW4gYmlnZ2VyIHBhcnRzIG9mIHRoZSBzY3JlZW4uIElmIHRoZSBvYmplY3RzIGFyZSBpbiB0aGUgc2FtZSBjZWxsLFxyXG4gICAgICogdGhlaXIgY29sbGlzaW9ucyB3aWxsIGJlIGNhbGN1bGF0ZWQuXHJcbiAgICAgKiBAcGFyYW0gc2l6ZSBzaXplIG9mIHRoZSBncmlkIGluIHBpeGVsc1xyXG4gICAgICogQHBhcmFtIHBhcmVudCBpZiBhIGhpZ2hlciBsZXZlbCBncmlkIGV4aXN0cywgdGhpcyBpcyBpdC5cclxuICAgICAqL1xyXG4gICAgY29uc3RydWN0b3IocHVibGljIHNpemU6IG51bWJlciwgcHVibGljIHBhcmVudDogR3JpZCkge1xyXG4gICAgICAgIC8vaW5pdFxyXG4gICAgICAgIHRoaXMuY2xlYXIoKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENhbGN1bGF0ZXMgY2VsbCBwb3NpdGlvbnMgZm9yIHRoZSBnaXZlbiBvYmplY3QgYW5kIHN0b3JlcyBpdCBpbiB0aGUgY2VsbHMuXHJcbiAgICAgKiBAcGFyYW0gZ28gXHJcbiAgICAgKi9cclxuICAgIGFkZFRvQ2VsbHMoZ286IEdhbWVPYmplY3QpIHtcclxuICAgICAgICBsZXQgcG9zaXRpb25zID0gZ28ucG9zaXRpb25zO1xyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcG9zaXRpb25zLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGlmICghdGhpcy5jZWxsc1twb3NpdGlvbnNbaV0ueF0pIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2VsbHNbcG9zaXRpb25zW2ldLnhdID0gW107XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKCF0aGlzLmNlbGxzW3Bvc2l0aW9uc1tpXS54XVtwb3NpdGlvbnNbaV0ueV0pIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2VsbHNbcG9zaXRpb25zW2ldLnhdW3Bvc2l0aW9uc1tpXS55XSA9IFtdO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuY2VsbHNbcG9zaXRpb25zW2ldLnhdW3Bvc2l0aW9uc1tpXS55XS5wdXNoKGdvKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMucGFyZW50KSB7XHJcbiAgICAgICAgICAgIC8vIGFkZCB0byBwYXJlbnQgY2VsbHMgdG9vXHJcbiAgICAgICAgICAgIHRoaXMucGFyZW50LmFkZFRvQ2VsbHMoZ28pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIERlbGV0ZXMgYWxsIG9iamVjdHMgZnJvbSB0aGUgZ3JpZC5cclxuICAgICAqL1xyXG4gICAgY2xlYXIoKSB7XHJcbiAgICAgICAgdGhpcy5jZWxscyA9IFtdO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmV0dXJucyBhbGwgdGhlIG9iamVjdHMgdGhhdCBhcmUgaW4gdGhlIGdpdmVuIGNlbGwuXHJcbiAgICAgKiBAcGFyYW0gcG9zIGdpcmQgY2VsbCBjb29yZGluYXRlc1xyXG4gICAgICovXHJcbiAgICBnZXRPYmplY3RzSW5DZWxsKHBvczogR3JpZFBvc2l0aW9uKTogR2FtZU9iamVjdFtdIHtcclxuICAgICAgICBsZXQgb2JqZWN0czogR2FtZU9iamVjdFtdID0gW107XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnBhcmVudCkge1xyXG4gICAgICAgICAgICBsZXQgcGFyZW50UG9zOiBHcmlkUG9zaXRpb24gPSB7IHg6IE1hdGguZmxvb3IocG9zLnggLyAyKSwgeTogTWF0aC5mbG9vcihwb3MueSAvIDIpIH07XHJcbiAgICAgICAgICAgIG9iamVjdHMgPSBvYmplY3RzLmNvbmNhdCh0aGlzLnBhcmVudC5nZXRPYmplY3RzSW5DZWxsKHBhcmVudFBvcykpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuY2VsbHNbcG9zLnhdICYmIHRoaXMuY2VsbHNbcG9zLnhdW3Bvcy55XSkge1xyXG4gICAgICAgICAgICByZXR1cm4gb2JqZWN0cy5jb25jYXQodGhpcy5jZWxsc1twb3MueF1bcG9zLnldKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gW107XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59IiwiaW1wb3J0IERpc3BsYXlPYmplY3QgZnJvbSBcIi4vRGlzcGxheU9iamVjdFwiO1xyXG5pbXBvcnQgeyBFdmVudHMgfSBmcm9tIFwiLi9UaWNrZXJcIjtcclxuaW1wb3J0IHsgUG9pbnRlclBvc2l0aW9uIH0gZnJvbSBcIi4vU3RhZ2VcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIENvbnRhaW5lciBleHRlbmRzIERpc3BsYXlPYmplY3Qge1xyXG4gICAgcHVibGljIGNoaWxkcmVuOiBEaXNwbGF5T2JqZWN0W10gPSBbXTtcclxuXHJcbiAgICAvKipcclxuICAgICAqIEFkZHMgdGhlIG9iamVjdCB0byB0aGUgY29udGFpbmVyLiB3aWxsIG5vdCBhZGQgdGhlIG9iamVjdCB0d2ljZS5cclxuICAgICAqIEBwYXJhbSBkaXNwbGF5T2JqZWN0IG9iamVjdCB0byBiZSBhZGRlZFxyXG4gICAgICovXHJcbiAgICBhZGRDaGlsZChkaXNwbGF5T2JqZWN0OiBEaXNwbGF5T2JqZWN0KTogdm9pZCB7XHJcbiAgICAgICAgaWYgKCF0aGlzLmNoaWxkcmVuLmZpbmQoY2hpbGQgPT4gY2hpbGQgPT09IGRpc3BsYXlPYmplY3QpKSB7XHJcbiAgICAgICAgICAgIGRpc3BsYXlPYmplY3QucGFyZW50ID0gdGhpcztcclxuICAgICAgICAgICAgdGhpcy5jaGlsZHJlbi5wdXNoKGRpc3BsYXlPYmplY3QpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJlbW92ZXMgdGhlIG9iamVjdCBmcm9tIHRoZSBjb250YWluZXIgaWYgZXhpc3RzXHJcbiAgICAgKiBAcGFyYW0gY2hpbGQgb2JqZWN0IHRvIGJlIHJlbW92ZWRcclxuICAgICAqL1xyXG4gICAgcmVtb3ZlQ2hpbGQoY2hpbGQ6IERpc3BsYXlPYmplY3QpOiB2b2lkIHtcclxuICAgICAgICBjaGlsZC5wYXJlbnQgPSB1bmRlZmluZWQ7XHJcbiAgICAgICAgY2hpbGQuY3R4ID0gdW5kZWZpbmVkO1xyXG4gICAgICAgIGNoaWxkLnN0YWdlID0gdW5kZWZpbmVkO1xyXG4gICAgICAgIHRoaXMuY2hpbGRyZW4uc3BsaWNlKHRoaXMuY2hpbGRyZW4uaW5kZXhPZihjaGlsZCksIDEpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVXBkYXRlcyBhbGwgY2hpbGRyZW5cclxuICAgICAqL1xyXG4gICAgdXBkYXRlKCk6IHZvaWQge1xyXG4gICAgICAgIGlmICghdGhpcy52aXNpYmxlKSByZXR1cm47XHJcbiAgICAgICAgdGhpcy5jaGlsZHJlbi5mb3JFYWNoKGdhbWVPYmplY3QgPT4ge1xyXG4gICAgICAgICAgICBpZiAoZ2FtZU9iamVjdC52aXNpYmxlKSB7XHJcbiAgICAgICAgICAgICAgICBnYW1lT2JqZWN0LnVwZGF0ZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBNb3VzZSBldmVudHMgYXJlIHBhc3NlZCBkb3duIHRvIGFsbCBjaGlsZHJlbiBmb3IgY2FsY3VsYXRpb25cclxuICAgICAqIEBwYXJhbSBtb3VzZSBtb3VzZSBwb3NpdGlvblxyXG4gICAgICovXHJcbiAgICBfb25Nb3VzZURvd24obW91c2U6IFBvaW50ZXJQb3NpdGlvbikge1xyXG4gICAgICAgIHN1cGVyLl9vbk1vdXNlRG93bihtb3VzZSk7XHJcbiAgICAgICAgdGhpcy5jaGlsZHJlbi5mb3JFYWNoKGNoaWxkID0+IGNoaWxkLl9vbk1vdXNlRG93bihtb3VzZSkpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogTW91c2UgZXZlbnRzIGFyZSBwYXNzZWQgZG93biB0byBhbGwgY2hpbGRyZW4gZm9yIGNhbGN1bGF0aW9uXHJcbiAgICAgKi9cclxuICAgIF9vbk1vdXNlVXAoKSB7XHJcbiAgICAgICAgc3VwZXIuX29uTW91c2VVcCgpO1xyXG4gICAgICAgIHRoaXMuY2hpbGRyZW4uZm9yRWFjaChjaGlsZCA9PiBjaGlsZC5fb25Nb3VzZVVwKCkpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogTW91c2UgZXZlbnRzIGFyZSBwYXNzZWQgZG93biB0byBhbGwgY2hpbGRyZW4gZm9yIGNhbGN1bGF0aW9uXHJcbiAgICAgKiBAcGFyYW0gbW91c2UgbW91c2UgcG9zaXRpb25cclxuICAgICAqL1xyXG4gICAgX29uQ2xpY2sobW91c2U6IFBvaW50ZXJQb3NpdGlvbikge1xyXG4gICAgICAgIHN1cGVyLl9vbkNsaWNrKG1vdXNlKTtcclxuICAgICAgICB0aGlzLmNoaWxkcmVuLmZvckVhY2goY2hpbGQgPT4gY2hpbGQuX29uQ2xpY2sobW91c2UpKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIE1vdXNlIGV2ZW50cyBhcmUgcGFzc2VkIGRvd24gdG8gYWxsIGNoaWxkcmVuIGZvciBjYWxjdWxhdGlvblxyXG4gICAgICogQHBhcmFtIG1vdXNlIG1vdXNlIHBvc2l0aW9uXHJcbiAgICAgKi9cclxuICAgIF9vbk1vdXNlTW92ZShtb3VzZTogUG9pbnRlclBvc2l0aW9uKSB7XHJcbiAgICAgICAgc3VwZXIuX29uTW91c2VNb3ZlKG1vdXNlKTtcclxuICAgICAgICB0aGlzLmNoaWxkcmVuLmZvckVhY2goY2hpbGQgPT4gY2hpbGQuX29uTW91c2VNb3ZlKG1vdXNlKSk7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQgeyBFdmVudEVtaXR0ZXIgfSBmcm9tIFwiZXZlbnRzXCI7XHJcbmltcG9ydCBTdGFnZSwgeyBQb2ludGVyUG9zaXRpb24gfSBmcm9tIFwiLi9TdGFnZVwiO1xyXG5pbXBvcnQgeyBFdmVudHMgfSBmcm9tIFwiLi9UaWNrZXJcIjtcclxuaW1wb3J0IENvbnRhaW5lciBmcm9tIFwiLi9Db250YWluZXJcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIERpc3BsYXlPYmplY3QgZXh0ZW5kcyBFdmVudEVtaXR0ZXIgaW1wbGVtZW50cyBVcGRhdGVhYmxlIHtcclxuICAgIGxvY2FsWDogbnVtYmVyID0gMDsgLy8gY29vcmRpbmF0ZXMgcmVsYXRpdmUgdG8gdGhlIHBhcmVudFxyXG4gICAgbG9jYWxZOiBudW1iZXIgPSAwOyAvLyBjb29yZGluYXRlcyByZWxhdGl2ZSB0byB0aGUgcGFyZW50XHJcbiAgICBsb2NhbE9wYWNpdHk6IG51bWJlciA9IDE7IC8vIG93biBvcGFjaXR5IHZhbHVlXHJcbiAgICBwYXJlbnQ6IENvbnRhaW5lcjtcclxuICAgIGJvdW5kczogQm91bmRzID0geyB3aWR0aDogMCwgaGVpZ2h0OiAwIH07XHJcbiAgICB2aXNpYmxlOiBib29sZWFuID0gdHJ1ZTtcclxuXHJcbiAgICBwcm90ZWN0ZWQgX3N0YWdlOiBTdGFnZTtcclxuXHJcbiAgICBwcml2YXRlIF9jdHg6IENhbnZhc1JlbmRlcmluZ0NvbnRleHQyRDsgLy8gc2F2ZWQgY2FudnMgcmVuZGVyaW5nIGNvbnRleHQgZm9yIGVhc3kgYWNjZXNzXHJcbiAgICBwcml2YXRlIF9tb3VzZW92ZXIgPSBmYWxzZTtcclxuICAgIHByaXZhdGUgX21vdXNlZG93biA9IGZhbHNlO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2hlY2tzIGlmIHBvaW50ZXIgaXMgd2l0aGluIHRoZSBib3VuZHMgb2YgdGhpcyBvYmplY3RcclxuICAgICAqIEBwYXJhbSBtb3N1ZSBwb2ludGVyIHBvc2l0aW9uXHJcbiAgICAgKi9cclxuICAgIGNoZWNrUG9pbnRlclBvc2l0aW9uKG1vc3VlOiBQb2ludGVyUG9zaXRpb24pOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gbW9zdWUueCA+IHRoaXMueFxyXG4gICAgICAgICAgICAmJiBtb3N1ZS54IDwgdGhpcy54ICsgdGhpcy5ib3VuZHMud2lkdGhcclxuICAgICAgICAgICAgJiYgbW9zdWUueSA+IHRoaXMueVxyXG4gICAgICAgICAgICAmJiBtb3N1ZS55IDwgdGhpcy55ICsgdGhpcy5ib3VuZHMuaGVpZ2h0XHJcbiAgICB9XHJcblxyXG4gICAgX29uTW91c2VEb3duKG1vdXNlOiBQb2ludGVyUG9zaXRpb24pIHtcclxuICAgICAgICBpZiAoIXRoaXMuX21vdXNlZG93biAmJiB0aGlzLmNoZWNrUG9pbnRlclBvc2l0aW9uKG1vdXNlKSkge1xyXG4gICAgICAgICAgICAvL3BvaW50ZXIgb3ZlciBnYW1lb2JqZWN0XHJcbiAgICAgICAgICAgIHRoaXMuX21vdXNlZG93biA9IHRydWU7XHJcbiAgICAgICAgICAgIHRoaXMuZW1pdChFdmVudHMuTU9VU0VET1dOLCBtb3VzZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF9vbk1vdXNlVXAoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX21vdXNlZG93bikge1xyXG4gICAgICAgICAgICB0aGlzLmVtaXQoRXZlbnRzLk1PVVNFVVAsIGV2ZW50KTtcclxuICAgICAgICAgICAgdGhpcy5fbW91c2Vkb3duID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF9vbkNsaWNrKG1vdXNlOiBQb2ludGVyUG9zaXRpb24pIHtcclxuICAgICAgICBpZiAodGhpcy5jaGVja1BvaW50ZXJQb3NpdGlvbihtb3VzZSkpIHtcclxuICAgICAgICAgICAgLy9wb2ludGVyIG92ZXIgZ2FtZW9iamVjdFxyXG4gICAgICAgICAgICB0aGlzLmVtaXQoRXZlbnRzLkNMSUNLLCBtb3VzZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIF9vbk1vdXNlTW92ZShtb3VzZTogUG9pbnRlclBvc2l0aW9uKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY2hlY2tQb2ludGVyUG9zaXRpb24obW91c2UpKSB7XHJcbiAgICAgICAgICAgIC8vcG9pbnRlciBvdmVyIGdhbWVvYmplY3RcclxuICAgICAgICAgICAgdGhpcy5lbWl0KEV2ZW50cy5NT1VTRU1PVkUsIG1vdXNlKTtcclxuICAgICAgICAgICAgaWYgKCF0aGlzLl9tb3VzZW92ZXIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZW1pdChFdmVudHMuTU9VU0VFTlRFUiwgbW91c2UpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fbW91c2VvdmVyID0gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIC8vcG9pbnRlciBsZWZ0IGdhbWVvYmplY3RcclxuICAgICAgICAgICAgaWYgKHRoaXMuX21vdXNlb3Zlcikge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lbWl0KEV2ZW50cy5NT1VTRUxFQVZFLCBtb3VzZSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9tb3VzZW92ZXIgPSBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJ1bnMgaW4gZXZlcnkgYW5pbWF0aW9uIGZyYW1lXHJcbiAgICAgKi9cclxuICAgIHVwZGF0ZSgpIHtcclxuICAgICAgICAvLyBvdmVycmlkZSBpbiBjaGlsZCBjbGFzc1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2VuZHMgb2JqZWN0IHRvIHRoZSB0b3Agb2YgYWxsIG9iamVjdHMuXHJcbiAgICAgKi9cclxuICAgIHNlbmRUb0Zyb250KCkge1xyXG4gICAgICAgIGxldCBwYXJlbnQgPSB0aGlzLnBhcmVudDtcclxuICAgICAgICBpZiAocGFyZW50KSB7XHJcbiAgICAgICAgICAgIHBhcmVudC5yZW1vdmVDaGlsZCh0aGlzKTtcclxuICAgICAgICAgICAgcGFyZW50LmFkZENoaWxkKHRoaXMpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJlbW92ZXMgdGhpcyBvYmplY3QgZnJvbSBpdHMgY29udGFpbmVyLCBzbyBpdCB3b250IGJlIHZpc2libGUsXHJcbiAgICAgKiBhbmQgbWFrZXMgaXQgcmVhZHkgZm9yIGdhcmJhZ2UgY29sbGVjdGlvbi5cclxuICAgICAqL1xyXG4gICAgcmVtb3ZlKCkge1xyXG4gICAgICAgIHRoaXMucGFyZW50LnJlbW92ZUNoaWxkKHRoaXMpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyB0aGUgc3RhZ2UgcmVjdXJzaXZlbHkgZnJvbSB0aGUgdG9wbW9zdCBwYXJlbnRcclxuICAgICAqL1xyXG4gICAgZ2V0IHN0YWdlKCk6IFN0YWdlIHtcclxuICAgICAgICBpZiAoIXRoaXMuX3N0YWdlICYmIHRoaXMucGFyZW50KSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3N0YWdlID0gdGhpcy5wYXJlbnQuc3RhZ2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLl9zdGFnZTtcclxuICAgIH1cclxuXHJcbiAgICBzZXQgc3RhZ2Uoc3RhZ2U6IFN0YWdlKSB7XHJcbiAgICAgICAgdGhpcy5fc3RhZ2UgPSBzdGFnZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIGZpbmFsIG9wYWNpdHkgaXMgbXVsdGlwbGllZCBieSBhbGwgcGFyZW50cycgb3BhY2l0aWVzXHJcbiAgICAgKi9cclxuICAgIGdldCBvcGFjaXR5KCk6IG51bWJlciB7XHJcbiAgICAgICAgaWYgKHRoaXMucGFyZW50KSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmxvY2FsT3BhY2l0eSAqIHRoaXMucGFyZW50Lm9wYWNpdHk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLmxvY2FsT3BhY2l0eTtcclxuICAgIH1cclxuXHJcbiAgICBzZXQgb3BhY2l0eShvcGFjaXR5OiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLmxvY2FsT3BhY2l0eSA9IG9wYWNpdHk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBGaW5hbCBjb29yZGluYXRlcyBhcmUgYWRkZWQgZnJvbSBhbGwgcGFyZW50cycgY29vcmRpbmF0ZXNcclxuICAgICAqL1xyXG4gICAgZ2V0IHgoKTogbnVtYmVyIHtcclxuICAgICAgICBpZiAodGhpcy5wYXJlbnQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMubG9jYWxYICsgdGhpcy5wYXJlbnQueDtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRoaXMubG9jYWxYO1xyXG4gICAgfVxyXG5cclxuICAgIHNldCB4KHg6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMubG9jYWxYID0geDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEZpbmFsIGNvb3JkaW5hdGVzIGFyZSBhZGRlZCBmcm9tIGFsbCBwYXJlbnRzJyBjb29yZGluYXRlc1xyXG4gICAgICovXHJcbiAgICBnZXQgeSgpOiBudW1iZXIge1xyXG4gICAgICAgIGlmICh0aGlzLnBhcmVudCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5sb2NhbFkgKyB0aGlzLnBhcmVudC55O1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5sb2NhbFk7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0IHkoeTogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5sb2NhbFkgPSB5O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmVuZGVyaW5nIGNvbnRleHQgaXMgc2VhcmNoZWQgcmVjdXJzaXZlbHkgaW4gdGhlIHBhcmVudCBjaGFpbiB1bnRpbCBmb3VuZC5cclxuICAgICAqL1xyXG4gICAgZ2V0IGN0eCgpOiBDYW52YXNSZW5kZXJpbmdDb250ZXh0MkQge1xyXG4gICAgICAgIGlmICghdGhpcy5fY3R4ICYmIHRoaXMucGFyZW50KSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2N0eCA9IHRoaXMucGFyZW50LmN0eDtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2N0eDtcclxuICAgIH1cclxuXHJcbiAgICBzZXQgY3R4KGN0eDogQ2FudmFzUmVuZGVyaW5nQ29udGV4dDJEKSB7XHJcbiAgICAgICAgdGhpcy5fY3R4ID0gY3R4O1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIFVwZGF0ZWFibGUge1xyXG4gICAgdXBkYXRlKCk6IHZvaWRcclxufVxyXG5cclxuZXhwb3J0IGludGVyZmFjZSBCb3VuZHMge1xyXG4gICAgd2lkdGg6IG51bWJlclxyXG4gICAgaGVpZ2h0OiBudW1iZXJcclxufSIsImV4cG9ydCB0eXBlIEVhc2luZyA9ICh0aW1lOiBudW1iZXIpID0+IG51bWJlcjtcclxuXHJcbi8qKlxyXG4gKiBFYXNpbmcgZnVuY3Rpb25zIGZvciBhbmltYXRpb24gcHVycG9zZXNcclxuICovXHJcbmV4cG9ydCBjb25zdCBFYXNpbmdzID0ge1xyXG4gICAgc2luOiBmdW5jdGlvbiAodCkgeyByZXR1cm4gTWF0aC5zaW4oTWF0aC5QSSAqIDIgKiB0KTsgfSxcclxuICAgIGVhc2VJblNpbjogZnVuY3Rpb24gKHQpIHsgcmV0dXJuIDEgKyBNYXRoLnNpbihNYXRoLlBJIC8gMiAqIHQgLSBNYXRoLlBJIC8gMik7IH0sXHJcbiAgICBlYXNlT3V0U2luOiBmdW5jdGlvbiAodCkgeyByZXR1cm4gTWF0aC5zaW4oTWF0aC5QSSAvIDIgKiB0KTsgfSxcclxuICAgIGVhc2VJbk91dFNpbjogZnVuY3Rpb24gKHQpIHsgcmV0dXJuICgxICsgTWF0aC5zaW4oTWF0aC5QSSAqIHQgLSBNYXRoLlBJIC8gMikpIC8gMjsgfSxcclxuICAgIC8vIG5vIGVhc2luZywgbm8gYWNjZWxlcmF0aW9uXHJcbiAgICBsaW5lYXI6IGZ1bmN0aW9uICh0KSB7IHJldHVybiB0IH0sXHJcbiAgICAvLyBhY2NlbGVyYXRpbmcgZnJvbSB6ZXJvIHZlbG9jaXR5XHJcbiAgICBlYXNlSW5RdWFkOiBmdW5jdGlvbiAodCkgeyByZXR1cm4gdCAqIHQgfSxcclxuICAgIC8vIGRlY2VsZXJhdGluZyB0byB6ZXJvIHZlbG9jaXR5XHJcbiAgICBlYXNlT3V0UXVhZDogZnVuY3Rpb24gKHQpIHsgcmV0dXJuIHQgKiAoMiAtIHQpIH0sXHJcbiAgICAvLyBhY2NlbGVyYXRpb24gdW50aWwgaGFsZndheSwgdGhlbiBkZWNlbGVyYXRpb25cclxuICAgIGVhc2VJbk91dFF1YWQ6IGZ1bmN0aW9uICh0KSB7IHJldHVybiB0IDwgLjUgPyAyICogdCAqIHQgOiAtMSArICg0IC0gMiAqIHQpICogdCB9LFxyXG4gICAgLy8gYWNjZWxlcmF0aW5nIGZyb20gemVybyB2ZWxvY2l0eSBcclxuICAgIGVhc2VJbkN1YmljOiBmdW5jdGlvbiAodCkgeyByZXR1cm4gdCAqIHQgKiB0IH0sXHJcbiAgICAvLyBkZWNlbGVyYXRpbmcgdG8gemVybyB2ZWxvY2l0eSBcclxuICAgIGVhc2VPdXRDdWJpYzogZnVuY3Rpb24gKHQpIHsgcmV0dXJuICgtLXQpICogdCAqIHQgKyAxIH0sXHJcbiAgICAvLyBhY2NlbGVyYXRpb24gdW50aWwgaGFsZndheSwgdGhlbiBkZWNlbGVyYXRpb24gXHJcbiAgICBlYXNlSW5PdXRDdWJpYzogZnVuY3Rpb24gKHQpIHsgcmV0dXJuIHQgPCAuNSA/IDQgKiB0ICogdCAqIHQgOiAodCAtIDEpICogKDIgKiB0IC0gMikgKiAoMiAqIHQgLSAyKSArIDEgfSxcclxuICAgIC8vIGFjY2VsZXJhdGluZyBmcm9tIHplcm8gdmVsb2NpdHkgXHJcbiAgICBlYXNlSW5RdWFydDogZnVuY3Rpb24gKHQpIHsgcmV0dXJuIHQgKiB0ICogdCAqIHQgfSxcclxuICAgIC8vIGRlY2VsZXJhdGluZyB0byB6ZXJvIHZlbG9jaXR5IFxyXG4gICAgZWFzZU91dFF1YXJ0OiBmdW5jdGlvbiAodCkgeyByZXR1cm4gMSAtICgtLXQpICogdCAqIHQgKiB0IH0sXHJcbiAgICAvLyBhY2NlbGVyYXRpb24gdW50aWwgaGFsZndheSwgdGhlbiBkZWNlbGVyYXRpb25cclxuICAgIGVhc2VJbk91dFF1YXJ0OiBmdW5jdGlvbiAodCkgeyByZXR1cm4gdCA8IC41ID8gOCAqIHQgKiB0ICogdCAqIHQgOiAxIC0gOCAqICgtLXQpICogdCAqIHQgKiB0IH0sXHJcbiAgICAvLyBhY2NlbGVyYXRpbmcgZnJvbSB6ZXJvIHZlbG9jaXR5XHJcbiAgICBlYXNlSW5RdWludDogZnVuY3Rpb24gKHQpIHsgcmV0dXJuIHQgKiB0ICogdCAqIHQgKiB0IH0sXHJcbiAgICAvLyBkZWNlbGVyYXRpbmcgdG8gemVybyB2ZWxvY2l0eVxyXG4gICAgZWFzZU91dFF1aW50OiBmdW5jdGlvbiAodCkgeyByZXR1cm4gMSArICgtLXQpICogdCAqIHQgKiB0ICogdCB9LFxyXG4gICAgLy8gYWNjZWxlcmF0aW9uIHVudGlsIGhhbGZ3YXksIHRoZW4gZGVjZWxlcmF0aW9uIFxyXG4gICAgZWFzZUluT3V0UXVpbnQ6IGZ1bmN0aW9uICh0KSB7IHJldHVybiB0IDwgLjUgPyAxNiAqIHQgKiB0ICogdCAqIHQgKiB0IDogMSArIDE2ICogKC0tdCkgKiB0ICogdCAqIHQgKiB0IH1cclxufSIsImltcG9ydCBHcmFwaGljSW1hZ2UgZnJvbSBcIi4vR3JhcGhpY0ltYWdlXCI7XHJcbmltcG9ydCBQcmVsb2FkZXIgZnJvbSBcIi4vUHJlbG9hZGVyXCI7XHJcbmltcG9ydCBHYW1lT2JqZWN0IGZyb20gXCIuL0dhbWVPYmplY3RcIjtcclxuaW1wb3J0IHsgRXZlbnRzIH0gZnJvbSBcIi4vVGlja2VyXCI7XHJcbmltcG9ydCBBbmltYXRpb24gZnJvbSBcIi4vQW5pbWF0aW9uXCI7XHJcbmltcG9ydCB7IEVhc2luZ3MgfSBmcm9tIFwiLi9FYXNpbmdzXCI7XHJcbmltcG9ydCBQbGF5ZXIgZnJvbSBcIi4vUGxheWVyXCI7XHJcbmltcG9ydCBCdWxsZXQgZnJvbSBcIi4vQnVsbGV0c1wiO1xyXG5pbXBvcnQgUG9vbCBmcm9tIFwiLi9Qb29sXCI7XHJcbmltcG9ydCBFeHBsb3Npb24gZnJvbSBcIi4vRXhwbG9zaW9uXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBFbmVteSBleHRlbmRzIEdhbWVPYmplY3Qge1xyXG4gICAgaW1hZ2U6IEdyYXBoaWNJbWFnZTtcclxuICAgIGFjdGl2ZTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgbW92ZVkgPSAxMDA7XHJcblxyXG4gICAgcHJpdmF0ZSBfb3JpZ2luYWxZID0gTWF0aC5yYW5kb20oKSAqIDUwMCArIDUwO1xyXG4gICAgcHJpdmF0ZSBfb3JpZ2luYWxYID0gODAwO1xyXG4gICAgcHJpdmF0ZSBfbW92ZVlBbmltOiBBbmltYXRpb247XHJcbiAgICBwcml2YXRlIF9tb3ZlWEFuaW06IEFuaW1hdGlvbjtcclxuXHJcbiAgICAvKipcclxuICAgICAqIENyZWF0ZXMgYSByYW5kb21seSBtb3ZpbmcgZW5lbXlcclxuICAgICAqIEBwYXJhbSBleHBsb3Npb24gUG9vbCB1c2VkIHRvIGluc3RhbnRpYXRlIGV4cGxvc2lvbnNcclxuICAgICAqIEBwYXJhbSBwcmVsb2FkZXIgdXNlZCB0byBnZXQgaW1hZ2VzXHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBleHBsb3Npb246IFBvb2w8RXhwbG9zaW9uPiwgcHVibGljIHByZWxvYWRlcjogUHJlbG9hZGVyKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuXHJcbiAgICAgICAgLy8gc2V0IHVwIHZpc3VhbHNcclxuICAgICAgICB0aGlzLmltYWdlID0gbmV3IEdyYXBoaWNJbWFnZSh0aGlzLnByZWxvYWRlci5nZXRCeU5hbWUoJ2VuZW15JykuYml0bWFwKTtcclxuICAgICAgICB0aGlzLmltYWdlLnggPSAwO1xyXG4gICAgICAgIHRoaXMuaW1hZ2UueSA9IDA7XHJcbiAgICAgICAgdGhpcy5hZGRDaGlsZCh0aGlzLmltYWdlKTtcclxuICAgICAgICB0aGlzLmJvdW5kcyA9IHRoaXMuaW1hZ2UuYm91bmRzO1xyXG5cclxuICAgICAgICAvLyBzZXQgc3RhcnRpbmcgcG9pbnRcclxuICAgICAgICB0aGlzLnggPSB0aGlzLl9vcmlnaW5hbFg7XHJcbiAgICAgICAgdGhpcy55ID0gdGhpcy5fb3JpZ2luYWxZO1xyXG5cclxuICAgICAgICAvLyBtb3ZlcyBpbiBzaW5lIG1vdGlvbiB2ZXJ0aWNhbGx5XHJcbiAgICAgICAgdGhpcy5fbW92ZVlBbmltID0gbmV3IEFuaW1hdGlvbigyLCB0aGlzLCB7IG1vdmVZOiAxMDAgfSwgeyBmcm9tOiB7IG1vdmVZOiAwIH0sIGVhc2luZzogRWFzaW5ncy5zaW4sIGxvb3A6IHRydWUgfSk7XHJcblxyXG4gICAgICAgIC8vIG1vdmVzIGxpbmVhcmx5IGZyb20gcmlnaHQgdG8gbGVmdFxyXG4gICAgICAgIHRoaXMuX21vdmVYQW5pbSA9IG5ldyBBbmltYXRpb24oNiwgdGhpcywgeyB4OiAtdGhpcy5ib3VuZHMud2lkdGggfSwgeyBmcm9tOiB7IHg6IHRoaXMuX29yaWdpbmFsWCB9LCBlYXNpbmc6IEVhc2luZ3MubGluZWFyIH0pO1xyXG4gICAgICAgIHRoaXMuX21vdmVYQW5pbS5vbihFdmVudHMuQ09NUExFVEUsICgpID0+IHRoaXMucmVzZXQoKSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25Db2xsaXNpb24oZ286IEdhbWVPYmplY3QpIHtcclxuICAgICAgICBpZiAoZ28gaW5zdGFuY2VvZiBCdWxsZXQgfHwgZ28gaW5zdGFuY2VvZiBQbGF5ZXIpIHtcclxuICAgICAgICAgICAgdGhpcy5kZXN0cm95KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJlc2V0KCkge1xyXG4gICAgICAgIHRoaXMucmVtb3ZlRXZlbnRzKCk7XHJcbiAgICAgICAgdGhpcy5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLnggPSB0aGlzLl9vcmlnaW5hbFg7XHJcbiAgICAgICAgc3VwZXIucmVzZXQoKTtcclxuICAgIH1cclxuXHJcbiAgICBzdGFydCgpIHtcclxuICAgICAgICBpZiAodGhpcy5hY3RpdmUpIHJldHVybjtcclxuICAgICAgICB0aGlzLmFkZEV2ZW50cygpO1xyXG4gICAgICAgIHRoaXMuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLl9vcmlnaW5hbFkgPSBNYXRoLnJhbmRvbSgpICogNTAwICsgNTA7XHJcbiAgICAgICAgc3VwZXIuc3RhcnQoKTtcclxuICAgIH1cclxuXHJcbiAgICBhZGRFdmVudHMoKSB7XHJcbiAgICAgICAgdGhpcy5fbW92ZVlBbmltLnBsYXkoKTtcclxuICAgICAgICB0aGlzLl9tb3ZlWEFuaW0ucGxheSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbW92ZUV2ZW50cygpIHtcclxuICAgICAgICB0aGlzLl9tb3ZlWUFuaW0uc3RvcCgpO1xyXG4gICAgICAgIHRoaXMuX21vdmVYQW5pbS5zdG9wKCk7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlKCkge1xyXG4gICAgICAgIGlmICghdGhpcy5hY3RpdmUpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnkgPSB0aGlzLl9vcmlnaW5hbFkgKyB0aGlzLm1vdmVZO1xyXG4gICAgICAgIHN1cGVyLnVwZGF0ZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGV4cGxvZGUoKSB7XHJcbiAgICAgICAgdGhpcy5leHBsb3Npb24uYWN0aXZhdGVPbmUodGhpcy54ICsgdGhpcy5ib3VuZHMud2lkdGggLyAyLCB0aGlzLnkgKyB0aGlzLmJvdW5kcy5oZWlnaHQgLyAyKTtcclxuICAgIH1cclxuXHJcbiAgICBkZXN0cm95KCkge1xyXG4gICAgICAgIHRoaXMuZXhwbG9kZSgpO1xyXG4gICAgICAgIHRoaXMucmVzZXQoKTtcclxuICAgICAgICBzdXBlci5kZXN0cm95KCk7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQgRGlzcGxheU9iamVjdCBmcm9tIFwiLi9EaXNwbGF5T2JqZWN0XCI7XHJcbmltcG9ydCBBbmltYXRpb24gZnJvbSBcIi4vQW5pbWF0aW9uXCI7XHJcbmltcG9ydCB7IEV2ZW50cyB9IGZyb20gXCIuL1RpY2tlclwiO1xyXG5pbXBvcnQgeyBQb29sYWJsZSB9IGZyb20gXCIuL1Bvb2xcIjtcclxuaW1wb3J0IENvbnRhaW5lciBmcm9tIFwiLi9Db250YWluZXJcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEV4cGxvc2lvbiBleHRlbmRzIERpc3BsYXlPYmplY3QgaW1wbGVtZW50cyBQb29sYWJsZSB7XHJcbiAgICBwdWJsaWMgbGluZVdpZHRoOiBudW1iZXI7XHJcbiAgICBwdWJsaWMgcmFkaXVzOiBudW1iZXI7XHJcbiAgICBwdWJsaWMgYWN0aXZlID0gdHJ1ZTtcclxuXHJcbiAgICBwcml2YXRlIF9hbmltOiBBbmltYXRpb247XHJcbiAgICBwcml2YXRlIF9zdGFydFJhZGl1cyA9IDIwO1xyXG4gICAgcHJpdmF0ZSBfZW5kUmFkaXVzID0gMTUwO1xyXG4gICAgcHJpdmF0ZSBfc3RhcnRMaW5lV2lkdGggPSAyMDtcclxuICAgIHByaXZhdGUgX2VuZExpbmVXaWR0aCA9IDA7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDcmVhdGVzIGFuIGFuaW1hdGVkIGV4cGxvc2lvbi4gQSB3aGl0ZSBjcmljbGUgd2lsbCBncm93IGFuZCBmYWRlIG91dCBxdWlja2x5LlxyXG4gICAgICogQHBhcmFtIF9jb250YWluZXIgZXhwbG9zaW9ucyBhcmUgYWRkZWQgdG8gdGhpcyBjb250YWluZXJcclxuICAgICAqL1xyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfY29udGFpbmVyOiBDb250YWluZXIpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG5cclxuICAgICAgICB0aGlzLl9hbmltID0gbmV3IEFuaW1hdGlvbihcclxuICAgICAgICAgICAgMC4yLFxyXG4gICAgICAgICAgICB0aGlzLFxyXG4gICAgICAgICAgICB7IGxpbmVXaWR0aDogdGhpcy5fZW5kTGluZVdpZHRoLCByYWRpdXM6IHRoaXMuX2VuZFJhZGl1cywgb3BhY2l0eTogMCB9LFxyXG4gICAgICAgICAgICB7IGZyb206IHsgbGluZVdpZHRoOiB0aGlzLl9zdGFydExpbmVXaWR0aCwgcmFkaXVzOiB0aGlzLl9zdGFydFJhZGl1cywgb3BhY2l0eTogMSB9IH1cclxuICAgICAgICApO1xyXG5cclxuICAgICAgICB0aGlzLl9hbmltLm9uKEV2ZW50cy5DT01QTEVURSwgKCkgPT4gdGhpcy5yZXNldCgpKTtcclxuICAgICAgICB0aGlzLl9jb250YWluZXIuYWRkQ2hpbGQodGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVzZXQoKSB7XHJcbiAgICAgICAgdGhpcy5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLl9hbmltLnN0b3AoKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFBsYXlzIHRoZSBleHBsb3Npb24gYXQgdGhlIGdpdmVuIGNvb3JkaW5hdGVzXHJcbiAgICAgKiBAcGFyYW0geCBzdGFydGluZyBwb3NpdGlvblxyXG4gICAgICogQHBhcmFtIHkgc3RhcnRpbmcgcG9zaXRpb25cclxuICAgICAqL1xyXG4gICAgc3RhcnQoeDogbnVtYmVyLCB5OiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLnggPSB4O1xyXG4gICAgICAgIHRoaXMueSA9IHk7XHJcbiAgICAgICAgdGhpcy5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuX2FuaW0ucGxheSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGRlc3Ryb3koKSB7XHJcbiAgICAgICAgdGhpcy5fYW5pbS5yZW1vdmVBbGxMaXN0ZW5lcnMoKTtcclxuICAgICAgICB0aGlzLl9hbmltLnN0b3AoKTtcclxuICAgICAgICB0aGlzLnJlbW92ZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZSgpIHtcclxuICAgICAgICB0aGlzLmN0eC5iZWdpblBhdGgoKTtcclxuICAgICAgICB0aGlzLmN0eC5hcmModGhpcy54LCB0aGlzLnksIHRoaXMucmFkaXVzLCAwLCAyICogTWF0aC5QSSwgZmFsc2UpO1xyXG4gICAgICAgIHRoaXMuY3R4LmxpbmVXaWR0aCA9IHRoaXMubGluZVdpZHRoO1xyXG4gICAgICAgIHRoaXMuY3R4LnN0cm9rZVN0eWxlID0gJyNmZmYnO1xyXG4gICAgICAgIHRoaXMuY3R4Lmdsb2JhbEFscGhhID0gdGhpcy5vcGFjaXR5O1xyXG4gICAgICAgIHRoaXMuY3R4LnN0cm9rZSgpO1xyXG4gICAgfVxyXG59IiwiaW1wb3J0IFJlY3QgZnJvbSBcIi4vUmVjdFwiO1xyXG5pbXBvcnQgU3RhZ2UgZnJvbSBcIi4vU3RhZ2VcIjtcclxuaW1wb3J0IEFuaW1hdGlvbiBmcm9tIFwiLi9BbmltYXRpb25cIjtcclxuaW1wb3J0IHsgRXZlbnRFbWl0dGVyIH0gZnJvbSBcImV2ZW50c1wiO1xyXG5pbXBvcnQgeyBFdmVudHMgfSBmcm9tIFwiLi9UaWNrZXJcIjtcclxuaW1wb3J0IHsgRWFzaW5ncyB9IGZyb20gXCIuL0Vhc2luZ3NcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEZhZGVTY3JlZW4gZXh0ZW5kcyBSZWN0IHtcclxuICAgIHByaXZhdGUgX2ZhZGVPdXRBbmltOiBBbmltYXRpb247XHJcbiAgICBwcml2YXRlIF9mYWRlSW5BbmltOiBBbmltYXRpb247XHJcbiAgICBwcml2YXRlIF9vbkZhZGVPdXRQcm94eSA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLmVtaXQoRXZlbnRzLkZBREVfT1VUKTtcclxuICAgIH07XHJcbiAgICBwcml2YXRlIF9vbkZhZGVJblByb3h5ID0gKCkgPT4ge1xyXG4gICAgICAgIHRoaXMuZW1pdChFdmVudHMuRkFERV9JTik7XHJcbiAgICB9O1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ3JlYXRlcyBhIGJsYWNrIHNjcmVlbiB3aGljaCBpcyB1c2VkIHRvIGNvdmVyIHRoZSBjYW52YXMgdG8gY3JlYXRlIGEgZmFkZSBpbiAvIGZhZGUgb3V0IGVmZmVjdC5cclxuICAgICAqIFdpbGwgZW1pdCBGQURFX09VVCBhbmQgRkFERV9JTiBldmVudHMgd2hlbiB0aGUgYW5pbWF0aW9uIGlzIGZpbmlzaGVkXHJcbiAgICAgKiBAcGFyYW0gc3RhZ2UgXHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBzdGFnZTogU3RhZ2UpIHtcclxuICAgICAgICBzdXBlcignIzAwMCcsIHN0YWdlLmJvdW5kcy53aWR0aCwgc3RhZ2UuYm91bmRzLmhlaWdodCk7XHJcblxyXG4gICAgICAgIHRoaXMuX2ZhZGVPdXRBbmltID0gbmV3IEFuaW1hdGlvbigwLjcsIHRoaXMsIHsgbG9jYWxPcGFjaXR5OiAwIH0sIHsgZWFzaW5nOiBFYXNpbmdzLmVhc2VJblF1YWQgfSk7XHJcbiAgICAgICAgdGhpcy5fZmFkZUluQW5pbSA9IG5ldyBBbmltYXRpb24oMC43LCB0aGlzLCB7IGxvY2FsT3BhY2l0eTogMSB9LCB7IGVhc2luZzogRWFzaW5ncy5lYXNlT3V0UXVhZCB9KTtcclxuXHJcbiAgICAgICAgdGhpcy5fZmFkZU91dEFuaW0ub24oRXZlbnRzLkNPTVBMRVRFLCB0aGlzLl9vbkZhZGVPdXRQcm94eSk7XHJcbiAgICAgICAgdGhpcy5fZmFkZUluQW5pbS5vbihFdmVudHMuQ09NUExFVEUsIHRoaXMuX29uRmFkZUluUHJveHkpO1xyXG4gICAgfVxyXG5cclxuICAgIGZhZGVJbigpIHtcclxuICAgICAgICBpZiAodGhpcy5vcGFjaXR5ID09PSAxKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX29uRmFkZUluUHJveHkoKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNlbmRUb0Zyb250KCk7XHJcbiAgICAgICAgdGhpcy5fZmFkZUluQW5pbS5wbGF5KCk7XHJcbiAgICB9XHJcblxyXG4gICAgZmFkZU91dCgpIHtcclxuICAgICAgICBpZiAodGhpcy5vcGFjaXR5ID09PSAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX29uRmFkZUluUHJveHkoKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNlbmRUb0Zyb250KCk7XHJcbiAgICAgICAgdGhpcy5fZmFkZU91dEFuaW0ucGxheSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHN0b3AoKSB7XHJcbiAgICAgICAgdGhpcy5fZmFkZU91dEFuaW0uc3RvcCgpO1xyXG4gICAgICAgIHRoaXMuX2ZhZGVJbkFuaW0uc3RvcCgpO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICBkZXN0cm95KCkge1xyXG4gICAgICAgIHRoaXMuc3RvcCgpO1xyXG4gICAgICAgIHRoaXMuX2ZhZGVPdXRBbmltLnJlbW92ZUFsbExpc3RlbmVycygpO1xyXG4gICAgICAgIHRoaXMuX2ZhZGVJbkFuaW0ucmVtb3ZlQWxsTGlzdGVuZXJzKCk7XHJcbiAgICAgICAgc3VwZXIucmVtb3ZlKCk7XHJcbiAgICB9XHJcbn1cclxuIiwiaW1wb3J0IFN0YWdlIGZyb20gXCIuL1N0YWdlXCI7XHJcbmltcG9ydCBQcmVsb2FkZXIgZnJvbSBcIi4vUHJlbG9hZGVyXCI7XHJcbmltcG9ydCBTcGxhc2hTY3JlZW4gZnJvbSBcIi4vU3BsYXNoU2NyZWVuXCI7XHJcbmltcG9ydCBNYWluU2NyZWVuIGZyb20gXCIuL01haW5TY3JlZW5cIjtcclxuaW1wb3J0IHsgVGlja2VyLCBFdmVudHMgfSBmcm9tIFwiLi9UaWNrZXJcIjtcclxuaW1wb3J0IEdhbWVTY3JlZW4gZnJvbSBcIi4vR2FtZVNjcmVlblwiO1xyXG5pbXBvcnQgR2FtZU92ZXJTY3JlZW4gZnJvbSBcIi4vR2FtZU92ZXJTY3JlZW5cIjtcclxuaW1wb3J0IEZhZGVTY3JlZW4gZnJvbSBcIi4vRmFkZVNjcmVlblwiO1xyXG5pbXBvcnQgU3RhdGVNYWNoaW5lLCB7IFN0YXRlTWFjaGluZUNvbmZpZyB9IGZyb20gXCIuL1N0YXRlTWFjaGluZVwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgR2FtZUNvbnRyb2xsZXIge1xyXG4gICAgc3BsYXNoOiBTcGxhc2hTY3JlZW47XHJcbiAgICBtYWluOiBNYWluU2NyZWVuO1xyXG4gICAgZ2FtZTogR2FtZVNjcmVlbjtcclxuICAgIGdhbWVPdmVyOiBHYW1lT3ZlclNjcmVlbjtcclxuICAgIGZhZGVyOiBGYWRlU2NyZWVuO1xyXG4gICAgc206IFN0YXRlTWFjaGluZTtcclxuXHJcbiAgICAvKipcclxuICAgICAqIExvYWRzIGFsbCBpbWFnZXMgYW5kIG1hbmFnZXMgc2NyZWVucyB3aXRoIGEgc3RhdGUgbWFjaGluZS5cclxuICAgICAqIEBwYXJhbSBjYW52YXMgY2FudmFzXHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBjYW52YXM6IEhUTUxDYW52YXNFbGVtZW50KSB7XHJcbiAgICAgICAgLy9wcmVsb2FkIGltYWdlc1xyXG4gICAgICAgIGxldCBwcmVsb2FkZXIgPSBuZXcgUHJlbG9hZGVyKFtcclxuICAgICAgICAgICAgeyBuYW1lOiAnc3BsYXNoJywgc3JjOiAnL3NwbGFzaC5qcGcnIH0sXHJcbiAgICAgICAgICAgIHsgbmFtZTogJ21haW4nLCBzcmM6ICcvbWFpbi5qcGcnIH0sXHJcbiAgICAgICAgICAgIHsgbmFtZTogJ2xvZ28nLCBzcmM6ICcvbG9nby5wbmcnIH0sXHJcbiAgICAgICAgICAgIHsgbmFtZTogJ2JsdWUgY29tZXQnLCBzcmM6ICcvYmx1ZS5wbmcnIH0sXHJcbiAgICAgICAgICAgIHsgbmFtZTogJ3B1cnBsZSBjb21ldCcsIHNyYzogJy9wdXJwbGUucG5nJyB9LFxyXG4gICAgICAgICAgICB7IG5hbWU6ICdzdGFycycsIHNyYzogJy9zdGFycy5wbmcnIH0sXHJcbiAgICAgICAgICAgIHsgbmFtZTogJ25lYnVsYScsIHNyYzogJy9uZWJ1bGFfZGFya2JsdWUucG5nJyB9LFxyXG4gICAgICAgICAgICB7IG5hbWU6ICdzaGlwJywgc3JjOiAnL3NoaXAucG5nJyB9LFxyXG4gICAgICAgICAgICB7IG5hbWU6ICdlbmVteScsIHNyYzogJy9lbmVteV9zbS5wbmcnIH0sXHJcbiAgICAgICAgICAgIHsgbmFtZTogJ2J1bGxldCcsIHNyYzogJy9idWxsZXQucG5nJyB9LFxyXG4gICAgICAgIF0sICcvaW1nJylcclxuXHJcbiAgICAgICAgcHJlbG9hZGVyLm9uKEV2ZW50cy5DT01QTEVURSwgKCkgPT4ge1xyXG4gICAgICAgICAgICBsZXQgc3RhZ2UgPSBuZXcgU3RhZ2UoY2FudmFzKTtcclxuICAgICAgICAgICAgVGlja2VyLm9uKEV2ZW50cy5VUERBVEUsICgpID0+IHN0YWdlLnVwZGF0ZSgpKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuc3BsYXNoID0gbmV3IFNwbGFzaFNjcmVlbihzdGFnZSwgcHJlbG9hZGVyKTtcclxuICAgICAgICAgICAgdGhpcy5tYWluID0gbmV3IE1haW5TY3JlZW4oc3RhZ2UsIHByZWxvYWRlcik7XHJcbiAgICAgICAgICAgIHRoaXMuZ2FtZSA9IG5ldyBHYW1lU2NyZWVuKHN0YWdlLCBwcmVsb2FkZXIpO1xyXG4gICAgICAgICAgICB0aGlzLmdhbWVPdmVyID0gbmV3IEdhbWVPdmVyU2NyZWVuKHN0YWdlKTtcclxuICAgICAgICAgICAgdGhpcy5mYWRlciA9IG5ldyBGYWRlU2NyZWVuKHN0YWdlKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuc3BsYXNoLnZpc2libGUgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5tYWluLnZpc2libGUgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5nYW1lLnZpc2libGUgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5nYW1lT3Zlci52aXNpYmxlID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMuZmFkZXIudmlzaWJsZSA9IHRydWU7XHJcblxyXG4gICAgICAgICAgICBzdGFnZS5hZGRDaGlsZCh0aGlzLnNwbGFzaCk7XHJcbiAgICAgICAgICAgIHN0YWdlLmFkZENoaWxkKHRoaXMubWFpbik7XHJcbiAgICAgICAgICAgIHN0YWdlLmFkZENoaWxkKHRoaXMuZ2FtZSk7XHJcbiAgICAgICAgICAgIHN0YWdlLmFkZENoaWxkKHRoaXMuZ2FtZU92ZXIpO1xyXG4gICAgICAgICAgICBzdGFnZS5hZGRDaGlsZCh0aGlzLmZhZGVyKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuc3RhcnQoKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBzdGFydCgpIHtcclxuXHJcbiAgICAgICAgbGV0IHN0YXRlczogU3RhdGVNYWNoaW5lQ29uZmlnID0ge1xyXG4gICAgICAgICAgICBibGFjazoge1xyXG4gICAgICAgICAgICAgICAgbmV4dDoge1xyXG4gICAgICAgICAgICAgICAgICAgIGJlZm9yZUZhZGVJbjogKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNwbGFzaC52aXNpYmxlID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIGRlbGF5OiAyLFxyXG4gICAgICAgICAgICAgICAgICAgIHRvOiAnc3BsYXNoJ1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBzcGxhc2g6IHtcclxuICAgICAgICAgICAgICAgIG5leHQ6IHtcclxuICAgICAgICAgICAgICAgICAgICBiZWZvcmVGYWRlSW46ICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zcGxhc2gudmlzaWJsZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1haW4uc2hvdygpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgdG86ICdtYWluJ1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBtYWluOiB7XHJcbiAgICAgICAgICAgICAgICBsb2FkR2FtZToge1xyXG4gICAgICAgICAgICAgICAgICAgIGJlZm9yZUZhZGVJbjogKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1haW4uaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdhbWUuc2hvdygpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgdG86ICdnYW1lJ1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGV4aXQ6IHtcclxuICAgICAgICAgICAgICAgICAgICBiZWZvcmVGYWRlSW46ICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5tYWluLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zcGxhc2gudmlzaWJsZSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBkZWxheTogMixcclxuICAgICAgICAgICAgICAgICAgICB0bzogJ3NwbGFzaCdcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgZ2FtZToge1xyXG4gICAgICAgICAgICAgICAgZ2FtZU92ZXI6IHtcclxuICAgICAgICAgICAgICAgICAgICBiZWZvcmVGYWRlSW46ICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5nYW1lLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5nYW1lT3Zlci52aXNpYmxlID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIGRlbGF5OiAyLFxyXG4gICAgICAgICAgICAgICAgICAgIHRvOiAnZ2FtZU92ZXInXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGdhbWVPdmVyOiB7XHJcbiAgICAgICAgICAgICAgICBuZXh0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYmVmb3JlRmFkZUluOiAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZ2FtZU92ZXIudmlzaWJsZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNwbGFzaC52aXNpYmxlID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIGRlbGF5OiAyLFxyXG4gICAgICAgICAgICAgICAgICAgIHRvOiAnc3BsYXNoJ1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBzZXQgdXAgdHJhbnNpdGlvbiBldmVudHNcclxuICAgICAgICB0aGlzLnNtID0gbmV3IFN0YXRlTWFjaGluZSh0aGlzLCBzdGF0ZXMsICdibGFjaycpO1xyXG4gICAgICAgIHRoaXMuc20udHJhbnNpdGlvbignbmV4dCcpOyAvLyBmaXJzdCB0cmFuc2l0aW9uXHJcbiAgICAgICAgdGhpcy5zbS5vbihFdmVudHMuQ09NUExFVEUsICgpID0+IHRoaXMuc20udHJhbnNpdGlvbignbmV4dCcpKTsgLy8gZ2VuZXJhbCB0cmFuc2l0aW9uIGJldHdlZW4gc2NlbmVzXHJcbiAgICAgICAgdGhpcy5tYWluLm9uKEV2ZW50cy5HQU1FX1NUQVJULCAoKSA9PiB0aGlzLnNtLnRyYW5zaXRpb24oJ2xvYWRHYW1lJykpOyAvLyBjbGlja2VkIG9uIGEgZ2FtZSBidXR0b25cclxuICAgICAgICB0aGlzLm1haW4ub24oRXZlbnRzLkVYSVQsICgpID0+IHRoaXMuc20udHJhbnNpdGlvbignZXhpdCcpKTsgLy8gY2xpY2tlZCBvbiBleGl0IGJ1dHRvblxyXG4gICAgICAgIHRoaXMuZ2FtZS5vbihFdmVudHMuR0FNRV9PVkVSLCAoKSA9PiB0aGlzLnNtLnRyYW5zaXRpb24oJ2dhbWVPdmVyJykpOyAvLyBwbGF5ZXIgZGllZFxyXG4gICAgfVxyXG59IiwiaW1wb3J0IENvbnRhaW5lciBmcm9tIFwiLi9Db250YWluZXJcIjtcclxuaW1wb3J0IHsgR3JpZFBvc2l0aW9uIH0gZnJvbSBcIi4vQ29sbGlzaW9uRGV0ZWN0b3JcIjtcclxuaW1wb3J0IHsgUG9vbGFibGUgfSBmcm9tIFwiLi9Qb29sXCI7XHJcbmltcG9ydCB7IEV2ZW50cyB9IGZyb20gXCIuL1RpY2tlclwiO1xyXG5pbXBvcnQgR3JpZCBmcm9tIFwiLi9Db2xsaXNpb25HcmlkXCI7XHJcblxyXG4vKipcclxuICogQmFzZSBjbGFzcyBmb3IgaW4tZ2FtZSBvYmplY3RzLiBUdXJucyBjb2xsaXNpb24gZGV0ZWN0aW9uIG9uIC8gb2ZmIGF1dG9tYXRpY2FsbHkuXHJcbiAqL1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBHYW1lT2JqZWN0IGV4dGVuZHMgQ29udGFpbmVyIGltcGxlbWVudHMgUG9vbGFibGUge1xyXG4gICAgZ3JpZDogR3JpZDtcclxuICAgIHBvc2l0aW9uczogR3JpZFBvc2l0aW9uW107XHJcbiAgICBhY3RpdmU6IGJvb2xlYW47XHJcblxyXG4gICAgcHJpdmF0ZSBfb25Db2xsaXNpb25Qcm94eSA9IChnbzogR2FtZU9iamVjdCkgPT4ge1xyXG4gICAgICAgIHRoaXMub25Db2xsaXNpb24oZ28pXHJcbiAgICB9O1xyXG5cclxuICAgIG9uQ29sbGlzaW9uKGdvOiBHYW1lT2JqZWN0KSB7IH1cclxuXHJcbiAgICBzdGFydCgpIHtcclxuICAgICAgICB0aGlzLm9uKEV2ZW50cy5DT0xMSVNJT04sIHRoaXMuX29uQ29sbGlzaW9uUHJveHkpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlc2V0KCkge1xyXG4gICAgICAgIHRoaXMucmVtb3ZlTGlzdGVuZXIoRXZlbnRzLkNPTExJU0lPTiwgdGhpcy5fb25Db2xsaXNpb25Qcm94eSk7XHJcbiAgICB9XHJcblxyXG4gICAgZGVzdHJveSgpIHtcclxuICAgICAgICB0aGlzLnBhcmVudC5yZW1vdmVDaGlsZCh0aGlzKTtcclxuICAgIH1cclxufSIsImltcG9ydCBTdGFnZSBmcm9tIFwiLi9TdGFnZVwiO1xyXG5pbXBvcnQgVGV4dCBmcm9tIFwiLi9UZXh0XCI7XHJcbmltcG9ydCBDb250YWluZXIgZnJvbSBcIi4vQ29udGFpbmVyXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBHYW1lT3ZlclNjcmVlbiBleHRlbmRzIENvbnRhaW5lciB7XHJcbiAgICAvKipcclxuICAgICAqIFNpbXBsZSB0ZXh0IGluIHRoZSBjZW50ZXIgb2YgdGhlIHNjcmVlblxyXG4gICAgICogQHBhcmFtIHN0YWdlIHN0YWdlXHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBzdGFnZTogU3RhZ2UpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgICAgIGxldCB0ZXh0ID0gbmV3IFRleHQoJ0dBTUUgT1ZFUicsICcjZmZmJywgJzMycHggT3JiaXRyb24nLCAnY2VudGVyJyk7XHJcbiAgICAgICAgdGV4dC54ID0gdGhpcy5zdGFnZS5jYW52YXMud2lkdGggLyAyO1xyXG4gICAgICAgIHRleHQueSA9IHRoaXMuc3RhZ2UuY2FudmFzLmhlaWdodCAvIDI7XHJcbiAgICAgICAgdGhpcy5hZGRDaGlsZCh0ZXh0KTtcclxuICAgIH1cclxufSIsImltcG9ydCBTdGFnZSwgeyBQb2ludGVyUG9zaXRpb24gfSBmcm9tIFwiLi9TdGFnZVwiO1xyXG5pbXBvcnQgUHJlbG9hZGVyIGZyb20gXCIuL1ByZWxvYWRlclwiO1xyXG5pbXBvcnQgQ29udGFpbmVyIGZyb20gXCIuL0NvbnRhaW5lclwiO1xyXG5pbXBvcnQgR3JhcGhpY0ltYWdlIGZyb20gXCIuL0dyYXBoaWNJbWFnZVwiO1xyXG5pbXBvcnQgeyBUaWNrZXIsIEV2ZW50cyB9IGZyb20gXCIuL1RpY2tlclwiO1xyXG5pbXBvcnQgUGxheWVyIGZyb20gXCIuL1BsYXllclwiO1xyXG5pbXBvcnQgRW5lbXkgZnJvbSBcIi4vRW5lbXlcIjtcclxuaW1wb3J0IENvbGxpc2lvbkRldGVjdG9yIGZyb20gXCIuL0NvbGxpc2lvbkRldGVjdG9yXCI7XHJcbmltcG9ydCBQb29sIGZyb20gXCIuL1Bvb2xcIjtcclxuaW1wb3J0IEV4cGxvc2lvbiBmcm9tIFwiLi9FeHBsb3Npb25cIjtcclxuaW1wb3J0IEJ1bGxldCBmcm9tIFwiLi9CdWxsZXRzXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBHYW1lU2NyZWVuIGV4dGVuZHMgQ29udGFpbmVyIHtcclxuICAgIHN0YXJzQ29udGFpbmVyOiBDb250YWluZXI7XHJcbiAgICBuZWJ1bGFDb250YWluZXI6IENvbnRhaW5lcjtcclxuICAgIGV4cGxvc2lvbkNvbnRhaW5lcjogQ29udGFpbmVyO1xyXG4gICAgaGl0Q29udGFpbmVyOiBDb250YWluZXI7XHJcbiAgICBwbGF5ZXI6IFBsYXllcjtcclxuICAgIGNvbGxpc2lvbnM6IENvbGxpc2lvbkRldGVjdG9yO1xyXG4gICAgc3Bhd25UaW1lOiBudW1iZXIgPSAxO1xyXG4gICAgZW5lbXlQb29sOiBQb29sPEVuZW15PjtcclxuICAgIGV4cGxvc2lvblBvb2w6IFBvb2w8RXhwbG9zaW9uPjtcclxuICAgIGJ1bGxldFBvb2w6IFBvb2w8QnVsbGV0PjtcclxuXHJcbiAgICBwcml2YXRlIF9zcGF3bkludGVydmFsOiBudW1iZXI7XHJcbiAgICBwcml2YXRlIF9maXJlSW50ZXJ2YWw6IG51bWJlcjtcclxuICAgIHByaXZhdGUgX2ZpcmVUaW1lOiBudW1iZXIgPSAwLjE1O1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogTG9hZHMgdGhlIGdhbWUgYW5kIGNvbnRyb2xzIHRoZSBnYW1lIG1lY2hhbmljcy5cclxuICAgICAqIE1hbmFnZXMgQnVsbGV0cywgRW5lbWllcyBhbmQgdGhlIFBsYXllciBvYmplY3QuXHJcbiAgICAgKiBNb3ZlcyB0aGUgcGFyYWxsYXggYmFja2dyb3VuZC5cclxuICAgICAqIEBwYXJhbSBzdGFnZSBzdGFnZVxyXG4gICAgICogQHBhcmFtIHByZWxvYWRlciB1c2VkIHRvIGdldCBsb2FkZWQgaW1hZ2VzXHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBzdGFnZTogU3RhZ2UsIHB1YmxpYyBwcmVsb2FkZXI6IFByZWxvYWRlcikge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcblxyXG4gICAgICAgIC8vIGluaXQgY29udGFpbmVyIGZvciBleHBsb3Npb25zIGFuZCBiYWNrZ3JvdW5kcyAod2UgZG9udCB3YW50IHRvIGluY2x1ZGUgdGhlbSBpbiBjb2xsaXNpb24gZGV0ZWN0aW9uKVxyXG4gICAgICAgIHRoaXMuZXhwbG9zaW9uQ29udGFpbmVyID0gbmV3IENvbnRhaW5lcigpO1xyXG4gICAgICAgIHRoaXMuc3RhcnNDb250YWluZXIgPSBuZXcgQ29udGFpbmVyKCk7XHJcbiAgICAgICAgdGhpcy5uZWJ1bGFDb250YWluZXIgPSBuZXcgQ29udGFpbmVyKCk7XHJcbiAgICAgICAgdGhpcy5oaXRDb250YWluZXIgPSBuZXcgQ29udGFpbmVyKCk7XHJcbiAgICAgICAgdGhpcy5uZWJ1bGFDb250YWluZXIub3BhY2l0eSA9IDAuOTtcclxuXHJcbiAgICAgICAgLy8gY3JhZXRlIHBvb2xzXHJcbiAgICAgICAgdGhpcy5leHBsb3Npb25Qb29sID0gbmV3IFBvb2w8RXhwbG9zaW9uPihFeHBsb3Npb24sIHRoaXMuZXhwbG9zaW9uQ29udGFpbmVyKTtcclxuICAgICAgICB0aGlzLmVuZW15UG9vbCA9IG5ldyBQb29sPEVuZW15PihFbmVteSwgdGhpcy5leHBsb3Npb25Qb29sLCB0aGlzLnByZWxvYWRlcik7XHJcblxyXG4gICAgICAgIC8vIGluaXQgY29sbGlzaW9uIGRldGVjdGlvblxyXG4gICAgICAgIHRoaXMuY29sbGlzaW9ucyA9IG5ldyBDb2xsaXNpb25EZXRlY3RvcigxLCAyMDAsIHRoaXMuaGl0Q29udGFpbmVyKTtcclxuXHJcbiAgICAgICAgLy8gaW5pdCBwYXJhbGxheCBiYWNrZ3JvdW5kc1xyXG4gICAgICAgIGxldCBzdGFyczEgPSBuZXcgR3JhcGhpY0ltYWdlKHRoaXMucHJlbG9hZGVyLmdldEJ5TmFtZSgnc3RhcnMnKS5iaXRtYXApO1xyXG4gICAgICAgIGxldCBzdGFyczIgPSBuZXcgR3JhcGhpY0ltYWdlKHRoaXMucHJlbG9hZGVyLmdldEJ5TmFtZSgnc3RhcnMnKS5iaXRtYXApO1xyXG4gICAgICAgIHN0YXJzMi54ID0gdGhpcy5zdGFnZS5ib3VuZHMud2lkdGggLSAxO1xyXG4gICAgICAgIGxldCBuZWJ1bGExID0gbmV3IEdyYXBoaWNJbWFnZSh0aGlzLnByZWxvYWRlci5nZXRCeU5hbWUoJ25lYnVsYScpLmJpdG1hcCk7XHJcbiAgICAgICAgbGV0IG5lYnVsYTIgPSBuZXcgR3JhcGhpY0ltYWdlKHRoaXMucHJlbG9hZGVyLmdldEJ5TmFtZSgnbmVidWxhJykuYml0bWFwKTtcclxuICAgICAgICBuZWJ1bGEyLnggPSB0aGlzLnN0YWdlLmJvdW5kcy53aWR0aCAtIDE7XHJcblxyXG4gICAgICAgIHRoaXMuc3RhcnNDb250YWluZXIuYWRkQ2hpbGQoc3RhcnMxKTtcclxuICAgICAgICB0aGlzLnN0YXJzQ29udGFpbmVyLmFkZENoaWxkKHN0YXJzMik7XHJcblxyXG4gICAgICAgIHRoaXMubmVidWxhQ29udGFpbmVyLmFkZENoaWxkKG5lYnVsYTEpO1xyXG4gICAgICAgIHRoaXMubmVidWxhQ29udGFpbmVyLmFkZENoaWxkKG5lYnVsYTIpO1xyXG5cclxuICAgICAgICAvLyBpbml0IHBsYXllclxyXG4gICAgICAgIHRoaXMucGxheWVyID0gbmV3IFBsYXllcih0aGlzLmV4cGxvc2lvblBvb2wsIHRoaXMucHJlbG9hZGVyKTtcclxuICAgICAgICB0aGlzLnBsYXllci5vbihFdmVudHMuR0FNRV9PVkVSLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZW1pdChFdmVudHMuR0FNRV9PVkVSKTtcclxuICAgICAgICB9KTtcclxuICAgICAgICAvLyBpbml0IGJ1bGxldCBwb29sIChyZXF1aXJlcyBwbGF5ZXIgb2JqZWN0KVxyXG4gICAgICAgIHRoaXMuYnVsbGV0UG9vbCA9IG5ldyBQb29sPEJ1bGxldD4oQnVsbGV0LCB0aGlzLnByZWxvYWRlciwgdGhpcy5wbGF5ZXIpO1xyXG5cclxuICAgICAgICAvLyBhZGQgYWxsIHRvIHN0YWdlXHJcbiAgICAgICAgdGhpcy5hZGRDaGlsZCh0aGlzLm5lYnVsYUNvbnRhaW5lcik7XHJcbiAgICAgICAgdGhpcy5hZGRDaGlsZCh0aGlzLnN0YXJzQ29udGFpbmVyKTtcclxuICAgICAgICB0aGlzLmFkZENoaWxkKHRoaXMuZXhwbG9zaW9uQ29udGFpbmVyKTtcclxuICAgICAgICB0aGlzLmFkZENoaWxkKHRoaXMuaGl0Q29udGFpbmVyKTtcclxuICAgICAgICB0aGlzLmhpdENvbnRhaW5lci5hZGRDaGlsZCh0aGlzLnBsYXllcik7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTaG93IHRoZSBnYW1lIHNjcmVlbiwgc3RhcnQgcGxheWVyIG1vdmVtZW50LCBzcGF3bnMgZW5lbWllcyBhbmQgYnVsbGV0c1xyXG4gICAgICovXHJcbiAgICBzaG93KCkge1xyXG4gICAgICAgIHRoaXMudmlzaWJsZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5wbGF5ZXIuc3RhcnQoKTtcclxuXHJcbiAgICAgICAgdGhpcy5fc3Bhd25JbnRlcnZhbCA9IHdpbmRvdy5zZXRJbnRlcnZhbCgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuaGl0Q29udGFpbmVyLmFkZENoaWxkKHRoaXMuZW5lbXlQb29sLmFjdGl2YXRlT25lKCkpO1xyXG4gICAgICAgIH0sIHRoaXMuc3Bhd25UaW1lICogMTAwMCk7XHJcblxyXG4gICAgICAgIHRoaXMuX2ZpcmVJbnRlcnZhbCA9IHdpbmRvdy5zZXRJbnRlcnZhbCgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuaGl0Q29udGFpbmVyLmFkZENoaWxkKHRoaXMuYnVsbGV0UG9vbC5hY3RpdmF0ZU9uZSgpKTtcclxuICAgICAgICB9LCB0aGlzLl9maXJlVGltZSAqIDEwMDApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU3RvcHMgYWxsIGdhbWUgbWVjaGFuaWNzIGFuZCBzcGF3bnNcclxuICAgICAqL1xyXG4gICAgaGlkZSgpIHtcclxuICAgICAgICB0aGlzLnZpc2libGUgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLnBsYXllci5yZXNldCgpO1xyXG4gICAgICAgIHRoaXMuZW5lbXlQb29sLmRlYWN0aXZhdGVBbGwoKTtcclxuICAgICAgICB3aW5kb3cuY2xlYXJJbnRlcnZhbCh0aGlzLl9maXJlSW50ZXJ2YWwpO1xyXG4gICAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy5fc3Bhd25JbnRlcnZhbCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBNb3ZlcyB0aGUgYmFja2dyb3VuZCwgY2FsY3VsYXRlcyBjb2xsaXNpb25zXHJcbiAgICAgKi9cclxuICAgIHVwZGF0ZSgpIHtcclxuICAgICAgICB0aGlzLnN0YXJzQ29udGFpbmVyLnggKz0gdGhpcy5zdGFyc0NvbnRhaW5lci54IC0gMSA8PSAtdGhpcy5zdGFnZS5ib3VuZHMud2lkdGggPyB0aGlzLnN0YWdlLmJvdW5kcy53aWR0aCA6IC0xO1xyXG4gICAgICAgIHRoaXMubmVidWxhQ29udGFpbmVyLnggKz0gdGhpcy5uZWJ1bGFDb250YWluZXIueCAtIDMgPD0gLXRoaXMuc3RhZ2UuYm91bmRzLndpZHRoID8gdGhpcy5zdGFnZS5ib3VuZHMud2lkdGggOiAtMztcclxuICAgICAgICB0aGlzLmNvbGxpc2lvbnMudXBkYXRlKCk7XHJcbiAgICAgICAgc3VwZXIudXBkYXRlKCk7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQgRGlzcGxheU9iamVjdCwgeyBCb3VuZHMgfSBmcm9tIFwiLi9EaXNwbGF5T2JqZWN0XCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBHcmFwaGljSW1hZ2UgZXh0ZW5kcyBEaXNwbGF5T2JqZWN0IHtcclxuXHJcbiAgICAvKipcclxuICAgICAqIENyZWF0ZXMgYW4gaW1hZ2Ugb2JqZWN0IHRoYXQgY2FuIGJlIHVzZWQgb24gdGhlIGNhbnZhcy5cclxuICAgICAqIEBwYXJhbSBpbWFnZSBiaXRtYXAgdXNlZCBvbiB0aGUgY2FudmFzXHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBpbWFnZTogSW1hZ2VCaXRtYXApIHtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgICAgIHRoaXMuYm91bmRzID0geyB3aWR0aDogdGhpcy5pbWFnZS53aWR0aCwgaGVpZ2h0OiB0aGlzLmltYWdlLmhlaWdodCB9O1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZSgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmN0eC5nbG9iYWxBbHBoYSA9IHRoaXMub3BhY2l0eTtcclxuICAgICAgICB0aGlzLmN0eC5kcmF3SW1hZ2UodGhpcy5pbWFnZSwgdGhpcy54LCB0aGlzLnkpO1xyXG4gICAgfVxyXG59IiwiaW1wb3J0IFByZWxvYWRlciBmcm9tIFwiLi9QcmVsb2FkZXJcIjtcclxuaW1wb3J0IFN0YWdlIGZyb20gXCIuL1N0YWdlXCI7XHJcbmltcG9ydCBUZXh0IGZyb20gXCIuL1RleHRcIjtcclxuaW1wb3J0IENvbnRhaW5lciBmcm9tIFwiLi9Db250YWluZXJcIjtcclxuaW1wb3J0IEdyYXBoaWNJbWFnZSBmcm9tIFwiLi9HcmFwaGljSW1hZ2VcIjtcclxuaW1wb3J0IHsgVXBkYXRlYWJsZSB9IGZyb20gXCIuL0Rpc3BsYXlPYmplY3RcIjtcclxuaW1wb3J0IHsgVGlja2VyLCBFdmVudHMgfSBmcm9tIFwiLi9UaWNrZXJcIjtcclxuaW1wb3J0IEJ1dHRvbiBmcm9tIFwiLi9CdXR0b25cIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE1haW5TY3JlZW4gZXh0ZW5kcyBDb250YWluZXIge1xyXG4gICAgcHJpdmF0ZSBfY29tZXRzOiBHcmFwaGljSW1hZ2VbXSA9IFtdO1xyXG4gICAgcHJpdmF0ZSBfc3RhcnRHYW1lUHJveHkgPSAoKSA9PiB7IHRoaXMuZW1pdChFdmVudHMuR0FNRV9TVEFSVCkgfTtcclxuICAgIHByaXZhdGUgX2V4aXRQcm94eSA9ICgpID0+IHsgdGhpcy5lbWl0KEV2ZW50cy5FWElUKSB9O1xyXG5cclxuICAgIC8vIGJ1dHRvbnNcclxuICAgIHByaXZhdGUgX2J1dHRvbjE6IEJ1dHRvbjtcclxuICAgIHByaXZhdGUgX2J1dHRvbjI6IEJ1dHRvbjtcclxuICAgIHByaXZhdGUgX2J1dHRvbjM6IEJ1dHRvbjtcclxuICAgIHByaXZhdGUgX2J1dHRvbjQ6IEJ1dHRvbjtcclxuXHJcbiAgICAvKipcclxuICAgICAqIENyZWF0ZXMgdGhlIG1lbnUgc2NyZWVuIHdpdGggNCBidXR0b25zLCBNb3ZpbmcgYmFja2dyb3VuZCwgbG9nbywgYW5kIHRpdGxlIHRleHQuXHJcbiAgICAgKiBAcGFyYW0gc3RhZ2Ugc3RhZ2VcclxuICAgICAqIEBwYXJhbSBwcmVsb2FkZXIgdXNlZCB0byBnZXQgbG9hZGVkIGltYWdlc1xyXG4gICAgICovXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgc3RhZ2U6IFN0YWdlLCBwdWJsaWMgcHJlbG9hZGVyOiBQcmVsb2FkZXIpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG5cclxuICAgICAgICAvLyBnYXRoZXIgaW1hZ2VzXHJcbiAgICAgICAgbGV0IGJnID0gbmV3IEdyYXBoaWNJbWFnZShwcmVsb2FkZXIuZ2V0QnlOYW1lKCdtYWluJykuYml0bWFwKTtcclxuICAgICAgICB0aGlzLmFkZENoaWxkKGJnKTtcclxuXHJcbiAgICAgICAgLy8gY3JlYXRlIGxvZ29cclxuICAgICAgICBsZXQgbG9nbyA9IG5ldyBHcmFwaGljSW1hZ2UocHJlbG9hZGVyLmdldEJ5TmFtZSgnbG9nbycpLmJpdG1hcCk7XHJcbiAgICAgICAgbG9nby54ID0gdGhpcy5zdGFnZS5jYW52YXMud2lkdGggLyAyIC0gbG9nby5pbWFnZS53aWR0aCAvIDI7XHJcbiAgICAgICAgbG9nby55ID0gdGhpcy5zdGFnZS5jYW52YXMuaGVpZ2h0IC8gMyAtIDIwO1xyXG4gICAgICAgIHRoaXMuYWRkQ2hpbGQobG9nbyk7XHJcblxyXG4gICAgICAgIC8vIGNyZWF0ZSB0aXRsZSB0ZXh0XHJcbiAgICAgICAgbGV0IHRpdGxlVGV4dCA9IG5ldyBUZXh0KCdPUkJMQVNURVInLCAnI2ZmZicsICczMnB4IE9yYml0cm9uJywgJ2NlbnRlcicpO1xyXG4gICAgICAgIHRpdGxlVGV4dC54ID0gdGhpcy5zdGFnZS5jYW52YXMud2lkdGggLyAyO1xyXG4gICAgICAgIHRpdGxlVGV4dC55ID0gdGhpcy5zdGFnZS5jYW52YXMuaGVpZ2h0IC8gNDtcclxuICAgICAgICB0aGlzLmFkZENoaWxkKHRpdGxlVGV4dCk7XHJcblxyXG4gICAgICAgIC8vIGNyZWF0ZSBidXR0b25zXHJcbiAgICAgICAgdGhpcy5jcmVhdGVCdXR0b25zKCk7XHJcblxyXG4gICAgICAgIC8vIGNyZWF0ZSBjb21ldHNcclxuICAgICAgICB0aGlzLmNyZWF0ZUNvbWV0cyg1KTtcclxuICAgIH1cclxuXHJcbiAgICBjcmVhdGVCdXR0b25zKCkge1xyXG4gICAgICAgIC8vIGNyZWF0ZSBidXR0b25zXHJcbiAgICAgICAgdGhpcy5fYnV0dG9uMSA9IG5ldyBCdXR0b24oJ0dBTUUgMScpO1xyXG4gICAgICAgIHRoaXMuX2J1dHRvbjIgPSBuZXcgQnV0dG9uKCdHQU1FIDInKTtcclxuICAgICAgICB0aGlzLl9idXR0b24zID0gbmV3IEJ1dHRvbignR0FNRSAzJyk7XHJcbiAgICAgICAgdGhpcy5fYnV0dG9uNCA9IG5ldyBCdXR0b24oJ0VYSVQnKTtcclxuICAgICAgICB0aGlzLl9idXR0b24xLnggPSB0aGlzLl9idXR0b24yLnggPSB0aGlzLl9idXR0b24zLnggPSB0aGlzLl9idXR0b240LnggPSB0aGlzLnN0YWdlLmNhbnZhcy53aWR0aCAvIDI7XHJcbiAgICAgICAgdGhpcy5fYnV0dG9uMS55ID0gdGhpcy5zdGFnZS5jYW52YXMuaGVpZ2h0IC8gMiArIDc1O1xyXG4gICAgICAgIHRoaXMuX2J1dHRvbjIueSA9IHRoaXMuX2J1dHRvbjEueSArIDQzO1xyXG4gICAgICAgIHRoaXMuX2J1dHRvbjMueSA9IHRoaXMuX2J1dHRvbjIueSArIDQzO1xyXG4gICAgICAgIHRoaXMuX2J1dHRvbjQueSA9IHRoaXMuX2J1dHRvbjMueSArIDYwO1xyXG5cclxuICAgICAgICAvLyBhZGQgdGhlbSB0byB0aGUgc3RhZ2VcclxuICAgICAgICB0aGlzLmFkZENoaWxkKHRoaXMuX2J1dHRvbjEpO1xyXG4gICAgICAgIHRoaXMuYWRkQ2hpbGQodGhpcy5fYnV0dG9uMik7XHJcbiAgICAgICAgdGhpcy5hZGRDaGlsZCh0aGlzLl9idXR0b24zKTtcclxuICAgICAgICB0aGlzLmFkZENoaWxkKHRoaXMuX2J1dHRvbjQpO1xyXG4gICAgfVxyXG5cclxuICAgIHNob3coKSB7XHJcbiAgICAgICAgdGhpcy52aXNpYmxlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLl9idXR0b24xLm9uKEV2ZW50cy5DTElDSywgdGhpcy5fc3RhcnRHYW1lUHJveHkpO1xyXG4gICAgICAgIHRoaXMuX2J1dHRvbjIub24oRXZlbnRzLkNMSUNLLCB0aGlzLl9zdGFydEdhbWVQcm94eSk7XHJcbiAgICAgICAgdGhpcy5fYnV0dG9uMy5vbihFdmVudHMuQ0xJQ0ssIHRoaXMuX3N0YXJ0R2FtZVByb3h5KTtcclxuICAgICAgICB0aGlzLl9idXR0b240Lm9uKEV2ZW50cy5DTElDSywgdGhpcy5fZXhpdFByb3h5KTtcclxuICAgIH1cclxuXHJcbiAgICBoaWRlKCkge1xyXG4gICAgICAgIHRoaXMudmlzaWJsZSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuX2J1dHRvbjEucmVtb3ZlQWxsTGlzdGVuZXJzKEV2ZW50cy5DTElDSyk7XHJcbiAgICAgICAgdGhpcy5fYnV0dG9uMi5yZW1vdmVBbGxMaXN0ZW5lcnMoRXZlbnRzLkNMSUNLKTtcclxuICAgICAgICB0aGlzLl9idXR0b24zLnJlbW92ZUFsbExpc3RlbmVycyhFdmVudHMuQ0xJQ0spO1xyXG4gICAgICAgIHRoaXMuX2J1dHRvbjQucmVtb3ZlQWxsTGlzdGVuZXJzKEV2ZW50cy5DTElDSyk7XHJcbiAgICB9XHJcblxyXG4gICAgY3JlYXRlQ29tZXRzKGNvdW50OiBudW1iZXIpIHtcclxuICAgICAgICBsZXQgY29tZXRUeXBlczogc3RyaW5nW10gPSBbJ2JsdWUgY29tZXQnLCAncHVycGxlIGNvbWV0J107XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBjb3VudDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGxldCBjb21ldCA9IG5ldyBHcmFwaGljSW1hZ2UodGhpcy5wcmVsb2FkZXIuZ2V0QnlOYW1lKGNvbWV0VHlwZXNbTWF0aC5yb3VuZChNYXRoLnJhbmRvbSgpKV0pLmJpdG1hcCk7XHJcbiAgICAgICAgICAgIGNvbWV0LnggPSBNYXRoLnJhbmRvbSgpICogdGhpcy5zdGFnZS5jYW52YXMud2lkdGggLSBjb21ldC5pbWFnZS53aWR0aDtcclxuICAgICAgICAgICAgY29tZXQueSA9IE1hdGgucmFuZG9tKCkgKiB0aGlzLnN0YWdlLmNhbnZhcy5oZWlnaHQgLSBjb21ldC5pbWFnZS5oZWlnaHQ7XHJcbiAgICAgICAgICAgIHRoaXMuYWRkQ2hpbGQoY29tZXQpO1xyXG4gICAgICAgICAgICB0aGlzLl9jb21ldHMucHVzaChjb21ldCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogTW92ZXMgdGhlIGNvbWV0c1xyXG4gICAgICovXHJcbiAgICB1cGRhdGUoKSB7XHJcbiAgICAgICAgdGhpcy5fY29tZXRzLmZvckVhY2goY29tZXQgPT4ge1xyXG4gICAgICAgICAgICBjb21ldC54ID0gY29tZXQubG9jYWxYICsgMS45ICogMjtcclxuICAgICAgICAgICAgY29tZXQueSA9IGNvbWV0LmxvY2FsWSArIDEgKiAyO1xyXG4gICAgICAgICAgICBpZiAoY29tZXQubG9jYWxYID4gdGhpcy5zdGFnZS5jYW52YXMud2lkdGgpIHtcclxuICAgICAgICAgICAgICAgIGNvbWV0LnggPSAtY29tZXQuaW1hZ2Uud2lkdGg7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKGNvbWV0LmxvY2FsWSA+IHRoaXMuc3RhZ2UuY2FudmFzLmhlaWdodCkge1xyXG4gICAgICAgICAgICAgICAgY29tZXQueSA9IC1jb21ldC5pbWFnZS5oZWlnaHQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICBzdXBlci51cGRhdGUoKTtcclxuICAgIH1cclxuXHJcbiAgICBkZXN0cm95KCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuaGlkZSgpO1xyXG4gICAgICAgIHRoaXMuX2NvbWV0cyA9IFtdO1xyXG4gICAgICAgIHN1cGVyLnJlbW92ZSgpO1xyXG4gICAgfVxyXG59IiwiaW1wb3J0IEdhbWVPYmplY3QgZnJvbSBcIi4vR2FtZU9iamVjdFwiO1xyXG5pbXBvcnQgR3JhcGhpY0ltYWdlIGZyb20gXCIuL0dyYXBoaWNJbWFnZVwiO1xyXG5pbXBvcnQgUHJlbG9hZGVyIGZyb20gXCIuL1ByZWxvYWRlclwiO1xyXG5pbXBvcnQgeyBFdmVudHMgfSBmcm9tIFwiLi9UaWNrZXJcIjtcclxuaW1wb3J0IHsgUG9pbnRlclBvc2l0aW9uIH0gZnJvbSBcIi4vU3RhZ2VcIjtcclxuaW1wb3J0IEVuZW15IGZyb20gXCIuL0VuZW15XCI7XHJcbmltcG9ydCBQb29sIGZyb20gXCIuL1Bvb2xcIjtcclxuaW1wb3J0IEV4cGxvc2lvbiBmcm9tIFwiLi9FeHBsb3Npb25cIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFBsYXllciBleHRlbmRzIEdhbWVPYmplY3Qge1xyXG4gICAgaW1hZ2U6IEdyYXBoaWNJbWFnZTtcclxuICAgIG1vdXNlWDogbnVtYmVyO1xyXG4gICAgbW91c2VZOiBudW1iZXI7XHJcblxyXG4gICAgcHJpdmF0ZSBfZm9sbG93TW91c2VQcm94eSA9IChtb3VzZTogUG9pbnRlclBvc2l0aW9uKSA9PiB7XHJcbiAgICAgICAgdGhpcy5tb3VzZVggPSBtb3VzZS54IC0gdGhpcy5pbWFnZS5ib3VuZHMud2lkdGggLyAyO1xyXG4gICAgICAgIHRoaXMubW91c2VZID0gbW91c2UueSAtIHRoaXMuaW1hZ2UuYm91bmRzLmhlaWdodCAvIDI7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBQbGF5ZXIgc2hpcCB0aGF0IGZvbGxvd3MgdGhlIHBvaW50ZXIuXHJcbiAgICAgKiBGaXJlcyBHQU1FX09WRVIgZXZlbnQgd2hlbiBkZXN0cm95ZWQuXHJcbiAgICAgKiBAcGFyYW0gZXhwbG9zaW9uIHBvb2wgdXNlZCB0byBoYW5kbGUgZXhwbG9zaW9uc1xyXG4gICAgICogQHBhcmFtIHByZWxvYWRlciB1c2VkIHRvIGdldCBsb2FkZWQgaW1hZ2VzXHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBleHBsb3Npb246IFBvb2w8RXhwbG9zaW9uPiwgcHVibGljIHByZWxvYWRlcjogUHJlbG9hZGVyKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuXHJcbiAgICAgICAgdGhpcy5pbWFnZSA9IG5ldyBHcmFwaGljSW1hZ2UodGhpcy5wcmVsb2FkZXIuZ2V0QnlOYW1lKCdzaGlwJykuYml0bWFwKTtcclxuICAgICAgICB0aGlzLmltYWdlLnggPSAwO1xyXG4gICAgICAgIHRoaXMuaW1hZ2UueSA9IDA7XHJcbiAgICAgICAgdGhpcy54ID0gdGhpcy5tb3VzZVggPSA1MDtcclxuICAgICAgICB0aGlzLnkgPSB0aGlzLm1vdXNlWSA9IDMwMDtcclxuXHJcbiAgICAgICAgdGhpcy5hZGRDaGlsZCh0aGlzLmltYWdlKTtcclxuICAgICAgICB0aGlzLmJvdW5kcyA9IHRoaXMuaW1hZ2UuYm91bmRzO1xyXG4gICAgfVxyXG5cclxuICAgIG9uQ29sbGlzaW9uKGdvOiBHYW1lT2JqZWN0KSB7XHJcbiAgICAgICAgaWYgKGdvIGluc3RhbmNlb2YgRW5lbXkpIHtcclxuICAgICAgICAgICAgdGhpcy5lbWl0KEV2ZW50cy5HQU1FX09WRVIpO1xyXG4gICAgICAgICAgICB0aGlzLmRlc3Ryb3koKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgc3RhcnQoKSB7XHJcbiAgICAgICAgdGhpcy52aXNpYmxlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmFkZEV2ZW50cygpO1xyXG4gICAgICAgIHN1cGVyLnN0YXJ0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVzZXQoKSB7XHJcbiAgICAgICAgdGhpcy52aXNpYmxlID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5yZW1vdmVFdmVudHMoKTtcclxuICAgICAgICBzdXBlci5yZXNldCgpO1xyXG4gICAgfVxyXG5cclxuICAgIGFkZEV2ZW50cygpIHtcclxuICAgICAgICB0aGlzLnN0YWdlLm9uKEV2ZW50cy5NT1VTRU1PVkUsIHRoaXMuX2ZvbGxvd01vdXNlUHJveHkpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbW92ZUV2ZW50cygpIHtcclxuICAgICAgICB0aGlzLnN0YWdlLnJlbW92ZUxpc3RlbmVyKEV2ZW50cy5NT1VTRU1PVkUsIHRoaXMuX2ZvbGxvd01vdXNlUHJveHkpO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZSgpIHtcclxuICAgICAgICB0aGlzLnggPSB0aGlzLnggLSAodGhpcy54IC0gdGhpcy5tb3VzZVgpIC8gNTtcclxuICAgICAgICB0aGlzLnkgPSB0aGlzLnkgLSAodGhpcy55IC0gdGhpcy5tb3VzZVkpIC8gNTtcclxuICAgICAgICBzdXBlci51cGRhdGUoKTtcclxuICAgIH1cclxuXHJcbiAgICBkZXN0cm95KCkge1xyXG4gICAgICAgIHRoaXMuZXhwbG9zaW9uLmFjdGl2YXRlT25lKHRoaXMueCwgdGhpcy55KTtcclxuICAgICAgICB0aGlzLnJlc2V0KCk7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQgR2FtZU9iamVjdCBmcm9tIFwiLi9HYW1lT2JqZWN0XCI7XHJcbmltcG9ydCBDb250YWluZXIgZnJvbSBcIi4vQ29udGFpbmVyXCI7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIFBvb2xhYmxlIHtcclxuICAgIGFjdGl2ZTogYm9vbGVhbixcclxuICAgIHJlc2V0KC4uLmFyZ3M6IGFueVtdKTogdm9pZFxyXG4gICAgc3RhcnQoLi4uYXJnczogYW55W10pOiB2b2lkXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFBvb2w8VCBleHRlbmRzIFBvb2xhYmxlPiB7XHJcbiAgICBwcml2YXRlIF9wb29sOiBUW107XHJcbiAgICBwcml2YXRlIF9hcmdzOiBhbnlbXTtcclxuXHJcbiAgICAvKipcclxuICAgICAqIEdlbmVyaWMgb2JqZWN0IHBvb2wgZm9yIHNhdmluZyBwcm9jZXNzaW5nIHBvd2VyLlxyXG4gICAgICogQHBhcmFtIF9jbGFzcyBjbGFzcyB0byBiZSBpbnN0YW50aWF0ZWRcclxuICAgICAqIEBwYXJhbSBhcmdzIGNvbnN0cnVjdG9yIGFyZ3VtZW50cyBmb3IgdGhlIGNsYXNzXHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX2NsYXNzOiB7IG5ldyguLi5hcmdzOiBhbnlbXSk6IFQ7IH0sIC4uLmFyZ3M6IGFueVtdKSB7XHJcbiAgICAgICAgdGhpcy5fYXJncyA9IGFyZ3M7XHJcbiAgICAgICAgdGhpcy5fcG9vbCA9IFtdO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRGVhY3RpdmF0ZXMgYWxsIHBvb2wgb2JqZWN0c1xyXG4gICAgICovXHJcbiAgICBkZWFjdGl2YXRlQWxsKCkge1xyXG4gICAgICAgIHRoaXMuX3Bvb2wuZm9yRWFjaChpdGVtID0+IGl0ZW0ucmVzZXQoKSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBGaW5kcyBhbiBpbmFjdGl2ZSBvYmplY3Qgb3IgY3JlYXRlcyBvbmUgaWYgdGhlcmUgaXMgbm9uZS5cclxuICAgICAqIEBwYXJhbSBhcmdzIGFyZ3VtZW50cyB0byB0aGUgc3RhcnQgZnVuY3Rpb24gaWYgaGFzIGFueVxyXG4gICAgICovXHJcbiAgICBhY3RpdmF0ZU9uZSguLi5hcmdzOiBhbnlbXSk6IFQge1xyXG4gICAgICAgIGxldCBpdGVtID0gdGhpcy5fcG9vbC5maW5kKGl0ZW0gPT4gaXRlbS5hY3RpdmUgPT09IGZhbHNlKSB8fCB0aGlzLmNyZWF0ZSgpO1xyXG4gICAgICAgIGl0ZW0uc3RhcnQoLi4uYXJncyk7XHJcbiAgICAgICAgcmV0dXJuIGl0ZW07XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBJbnN0YW50aWF0ZXMgdGhlIGNsYXNzXHJcbiAgICAgKi9cclxuICAgIGNyZWF0ZSgpOiBUIHtcclxuICAgICAgICBsZXQgaW5zdGFuY2UgPSBuZXcgdGhpcy5fY2xhc3MoLi4udGhpcy5fYXJncyk7XHJcbiAgICAgICAgdGhpcy5fcG9vbC5wdXNoKGluc3RhbmNlKTtcclxuICAgICAgICByZXR1cm4gaW5zdGFuY2U7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQgeyBFdmVudEVtaXR0ZXIgfSBmcm9tIFwiZXZlbnRzXCI7XHJcbmltcG9ydCB7IEV2ZW50cyB9IGZyb20gXCIuL1RpY2tlclwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUHJlbG9hZGVyIGV4dGVuZHMgRXZlbnRFbWl0dGVyIHtcclxuICAgIHB1YmxpYyByZXNvdXJjZXM6IFByb2Nlc3NlZEltYWdlUmVzb3VyY2VbXSA9IFtdO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogUHJlbG9hZHMgdGhlIGltYWdlcywgdGhlbiBmaXJlcyBhIENPTVBMRVRFIGV2ZW50LlxyXG4gICAgICogQHBhcmFtIGltYWdlcyBpbWFnZXMgdG8gYmUgbG9hZGVkXHJcbiAgICAgKiBAcGFyYW0gaW1nUGF0aCBwYXRoIHRvIGJlIHByZXBlbmRlZCBiZW9yZSBpbWFnZSBuYW1lc1xyXG4gICAgICovXHJcbiAgICBjb25zdHJ1Y3RvcihpbWFnZXM6IFByZWxvYWRJbWFnZVJlc291cmNlW10sIGltZ1BhdGg6IHN0cmluZykge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcblxyXG4gICAgICAgIGxldCBwcm9taXNlczogUHJvbWlzZTxzdHJpbmc+W10gPSBbXTtcclxuICAgICAgICBpbWFnZXMuZm9yRWFjaChpbWFnZSA9PiB7XHJcbiAgICAgICAgICAgIGxldCBwcm9taXNlID0gbmV3IFByb21pc2U8c3RyaW5nPigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgICAgICAgICAgICBsZXQgc3JjID0gaW1nUGF0aCArIGltYWdlLnNyYztcclxuICAgICAgICAgICAgICAgIGxldCBpbWcgPSBuZXcgSW1hZ2UoKTtcclxuICAgICAgICAgICAgICAgIGltZy5zcmMgPSBzcmM7XHJcbiAgICAgICAgICAgICAgICBsZXQgc3BsYXNoOiBJbWFnZUJpdG1hcDtcclxuICAgICAgICAgICAgICAgIGltZy5vbmxvYWQgPSAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY3JlYXRlSW1hZ2VCaXRtYXAoaW1nLCAwLCAwLCBpbWcubmF0dXJhbFdpZHRoLCBpbWcubmF0dXJhbEhlaWdodCkudGhlbigoaW1hZ2VCaXRtYXApID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yZXNvdXJjZXMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBpbWFnZS5uYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3JjOiBzcmMsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogaW1hZ2VCaXRtYXAud2lkdGgsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IGltYWdlQml0bWFwLmhlaWdodCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJpdG1hcDogaW1hZ2VCaXRtYXBcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSgnaW1hZ2UgbG9hZGVkJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpbWcub25lcnJvciA9IGZ1bmN0aW9uIChlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVqZWN0KGUpO1xyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHByb21pc2VzLnB1c2gocHJvbWlzZSk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIFByb21pc2UuYWxsKHByb21pc2VzKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5lbWl0KEV2ZW50cy5DT01QTEVURSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIHJlc291cmNlIGJ5IGl0cyBuYW1lXHJcbiAgICAgKiBAcGFyYW0gbmFtZSBuYW1lIG9mIHRoZSByZXNvdXJjZVxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0QnlOYW1lKG5hbWU6IHN0cmluZyk6IFByb2Nlc3NlZEltYWdlUmVzb3VyY2Uge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnJlc291cmNlcy5maW5kKHJlc291cmNlID0+IHJlc291cmNlLm5hbWUgPT09IG5hbWUpO1xyXG4gICAgfVxyXG59XHJcblxyXG5pbnRlcmZhY2UgUHJlbG9hZEltYWdlUmVzb3VyY2Uge1xyXG4gICAgbmFtZTogc3RyaW5nXHJcbiAgICBzcmM6IHN0cmluZyxcclxufVxyXG5cclxuaW50ZXJmYWNlIFByb2Nlc3NlZEltYWdlUmVzb3VyY2Uge1xyXG4gICAgbmFtZTogc3RyaW5nLFxyXG4gICAgc3JjOiBzdHJpbmcsXHJcbiAgICB3aWR0aDogbnVtYmVyLFxyXG4gICAgaGVpZ2h0OiBudW1iZXIsXHJcbiAgICBiaXRtYXA6IEltYWdlQml0bWFwXHJcbn0iLCJpbXBvcnQgRGlzcGxheU9iamVjdCBmcm9tIFwiLi9EaXNwbGF5T2JqZWN0XCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBSZWN0IGV4dGVuZHMgRGlzcGxheU9iamVjdCB7XHJcbiAgICBwdWJsaWMgc3Ryb2tlID0gJyc7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDcmVhdGVzIGEgY29sb3JlZCByZWN0YW5nbGUuXHJcbiAgICAgKiBAcGFyYW0gY29sb3IgY29sb3Igb2YgdGhlIHJlY3RhbmdsZVxyXG4gICAgICogQHBhcmFtIHdpZHRoIHdpZHRoIG9mIHRoZSByZWN0YW5nbGVcclxuICAgICAqIEBwYXJhbSBoZWlnaHQgaGVpZ2h0IG9mIHRoZSByZWN0YW5nbGVcclxuICAgICAqL1xyXG4gICAgY29uc3RydWN0b3IocHVibGljIGNvbG9yOiBzdHJpbmcsIHB1YmxpYyB3aWR0aDogbnVtYmVyLCBwdWJsaWMgaGVpZ2h0OiBudW1iZXIpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG5cclxuICAgICAgICB0aGlzLmJvdW5kcyA9IHsgd2lkdGg6IHRoaXMud2lkdGgsIGhlaWdodDogdGhpcy5oZWlnaHQgfTtcclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGUoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5jdHguZ2xvYmFsQWxwaGEgPSB0aGlzLm9wYWNpdHk7XHJcbiAgICAgICAgdGhpcy5jdHguZmlsbFN0eWxlID0gdGhpcy5jb2xvcjtcclxuICAgICAgICB0aGlzLmN0eC5zdHJva2VTdHlsZSA9IHRoaXMuc3Ryb2tlO1xyXG4gICAgICAgIHRoaXMuY3R4LmxpbmVXaWR0aCA9IDE7XHJcbiAgICAgICAgdGhpcy5jdHguc3Ryb2tlUmVjdCh0aGlzLngsIHRoaXMueSwgdGhpcy53aWR0aCwgdGhpcy5oZWlnaHQpO1xyXG4gICAgICAgIHRoaXMuY3R4LmZpbGxSZWN0KHRoaXMueCwgdGhpcy55LCB0aGlzLndpZHRoLCB0aGlzLmhlaWdodCk7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQgUHJlbG9hZGVyIGZyb20gXCIuL1ByZWxvYWRlclwiO1xyXG5pbXBvcnQgU3RhZ2UgZnJvbSBcIi4vU3RhZ2VcIjtcclxuaW1wb3J0IEdyYXBoaWNJbWFnZSBmcm9tIFwiLi9HcmFwaGljSW1hZ2VcIjtcclxuaW1wb3J0IENvbnRhaW5lciBmcm9tIFwiLi9Db250YWluZXJcIjtcclxuaW1wb3J0IFRleHQgZnJvbSBcIi4vVGV4dFwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU3BsYXNoU2NyZWVuIGV4dGVuZHMgQ29udGFpbmVyIHtcclxuICAgIC8qKlxyXG4gICAgICogU2ltcGxlIGJhY2tncm91bmQgd2l0aCBhIHRpdGxlIHRleHQuXHJcbiAgICAgKiBAcGFyYW0gc3RhZ2Ugc3RhZ2VcclxuICAgICAqIEBwYXJhbSBwcmVsb2FkZXIgdXNlZCB0byBnZXQgaW1hZ2VzXHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBzdGFnZTogU3RhZ2UsIHB1YmxpYyBwcmVsb2FkZXI6IFByZWxvYWRlcikge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcblxyXG4gICAgICAgIC8vIGdhdGhlciBpbWFnZXNcclxuICAgICAgICBsZXQgYmcgPSBuZXcgR3JhcGhpY0ltYWdlKHRoaXMucHJlbG9hZGVyLmdldEJ5TmFtZSgnc3BsYXNoJykuYml0bWFwKTtcclxuICAgICAgICBsZXQgdGV4dCA9IG5ldyBUZXh0KCdPUkJMQVNURVInLCAnI2ZmZicsICc0MHB4IE9yYml0cm9uJywgJ2NlbnRlcicpO1xyXG4gICAgICAgIHRleHQueCA9IHRoaXMuc3RhZ2UuY2FudmFzLndpZHRoIC8gMjtcclxuICAgICAgICB0ZXh0LnkgPSB0aGlzLnN0YWdlLmNhbnZhcy5oZWlnaHQgLyAzICogMjtcclxuXHJcbiAgICAgICAgdGhpcy5hZGRDaGlsZChiZyk7XHJcbiAgICAgICAgdGhpcy5hZGRDaGlsZCh0ZXh0KTtcclxuICAgIH1cclxufSIsImltcG9ydCBDb250YWluZXIgZnJvbSBcIi4vQ29udGFpbmVyXCI7XHJcbmltcG9ydCBSZWN0IGZyb20gXCIuL1JlY3RcIjtcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgUG9pbnRlclBvc2l0aW9uIHtcclxuICAgIHg6IG51bWJlcixcclxuICAgIHk6IG51bWJlclxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTdGFnZSBleHRlbmRzIENvbnRhaW5lciB7XHJcbiAgICAvKipcclxuICAgICAqIE1hbmFnZXMgZHJhd2luZyB0byB0aGUgY2FudmFzLiBBbGwgRGlzcGxheSBvYmplY3RzIG11c3QgYmUgYWRkZWQgdG8gdGhlIHN0YWdlXHJcbiAgICAgKiB0byBiZSBkcmF3biB0byB0aGUgY2FudmFzLiBJdCBoYXMgYSBzb2xpZCBibGFjayBiYWNrZ3JvdW5kLlxyXG4gICAgICogTGlzdGVucyBmb3IgbW91c2UgZXZlbnRzIGFuZCBjYWxjdWxhdGVzIG1vdXNlIHBvc2l0aW9uIG9uIHRoZSBjYW52YXMuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBtb3VzZTogUG9pbnRlclBvc2l0aW9uID0ge1xyXG4gICAgICAgIHg6IDAsXHJcbiAgICAgICAgeTogMFxyXG4gICAgfVxyXG4gICAgcHVibGljIG1vdXNlWDogbnVtYmVyID0gMDtcclxuICAgIHB1YmxpYyBtb3VzZVk6IG51bWJlciA9IDA7XHJcblxyXG4gICAgcHJpdmF0ZSBiZzogUmVjdDtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgY2FudmFzOiBIVE1MQ2FudmFzRWxlbWVudCkge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICAgICAgdGhpcy5jdHggPSBjYW52YXMuZ2V0Q29udGV4dCgnMmQnKTtcclxuICAgICAgICB0aGlzLl9zdGFnZSA9IHRoaXM7XHJcbiAgICAgICAgdGhpcy5iZyA9IG5ldyBSZWN0KCcjMDAwJywgdGhpcy5jYW52YXMud2lkdGgsIHRoaXMuY2FudmFzLmhlaWdodCk7XHJcbiAgICAgICAgdGhpcy5hZGRDaGlsZCh0aGlzLmJnKTtcclxuICAgICAgICB0aGlzLmNhbnZhcy5hZGRFdmVudExpc3RlbmVyKCdtb3VzZW1vdmUnLCBldmVudCA9PiB7IHRoaXMuY2FsY3VsYXRlTW91c2VQb3MoZXZlbnQpOyB0aGlzLl9vbk1vdXNlTW92ZSh0aGlzLm1vdXNlKTsgfSk7XHJcbiAgICAgICAgdGhpcy5jYW52YXMuYWRkRXZlbnRMaXN0ZW5lcignbW91c2Vkb3duJywgZXZlbnQgPT4geyB0aGlzLmNhbGN1bGF0ZU1vdXNlUG9zKGV2ZW50KTsgdGhpcy5fb25Nb3VzZURvd24odGhpcy5tb3VzZSk7IH0pO1xyXG4gICAgICAgIHRoaXMuY2FudmFzLmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNldXAnLCBldmVudCA9PiB7IHRoaXMuY2FsY3VsYXRlTW91c2VQb3MoZXZlbnQpOyB0aGlzLl9vbk1vdXNlVXAoKTsgfSk7XHJcbiAgICAgICAgdGhpcy5jYW52YXMuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBldmVudCA9PiB7IHRoaXMuY2FsY3VsYXRlTW91c2VQb3MoZXZlbnQpOyB0aGlzLl9vbkNsaWNrKHRoaXMubW91c2UpOyB9KTtcclxuXHJcbiAgICAgICAgdGhpcy5ib3VuZHMgPSB7IHdpZHRoOiB0aGlzLmNhbnZhcy53aWR0aCwgaGVpZ2h0OiB0aGlzLmNhbnZhcy5oZWlnaHQgfTtcclxuICAgIH1cclxuXHJcbiAgICBjYWxjdWxhdGVNb3VzZVBvcyhldmVudDogTW91c2VFdmVudCkge1xyXG4gICAgICAgIGxldCBjYW52YXNCb3VuZGluZ1JlY3QgPSB0aGlzLnN0YWdlLmNhbnZhcy5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcclxuICAgICAgICB0aGlzLm1vdXNlLnggPSBldmVudC5wYWdlWCAtIGNhbnZhc0JvdW5kaW5nUmVjdC5sZWZ0O1xyXG4gICAgICAgIHRoaXMubW91c2UueSA9IGV2ZW50LnBhZ2VZIC0gY2FudmFzQm91bmRpbmdSZWN0LnRvcDtcclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGUoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5jdHguY2xlYXJSZWN0KDAsIDAsIHRoaXMuY2FudmFzLndpZHRoLCB0aGlzLmNhbnZhcy5oZWlnaHQpO1xyXG4gICAgICAgIHN1cGVyLnVwZGF0ZSgpO1xyXG4gICAgfVxyXG59IiwiaW1wb3J0IHsgRXZlbnRFbWl0dGVyIH0gZnJvbSBcImV2ZW50c1wiO1xyXG5pbXBvcnQgeyBFdmVudHMgfSBmcm9tIFwiLi9UaWNrZXJcIjtcclxuaW1wb3J0IEdhbWVDb250cm9sbGVyIGZyb20gXCIuL0dhbWVDb250cm9sbGVyXCI7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIFN0YXRlTWFjaGluZUNvbmZpZyB7XHJcbiAgICBbaW5kZXg6IHN0cmluZ106IFN0YXRlXHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgU3RhdGUge1xyXG4gICAgW2luZGV4OiBzdHJpbmddOiBUcmFuc2l0aW9uO1xyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIFRyYW5zaXRpb24ge1xyXG4gICAgYmVmb3JlRmFkZUluOiAoKSA9PiB2b2lkLFxyXG4gICAgdG86IHN0cmluZyxcclxuICAgIGRlbGF5PzogbnVtYmVyIC8vIGluIHNlY29uZHNcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU3RhdGVNYWNoaW5lIGV4dGVuZHMgRXZlbnRFbWl0dGVyIHtcclxuICAgIHByaXZhdGUgX2RlbGF5VGltZW91dDogbnVtYmVyO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogU3RhdGUtbWFjaGluZS1saWtlIGNsYXNzIHdoaWNoIHNwZWNpYWxpemV6IGluIHRyYW5zaXRpb25pbmcgc2NyZWVucy5cclxuICAgICAqIEBwYXJhbSBnYyBnYW1lIGNvbnRyb2xsZXIgb2JqZWN0XHJcbiAgICAgKiBAcGFyYW0gc3RhdGVzIHN0YXRlcyB0aGUgbWFjaGluZSBpcyBhbGxvd2VkIHRvIHRyYW5zaXRpb24gdG8uXHJcbiAgICAgKiBAcGFyYW0gY3VycmVudFN0YXRlIHN0b3JlcyB0aGUgY3VycmVudCBzdGF0ZVxyXG4gICAgICovXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgZ2M6IEdhbWVDb250cm9sbGVyLCBwdWJsaWMgc3RhdGVzOiBTdGF0ZU1hY2hpbmVDb25maWcsIHB1YmxpYyBjdXJyZW50U3RhdGU6IHN0cmluZykge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBJZiB0aGUgY3VycmVudCBzdGF0ZSBoYXMgYSB0cmFuc2l0aW9uIGdpdmVuIGJ5IHRoZSBwYXJhbWV0ZXIsXHJcbiAgICAgKiB0aGVuIHRoZSBzdGF0ZSBpcyBjaGFuZ2VkIHRvIHRoZSBuZXcgc3RhdGUsIGFuZCB0aGUgdHJhbnNpdGlvblxyXG4gICAgICogZnVuY2l0b24gaXMgcGxheWVkLlxyXG4gICAgICogQHBhcmFtIHRyYW5zaXRpb24gdHJhbnNpdGlvbiBuYW1lXHJcbiAgICAgKi9cclxuICAgIHRyYW5zaXRpb24odHJhbnNpdGlvbjogc3RyaW5nKSB7XHJcbiAgICAgICAgbGV0IHQgPSB0aGlzLnN0YXRlc1t0aGlzLmN1cnJlbnRTdGF0ZV1bdHJhbnNpdGlvbl07XHJcbiAgICAgICAgaWYgKHQpIHtcclxuICAgICAgICAgICAgdGhpcy5nYy5mYWRlci5zdG9wKCk7XHJcbiAgICAgICAgICAgIHdpbmRvdy5jbGVhclRpbWVvdXQodGhpcy5fZGVsYXlUaW1lb3V0KTtcclxuICAgICAgICAgICAgdGhpcy5jdXJyZW50U3RhdGUgPSB0LnRvO1xyXG4gICAgICAgICAgICB0aGlzLnBsYXkodCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRmlyZXMgYSBjb21wbGV0ZSBldmVudFxyXG4gICAgICovXHJcbiAgICBjb21wbGV0ZSgpIHtcclxuICAgICAgICB0aGlzLmVtaXQoRXZlbnRzLkNPTVBMRVRFKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJ1bnMgdGhlIGNob3NlbiB0cmFuc2l0aW9uIGFuZCBhcHBsaWVzIGEgZGVsYXkgYmV0d2VlbiBiZWZvcmUgdGhlIG5leHQgdHJhbnNpdGlvbiBpZiBuZWNlc3NhcnlcclxuICAgICAqIEBwYXJhbSB0IHRhcm5zaXRpb24gdG8gYmUgcnVuXHJcbiAgICAgKi9cclxuICAgIHBsYXkodDogVHJhbnNpdGlvbikge1xyXG4gICAgICAgIC8vIHN0YXJ0IHByZXYgZmFkZSBvdXRcclxuICAgICAgICB0aGlzLmdjLmZhZGVyLm9uKEV2ZW50cy5GQURFX0lOLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIG9uIHByZXYgZmFkZSBvdXRcclxuICAgICAgICAgICAgdGhpcy5nYy5mYWRlci5yZW1vdmVBbGxMaXN0ZW5lcnMoKTtcclxuICAgICAgICAgICAgLy8gc2V0dXAgbG9naWMgZ29lcyBoZXJlXHJcbiAgICAgICAgICAgIHQuYmVmb3JlRmFkZUluKCk7XHJcblxyXG4gICAgICAgICAgICAvLyBzdGFydCB0byBmYWRlIGluIG5ldyBzY3JlZW5cclxuICAgICAgICAgICAgdGhpcy5nYy5mYWRlci5vbihFdmVudHMuRkFERV9PVVQsICgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZ2MuZmFkZXIucmVtb3ZlQWxsTGlzdGVuZXJzKCk7XHJcbiAgICAgICAgICAgICAgICAvLyB3YWl0IGZvciB4IHNlY29uZHMgYmVmb3JlIG5leHQgc2NyZWVuXHJcbiAgICAgICAgICAgICAgICBpZiAodC5kZWxheSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX2RlbGF5VGltZW91dCA9IHdpbmRvdy5zZXRUaW1lb3V0KCgpID0+IHsgdGhpcy5jb21wbGV0ZSgpIH0sIHQuZGVsYXkgKiAxMDAwKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgdGhpcy5nYy5mYWRlci5mYWRlT3V0KCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5nYy5mYWRlci5mYWRlSW4oKTtcclxuICAgIH1cclxufVxyXG4iLCJpbXBvcnQgRGlzcGxheU9iamVjdCwgeyBCb3VuZHMgfSBmcm9tIFwiLi9EaXNwbGF5T2JqZWN0XCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBUZXh0IGV4dGVuZHMgRGlzcGxheU9iamVjdCB7XHJcbiAgICAvKipcclxuICAgICAqIFNpbXBsZSB0ZXh0IG9iamVjdCBmb3IgdXNlIG9uIHRoZSBjYW52YXNcclxuICAgICAqIEBwYXJhbSB0ZXh0IHRleHRcclxuICAgICAqIEBwYXJhbSBjb2xvciBjb2xvclxyXG4gICAgICogQHBhcmFtIGZvbnQgQ1NTIGZvbnQgZGVmaW5pdGlvblxyXG4gICAgICogQHBhcmFtIGFsaWduIHRleHQgYWxpZ25tZW50XHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyB0ZXh0OiBzdHJpbmcsIHB1YmxpYyBjb2xvcjogc3RyaW5nLCBwdWJsaWMgZm9udDogc3RyaW5nLCBwdWJsaWMgYWxpZ246IHN0cmluZykge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICAgICAgXHJcbiAgICAgICAgbGV0IHRleHRIVE1MID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKTtcclxuICAgICAgICB0ZXh0SFRNTC5pbm5lckhUTUwgPSB0aGlzLnRleHQ7XHJcbiAgICAgICAgdGV4dEhUTUwuc3R5bGUucG9zaXRpb24gPSAnYWJzb2x1dGUnO1xyXG4gICAgICAgIHRleHRIVE1MLnN0eWxlLnRvcCA9ICctOTk5OXB4JztcclxuICAgICAgICB0ZXh0SFRNTC5zdHlsZS5sZWZ0ID0gJy05OTk5cHgnO1xyXG4gICAgICAgIHRleHRIVE1MLnN0eWxlLmZvbnQgPSB0aGlzLmZvbnQ7XHJcbiAgICAgICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZCh0ZXh0SFRNTCk7XHJcbiAgICAgICAgdGhpcy5ib3VuZHMgPSB7IHdpZHRoOiB0ZXh0SFRNTC5vZmZzZXRXaWR0aCwgaGVpZ2h0OiB0ZXh0SFRNTC5vZmZzZXRIZWlnaHQgfTtcclxuICAgICAgICBkb2N1bWVudC5ib2R5LnJlbW92ZUNoaWxkKHRleHRIVE1MKTtcclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGUoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5jdHguZ2xvYmFsQWxwaGEgPSB0aGlzLm9wYWNpdHk7XHJcbiAgICAgICAgdGhpcy5jdHguZm9udCA9IHRoaXMuZm9udDtcclxuICAgICAgICB0aGlzLmN0eC5maWxsU3R5bGUgPSB0aGlzLmNvbG9yO1xyXG4gICAgICAgIHRoaXMuY3R4LnRleHRBbGlnbiA9IHRoaXMuYWxpZ247XHJcbiAgICAgICAgdGhpcy5jdHguZmlsbFRleHQodGhpcy50ZXh0LCB0aGlzLngsIHRoaXMueSk7XHJcbiAgICB9XHJcbn0iLCJpbXBvcnQgeyBFdmVudEVtaXR0ZXIgfSBmcm9tIFwiZXZlbnRzXCI7XHJcblxyXG5jbGFzcyBfVGlja2VyIGV4dGVuZHMgRXZlbnRFbWl0dGVyIHtcclxuICAgIHByaXZhdGUgc3RhdGljIF9pbnN0YW5jZTogX1RpY2tlcjtcclxuXHJcbiAgICAvKipcclxuICAgICAqIEhhbmRsZXMgUmVxdWVzdEFuaW1hdGlvbkZyYW1lcy4gQ2FuIGJlIHVzZWQgdG8gbGlzdGVuIGZvciB0aGUgVVBEQVRFIGV2ZW50IGZvciBhbmltYXRpb24gcHVycG9zZXMuXHJcbiAgICAgKiBTaW5nbGV0b24gZm9yIGVhc3kgYWNjZXNzIGV2ZXJ5d2hlcmUgaW4gdGhlIGFwcC5cclxuICAgICAqL1xyXG4gICAgcHJpdmF0ZSBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgICAgIGxldCBzZWxmID0gdGhpcztcclxuICAgICAgICB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lKGZ1bmN0aW9uIGdhbWVMb29wKHRpbWVGcm9tU3RhcnQ6IG51bWJlcikge1xyXG4gICAgICAgICAgICB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lKGdhbWVMb29wKTtcclxuICAgICAgICAgICAgc2VsZi5lbWl0KEV2ZW50cy5VUERBVEUpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgZ2V0IEluc3RhbmNlKCkge1xyXG4gICAgICAgIC8vIERvIHlvdSBuZWVkIGFyZ3VtZW50cz8gTWFrZSBpdCBhIHJlZ3VsYXIgbWV0aG9kIGluc3RlYWQuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2luc3RhbmNlIHx8ICh0aGlzLl9pbnN0YW5jZSA9IG5ldyB0aGlzKCkpO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgY29uc3QgRXZlbnRzID0ge1xyXG4gICAgLy8gbW91c2UgZXZlbnRzXHJcbiAgICBVUERBVEU6IFN5bWJvbCgndXBkYXRlIGV2ZW50JyksXHJcbiAgICBNT1VTRU1PVkU6IFN5bWJvbCgnbW91c2Vtb3ZlIGV2ZW50JyksXHJcbiAgICBNT1VTRUVOVEVSOiBTeW1ib2woJ21vdXNlZW50ZXIgZXZlbnQnKSxcclxuICAgIE1PVVNFTEVBVkU6IFN5bWJvbCgnbW91c2VsZWF2ZSBldmVudCcpLFxyXG4gICAgTU9VU0VET1dOOiBTeW1ib2woJ21vdXNlZG93biBldmVudCcpLFxyXG4gICAgTU9VU0VVUDogU3ltYm9sKCdtb3VzZWRvd24gZXZlbnQnKSxcclxuICAgIENMSUNLOiBTeW1ib2woJ2NsaWNrIGV2ZW50JyksXHJcblxyXG4gICAgLy8gc3BlY2lmaWMgZXZlbnRzXHJcbiAgICBDT01QTEVURTogU3ltYm9sKCdvbmNvbXBsZXRlIGV2ZW50JyksXHJcbiAgICBGQURFX0lOOiBTeW1ib2woJ2ZhZGUgaW4gZmluaXNoZWQgZXZlbnQnKSxcclxuICAgIEZBREVfT1VUOiBTeW1ib2woJ2ZhZGUgb3V0IGZpbmlzZWQgZXZlbnQnKSxcclxuICAgIEdBTUVfU1RBUlQ6IFN5bWJvbCgnZ2FtZSBzdGFydCBldmVudCcpLFxyXG4gICAgR0FNRV9PVkVSOiBTeW1ib2woJ2dhbWUgb3ZlciBldmVudCcpLFxyXG4gICAgRVhJVDogU3ltYm9sKCdleGl0IGV2ZW50JyksXHJcbiAgICBDT0xMSVNJT046IFN5bWJvbCgnY29sbGlzaW9uIGV2ZW50JyksXHJcbn1cclxuXHJcbmV4cG9ydCBjb25zdCBUaWNrZXIgPSBfVGlja2VyLkluc3RhbmNlO1xyXG5UaWNrZXIuc2V0TWF4TGlzdGVuZXJzKDMwKTsiLCJpbXBvcnQgR2FtZUNvbnRyb2xsZXIgZnJvbSBcIi4vR2FtZUNvbnRyb2xsZXJcIjtcclxuaW1wb3J0IHsgVGlja2VyLCBFdmVudHMgfSBmcm9tIFwiLi9UaWNrZXJcIjtcclxuXHJcbi8vIHN0YXJ0IGFwcGxpY2F0aW9uXHJcbmNvbnN0IGdhbWUgPSBuZXcgR2FtZUNvbnRyb2xsZXIoPEhUTUxDYW52YXNFbGVtZW50PmRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdzdGFnZScpKTsiXX0=
