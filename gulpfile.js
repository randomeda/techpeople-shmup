var gulp = require("gulp");
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var browserify = require("browserify");
var source = require('vinyl-source-stream');
var tsify = require("tsify");
const autoprefixer = require('gulp-autoprefixer');
var paths = {
    sass: ['./src/sass/**/*.scss'],
    typescript: ['./src/ts/**/*.ts']
};

gulp.task('sass', function () {
    return gulp.src(paths.sass)
        .pipe(sourcemaps.init())
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./assets/css'));
});

gulp.task("typescript", function () {
    return browserify({
        basedir: '.',
        debug: true,
        entries: ['src/ts/app.ts'],
        cache: {},
        packageCache: {}
    })
        .plugin(tsify)
        .bundle()
        .on('error', swallowError)
        .pipe(source('app.js'))
        .pipe(gulp.dest("./assets/js"));
});

// Rerun the task when a file changes
gulp.task('watch', function () {
    gulp.watch(paths.sass, ['sass']);
    gulp.watch(paths.typescript, ['typescript']);
});

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['sass', 'typescript']);

function swallowError(error) {

    // If you want details of the error in the console
    console.log(error.toString())

    this.emit('end')
}