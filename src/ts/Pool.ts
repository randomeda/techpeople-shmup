import GameObject from "./GameObject";
import Container from "./Container";

export interface Poolable {
    active: boolean,
    reset(...args: any[]): void
    start(...args: any[]): void
}

export default class Pool<T extends Poolable> {
    private _pool: T[];
    private _args: any[];

    /**
     * Generic object pool for saving processing power.
     * @param _class class to be instantiated
     * @param args constructor arguments for the class
     */
    constructor(private _class: { new(...args: any[]): T; }, ...args: any[]) {
        this._args = args;
        this._pool = [];
    }

    /**
     * Deactivates all pool objects
     */
    deactivateAll() {
        this._pool.forEach(item => item.reset());
    }

    /**
     * Finds an inactive object or creates one if there is none.
     * @param args arguments to the start function if has any
     */
    activateOne(...args: any[]): T {
        let item = this._pool.find(item => item.active === false) || this.create();
        item.start(...args);
        return item;
    }

    /**
     * Instantiates the class
     */
    create(): T {
        let instance = new this._class(...this._args);
        this._pool.push(instance);
        return instance;
    }
}