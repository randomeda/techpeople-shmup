import Preloader from "./Preloader";
import Stage from "./Stage";
import GraphicImage from "./GraphicImage";
import Container from "./Container";
import Text from "./Text";

export default class SplashScreen extends Container {
    /**
     * Simple background with a title text.
     * @param stage stage
     * @param preloader used to get images
     */
    constructor(public stage: Stage, public preloader: Preloader) {
        super();

        // gather images
        let bg = new GraphicImage(this.preloader.getByName('splash').bitmap);
        let text = new Text('ORBLASTER', '#fff', '40px Orbitron', 'center');
        text.x = this.stage.canvas.width / 2;
        text.y = this.stage.canvas.height / 3 * 2;

        this.addChild(bg);
        this.addChild(text);
    }
}