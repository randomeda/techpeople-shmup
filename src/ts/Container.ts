import DisplayObject from "./DisplayObject";
import { Events } from "./Ticker";
import { PointerPosition } from "./Stage";

export default class Container extends DisplayObject {
    public children: DisplayObject[] = [];

    /**
     * Adds the object to the container. will not add the object twice.
     * @param displayObject object to be added
     */
    addChild(displayObject: DisplayObject): void {
        if (!this.children.find(child => child === displayObject)) {
            displayObject.parent = this;
            this.children.push(displayObject);
        }
    }

    /**
     * Removes the object from the container if exists
     * @param child object to be removed
     */
    removeChild(child: DisplayObject): void {
        child.parent = undefined;
        child.ctx = undefined;
        child.stage = undefined;
        this.children.splice(this.children.indexOf(child), 1);
    }

    /**
     * Updates all children
     */
    update(): void {
        if (!this.visible) return;
        this.children.forEach(gameObject => {
            if (gameObject.visible) {
                gameObject.update();
            }
        });
    }

    /**
     * Mouse events are passed down to all children for calculation
     * @param mouse mouse position
     */
    _onMouseDown(mouse: PointerPosition) {
        super._onMouseDown(mouse);
        this.children.forEach(child => child._onMouseDown(mouse));
    }

    /**
     * Mouse events are passed down to all children for calculation
     */
    _onMouseUp() {
        super._onMouseUp();
        this.children.forEach(child => child._onMouseUp());
    }

    /**
     * Mouse events are passed down to all children for calculation
     * @param mouse mouse position
     */
    _onClick(mouse: PointerPosition) {
        super._onClick(mouse);
        this.children.forEach(child => child._onClick(mouse));
    }

    /**
     * Mouse events are passed down to all children for calculation
     * @param mouse mouse position
     */
    _onMouseMove(mouse: PointerPosition) {
        super._onMouseMove(mouse);
        this.children.forEach(child => child._onMouseMove(mouse));
    }
}