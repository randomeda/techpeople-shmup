import { EventEmitter } from "events";
import Stage, { PointerPosition } from "./Stage";
import { Events } from "./Ticker";
import Container from "./Container";

export default class DisplayObject extends EventEmitter implements Updateable {
    localX: number = 0; // coordinates relative to the parent
    localY: number = 0; // coordinates relative to the parent
    localOpacity: number = 1; // own opacity value
    parent: Container;
    bounds: Bounds = { width: 0, height: 0 };
    visible: boolean = true;

    protected _stage: Stage;

    private _ctx: CanvasRenderingContext2D; // saved canvs rendering context for easy access
    private _mouseover = false;
    private _mousedown = false;

    /**
     * Checks if pointer is within the bounds of this object
     * @param mosue pointer position
     */
    checkPointerPosition(mosue: PointerPosition): boolean {
        return mosue.x > this.x
            && mosue.x < this.x + this.bounds.width
            && mosue.y > this.y
            && mosue.y < this.y + this.bounds.height
    }

    _onMouseDown(mouse: PointerPosition) {
        if (!this._mousedown && this.checkPointerPosition(mouse)) {
            //pointer over gameobject
            this._mousedown = true;
            this.emit(Events.MOUSEDOWN, mouse);
        }
    }

    _onMouseUp() {
        if (this._mousedown) {
            this.emit(Events.MOUSEUP, event);
            this._mousedown = false;
        }
    }

    _onClick(mouse: PointerPosition) {
        if (this.checkPointerPosition(mouse)) {
            //pointer over gameobject
            this.emit(Events.CLICK, mouse);
        }
    }

    _onMouseMove(mouse: PointerPosition) {
        if (this.checkPointerPosition(mouse)) {
            //pointer over gameobject
            this.emit(Events.MOUSEMOVE, mouse);
            if (!this._mouseover) {
                this.emit(Events.MOUSEENTER, mouse);
                this._mouseover = true;
            }
        } else {
            //pointer left gameobject
            if (this._mouseover) {
                this.emit(Events.MOUSELEAVE, mouse);
                this._mouseover = false;
            }
        }
    }

    /**
     * Runs in every animation frame
     */
    update() {
        // override in child class
    }

    /**
     * Sends object to the top of all objects.
     */
    sendToFront() {
        let parent = this.parent;
        if (parent) {
            parent.removeChild(this);
            parent.addChild(this);
        }
    }

    /**
     * Removes this object from its container, so it wont be visible,
     * and makes it ready for garbage collection.
     */
    remove() {
        this.parent.removeChild(this);
    }

    /**
     * Gets the stage recursively from the topmost parent
     */
    get stage(): Stage {
        if (!this._stage && this.parent) {
            this._stage = this.parent.stage;
        }
        return this._stage;
    }

    set stage(stage: Stage) {
        this._stage = stage;
    }

    /**
     * final opacity is multiplied by all parents' opacities
     */
    get opacity(): number {
        if (this.parent) {
            return this.localOpacity * this.parent.opacity;
        }
        return this.localOpacity;
    }

    set opacity(opacity: number) {
        this.localOpacity = opacity;
    }

    /**
     * Final coordinates are added from all parents' coordinates
     */
    get x(): number {
        if (this.parent) {
            return this.localX + this.parent.x;
        }
        return this.localX;
    }

    set x(x: number) {
        this.localX = x;
    }

    /**
     * Final coordinates are added from all parents' coordinates
     */
    get y(): number {
        if (this.parent) {
            return this.localY + this.parent.y;
        }
        return this.localY;
    }

    set y(y: number) {
        this.localY = y;
    }

    /**
     * Rendering context is searched recursively in the parent chain until found.
     */
    get ctx(): CanvasRenderingContext2D {
        if (!this._ctx && this.parent) {
            this._ctx = this.parent.ctx;
        }
        return this._ctx;
    }

    set ctx(ctx: CanvasRenderingContext2D) {
        this._ctx = ctx;
    }
}

export interface Updateable {
    update(): void
}

export interface Bounds {
    width: number
    height: number
}