import Stage from "./Stage";
import Text from "./Text";
import Container from "./Container";

export default class GameOverScreen extends Container {
    /**
     * Simple text in the center of the screen
     * @param stage stage
     */
    constructor(public stage: Stage) {
        super();
        let text = new Text('GAME OVER', '#fff', '32px Orbitron', 'center');
        text.x = this.stage.canvas.width / 2;
        text.y = this.stage.canvas.height / 2;
        this.addChild(text);
    }
}