import GameObject from "./GameObject";
import GraphicImage from "./GraphicImage";
import Preloader from "./Preloader";
import { Events } from "./Ticker";
import { PointerPosition } from "./Stage";
import Enemy from "./Enemy";
import Pool from "./Pool";
import Explosion from "./Explosion";

export default class Player extends GameObject {
    image: GraphicImage;
    mouseX: number;
    mouseY: number;

    private _followMouseProxy = (mouse: PointerPosition) => {
        this.mouseX = mouse.x - this.image.bounds.width / 2;
        this.mouseY = mouse.y - this.image.bounds.height / 2;
    }

    /**
     * Player ship that follows the pointer.
     * Fires GAME_OVER event when destroyed.
     * @param explosion pool used to handle explosions
     * @param preloader used to get loaded images
     */
    constructor(public explosion: Pool<Explosion>, public preloader: Preloader) {
        super();

        this.image = new GraphicImage(this.preloader.getByName('ship').bitmap);
        this.image.x = 0;
        this.image.y = 0;
        this.x = this.mouseX = 50;
        this.y = this.mouseY = 300;

        this.addChild(this.image);
        this.bounds = this.image.bounds;
    }

    onCollision(go: GameObject) {
        if (go instanceof Enemy) {
            this.emit(Events.GAME_OVER);
            this.destroy();
        }
    }

    start() {
        this.visible = true;
        this.addEvents();
        super.start();
    }

    reset() {
        this.visible = false;
        this.removeEvents();
        super.reset();
    }

    addEvents() {
        this.stage.on(Events.MOUSEMOVE, this._followMouseProxy);
    }

    removeEvents() {
        this.stage.removeListener(Events.MOUSEMOVE, this._followMouseProxy);
    }

    update() {
        this.x = this.x - (this.x - this.mouseX) / 5;
        this.y = this.y - (this.y - this.mouseY) / 5;
        super.update();
    }

    destroy() {
        this.explosion.activateOne(this.x, this.y);
        this.reset();
    }
}