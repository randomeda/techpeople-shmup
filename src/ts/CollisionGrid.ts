import GameObject from "./GameObject";
import { GridPosition } from "./CollisionDetector";

export default class Grid {
    cells = [];

    /**
     * The grid is responsible for storing the objects in cells. Cells
     * represen bigger parts of the screen. If the objects are in the same cell,
     * their collisions will be calculated.
     * @param size size of the grid in pixels
     * @param parent if a higher level grid exists, this is it.
     */
    constructor(public size: number, public parent: Grid) {
        //init
        this.clear();
    }

    /**
     * Calculates cell positions for the given object and stores it in the cells.
     * @param go 
     */
    addToCells(go: GameObject) {
        let positions = go.positions;
        for (let i = 0; i < positions.length; i++) {
            if (!this.cells[positions[i].x]) {
                this.cells[positions[i].x] = [];
            }
            if (!this.cells[positions[i].x][positions[i].y]) {
                this.cells[positions[i].x][positions[i].y] = [];
            }
            this.cells[positions[i].x][positions[i].y].push(go);
        }
        if (this.parent) {
            // add to parent cells too
            this.parent.addToCells(go);
        }
    }

    /**
     * Deletes all objects from the grid.
     */
    clear() {
        this.cells = [];
    }

    /**
     * Returns all the objects that are in the given cell.
     * @param pos gird cell coordinates
     */
    getObjectsInCell(pos: GridPosition): GameObject[] {
        let objects: GameObject[] = [];

        if (this.parent) {
            let parentPos: GridPosition = { x: Math.floor(pos.x / 2), y: Math.floor(pos.y / 2) };
            objects = objects.concat(this.parent.getObjectsInCell(parentPos));
        }

        if (this.cells[pos.x] && this.cells[pos.x][pos.y]) {
            return objects.concat(this.cells[pos.x][pos.y]);
        } else {
            return [];
        }
    }
}