import DisplayObject from "./DisplayObject";

export default class Rect extends DisplayObject {
    public stroke = '';

    /**
     * Creates a colored rectangle.
     * @param color color of the rectangle
     * @param width width of the rectangle
     * @param height height of the rectangle
     */
    constructor(public color: string, public width: number, public height: number) {
        super();

        this.bounds = { width: this.width, height: this.height };
    }

    update(): void {
        this.ctx.globalAlpha = this.opacity;
        this.ctx.fillStyle = this.color;
        this.ctx.strokeStyle = this.stroke;
        this.ctx.lineWidth = 1;
        this.ctx.strokeRect(this.x, this.y, this.width, this.height);
        this.ctx.fillRect(this.x, this.y, this.width, this.height);
    }
}