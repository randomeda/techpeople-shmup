import Stage, { PointerPosition } from "./Stage";
import Preloader from "./Preloader";
import Container from "./Container";
import GraphicImage from "./GraphicImage";
import { Ticker, Events } from "./Ticker";
import Player from "./Player";
import Enemy from "./Enemy";
import CollisionDetector from "./CollisionDetector";
import Pool from "./Pool";
import Explosion from "./Explosion";
import Bullet from "./Bullets";

export default class GameScreen extends Container {
    starsContainer: Container;
    nebulaContainer: Container;
    explosionContainer: Container;
    hitContainer: Container;
    player: Player;
    collisions: CollisionDetector;
    spawnTime: number = 1;
    enemyPool: Pool<Enemy>;
    explosionPool: Pool<Explosion>;
    bulletPool: Pool<Bullet>;

    private _spawnInterval: number;
    private _fireInterval: number;
    private _fireTime: number = 0.15;

    /**
     * Loads the game and controls the game mechanics.
     * Manages Bullets, Enemies and the Player object.
     * Moves the parallax background.
     * @param stage stage
     * @param preloader used to get loaded images
     */
    constructor(public stage: Stage, public preloader: Preloader) {
        super();

        // init container for explosions and backgrounds (we dont want to include them in collision detection)
        this.explosionContainer = new Container();
        this.starsContainer = new Container();
        this.nebulaContainer = new Container();
        this.hitContainer = new Container();
        this.nebulaContainer.opacity = 0.9;

        // craete pools
        this.explosionPool = new Pool<Explosion>(Explosion, this.explosionContainer);
        this.enemyPool = new Pool<Enemy>(Enemy, this.explosionPool, this.preloader);

        // init collision detection
        this.collisions = new CollisionDetector(1, 200, this.hitContainer);

        // init parallax backgrounds
        let stars1 = new GraphicImage(this.preloader.getByName('stars').bitmap);
        let stars2 = new GraphicImage(this.preloader.getByName('stars').bitmap);
        stars2.x = this.stage.bounds.width - 1;
        let nebula1 = new GraphicImage(this.preloader.getByName('nebula').bitmap);
        let nebula2 = new GraphicImage(this.preloader.getByName('nebula').bitmap);
        nebula2.x = this.stage.bounds.width - 1;

        this.starsContainer.addChild(stars1);
        this.starsContainer.addChild(stars2);

        this.nebulaContainer.addChild(nebula1);
        this.nebulaContainer.addChild(nebula2);

        // init player
        this.player = new Player(this.explosionPool, this.preloader);
        this.player.on(Events.GAME_OVER, () => {
            this.emit(Events.GAME_OVER);
        });
        // init bullet pool (requires player object)
        this.bulletPool = new Pool<Bullet>(Bullet, this.preloader, this.player);

        // add all to stage
        this.addChild(this.nebulaContainer);
        this.addChild(this.starsContainer);
        this.addChild(this.explosionContainer);
        this.addChild(this.hitContainer);
        this.hitContainer.addChild(this.player);
    }

    /**
     * Show the game screen, start player movement, spawns enemies and bullets
     */
    show() {
        this.visible = true;
        this.player.start();

        this._spawnInterval = window.setInterval(() => {
            this.hitContainer.addChild(this.enemyPool.activateOne());
        }, this.spawnTime * 1000);

        this._fireInterval = window.setInterval(() => {
            this.hitContainer.addChild(this.bulletPool.activateOne());
        }, this._fireTime * 1000);
    }

    /**
     * Stops all game mechanics and spawns
     */
    hide() {
        this.visible = false;
        this.player.reset();
        this.enemyPool.deactivateAll();
        window.clearInterval(this._fireInterval);
        clearInterval(this._spawnInterval);
    }

    /**
     * Moves the background, calculates collisions
     */
    update() {
        this.starsContainer.x += this.starsContainer.x - 1 <= -this.stage.bounds.width ? this.stage.bounds.width : -1;
        this.nebulaContainer.x += this.nebulaContainer.x - 3 <= -this.stage.bounds.width ? this.stage.bounds.width : -3;
        this.collisions.update();
        super.update();
    }
}