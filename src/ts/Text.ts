import DisplayObject, { Bounds } from "./DisplayObject";

export default class Text extends DisplayObject {
    /**
     * Simple text object for use on the canvas
     * @param text text
     * @param color color
     * @param font CSS font definition
     * @param align text alignment
     */
    constructor(public text: string, public color: string, public font: string, public align: string) {
        super();
        
        let textHTML = document.createElement("div");
        textHTML.innerHTML = this.text;
        textHTML.style.position = 'absolute';
        textHTML.style.top = '-9999px';
        textHTML.style.left = '-9999px';
        textHTML.style.font = this.font;
        document.body.appendChild(textHTML);
        this.bounds = { width: textHTML.offsetWidth, height: textHTML.offsetHeight };
        document.body.removeChild(textHTML);
    }

    update(): void {
        this.ctx.globalAlpha = this.opacity;
        this.ctx.font = this.font;
        this.ctx.fillStyle = this.color;
        this.ctx.textAlign = this.align;
        this.ctx.fillText(this.text, this.x, this.y);
    }
}