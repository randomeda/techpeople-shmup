import DisplayObject from "./DisplayObject";
import Rect from "./Rect";
import Text from "./Text";
import Container from "./Container";
import { Events } from "./Ticker";

export default class Button extends Container {
    private _rect: Rect;
    private _text: Text;

    /**
     * Creates a button with the given text
     * @param text Button text
     */
    constructor(public text: string) {
        super();

        // adding button elements
        this._text = new Text(this.text, '#fff', '16px Orbitron', 'center');
        this._text.x = 0;
        this._text.y = 7;
        this._rect = new Rect('#000', 250, this._text.bounds.height + 15);
        this._rect.stroke = '#fff';
        this._rect.x = -this._rect.bounds.width / 2;
        this._rect.y = -this._rect.bounds.height / 2;
        this._rect.opacity = 0.3;
        this.addChild(this._rect);
        this.addChild(this._text);

        // mouse events
        this._rect.on(Events.MOUSEENTER, (event) => { this.onMouseEnter(event) });
        this._rect.on(Events.MOUSELEAVE, (event) => { this.onMouseLeave(event) });
        this._rect.on(Events.MOUSEDOWN, (event) => { this.onMouseDown(event) });
        this._rect.on(Events.MOUSEUP, (event) => { this.onMouseUp() });
        this._rect.on(Events.CLICK, (event) => { this.onClick(event) });
    }

    onMouseEnter(event: MouseEvent) {
        this._rect.color = '#4286f4';
        this._rect.opacity = 0.5;
    }

    onMouseLeave(event: MouseEvent) {
        this._rect.color = '#000';
        this._rect.opacity = 0.3;
    }

    onMouseDown(event: MouseEvent) {
        this._rect.color = '#2e63ba';
    }

    onMouseUp() {
        this._rect.color = '#4286f4';
    }

    onClick(event: MouseEvent) {
        this.emit(Events.CLICK, event);
    }

    destroy() {
        this._rect.removeAllListeners(Events.MOUSEENTER);
        this._rect.removeAllListeners(Events.MOUSELEAVE);

        this.remove();
    }
}