import Rect from "./Rect";
import Stage from "./Stage";
import Animation from "./Animation";
import { EventEmitter } from "events";
import { Events } from "./Ticker";
import { Easings } from "./Easings";

export default class FadeScreen extends Rect {
    private _fadeOutAnim: Animation;
    private _fadeInAnim: Animation;
    private _onFadeOutProxy = () => {
        this.emit(Events.FADE_OUT);
    };
    private _onFadeInProxy = () => {
        this.emit(Events.FADE_IN);
    };

    /**
     * Creates a black screen which is used to cover the canvas to create a fade in / fade out effect.
     * Will emit FADE_OUT and FADE_IN events when the animation is finished
     * @param stage 
     */
    constructor(public stage: Stage) {
        super('#000', stage.bounds.width, stage.bounds.height);

        this._fadeOutAnim = new Animation(0.7, this, { localOpacity: 0 }, { easing: Easings.easeInQuad });
        this._fadeInAnim = new Animation(0.7, this, { localOpacity: 1 }, { easing: Easings.easeOutQuad });

        this._fadeOutAnim.on(Events.COMPLETE, this._onFadeOutProxy);
        this._fadeInAnim.on(Events.COMPLETE, this._onFadeInProxy);
    }

    fadeIn() {
        if (this.opacity === 1) {
            this._onFadeInProxy();
            return;
        }
        this.sendToFront();
        this._fadeInAnim.play();
    }

    fadeOut() {
        if (this.opacity === 0) {
            this._onFadeInProxy();
            return;
        }
        this.sendToFront();
        this._fadeOutAnim.play();
    }

    stop() {
        this._fadeOutAnim.stop();
        this._fadeInAnim.stop();
    }
    
    destroy() {
        this.stop();
        this._fadeOutAnim.removeAllListeners();
        this._fadeInAnim.removeAllListeners();
        super.remove();
    }
}
