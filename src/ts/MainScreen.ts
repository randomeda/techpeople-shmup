import Preloader from "./Preloader";
import Stage from "./Stage";
import Text from "./Text";
import Container from "./Container";
import GraphicImage from "./GraphicImage";
import { Updateable } from "./DisplayObject";
import { Ticker, Events } from "./Ticker";
import Button from "./Button";

export default class MainScreen extends Container {
    private _comets: GraphicImage[] = [];
    private _startGameProxy = () => { this.emit(Events.GAME_START) };
    private _exitProxy = () => { this.emit(Events.EXIT) };

    // buttons
    private _button1: Button;
    private _button2: Button;
    private _button3: Button;
    private _button4: Button;

    /**
     * Creates the menu screen with 4 buttons, Moving background, logo, and title text.
     * @param stage stage
     * @param preloader used to get loaded images
     */
    constructor(public stage: Stage, public preloader: Preloader) {
        super();

        // gather images
        let bg = new GraphicImage(preloader.getByName('main').bitmap);
        this.addChild(bg);

        // create logo
        let logo = new GraphicImage(preloader.getByName('logo').bitmap);
        logo.x = this.stage.canvas.width / 2 - logo.image.width / 2;
        logo.y = this.stage.canvas.height / 3 - 20;
        this.addChild(logo);

        // create title text
        let titleText = new Text('ORBLASTER', '#fff', '32px Orbitron', 'center');
        titleText.x = this.stage.canvas.width / 2;
        titleText.y = this.stage.canvas.height / 4;
        this.addChild(titleText);

        // create buttons
        this.createButtons();

        // create comets
        this.createComets(5);
    }

    createButtons() {
        // create buttons
        this._button1 = new Button('GAME 1');
        this._button2 = new Button('GAME 2');
        this._button3 = new Button('GAME 3');
        this._button4 = new Button('EXIT');
        this._button1.x = this._button2.x = this._button3.x = this._button4.x = this.stage.canvas.width / 2;
        this._button1.y = this.stage.canvas.height / 2 + 75;
        this._button2.y = this._button1.y + 43;
        this._button3.y = this._button2.y + 43;
        this._button4.y = this._button3.y + 60;

        // add them to the stage
        this.addChild(this._button1);
        this.addChild(this._button2);
        this.addChild(this._button3);
        this.addChild(this._button4);
    }

    show() {
        this.visible = true;
        this._button1.on(Events.CLICK, this._startGameProxy);
        this._button2.on(Events.CLICK, this._startGameProxy);
        this._button3.on(Events.CLICK, this._startGameProxy);
        this._button4.on(Events.CLICK, this._exitProxy);
    }

    hide() {
        this.visible = false;
        this._button1.removeAllListeners(Events.CLICK);
        this._button2.removeAllListeners(Events.CLICK);
        this._button3.removeAllListeners(Events.CLICK);
        this._button4.removeAllListeners(Events.CLICK);
    }

    createComets(count: number) {
        let cometTypes: string[] = ['blue comet', 'purple comet'];
        for (let i = 0; i < count; i++) {
            let comet = new GraphicImage(this.preloader.getByName(cometTypes[Math.round(Math.random())]).bitmap);
            comet.x = Math.random() * this.stage.canvas.width - comet.image.width;
            comet.y = Math.random() * this.stage.canvas.height - comet.image.height;
            this.addChild(comet);
            this._comets.push(comet);
        }
    }

    /**
     * Moves the comets
     */
    update() {
        this._comets.forEach(comet => {
            comet.x = comet.localX + 1.9 * 2;
            comet.y = comet.localY + 1 * 2;
            if (comet.localX > this.stage.canvas.width) {
                comet.x = -comet.image.width;
            }
            if (comet.localY > this.stage.canvas.height) {
                comet.y = -comet.image.height;
            }
        });
        super.update();
    }

    destroy(): void {
        this.hide();
        this._comets = [];
        super.remove();
    }
}