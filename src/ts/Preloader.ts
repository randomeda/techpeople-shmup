import { EventEmitter } from "events";
import { Events } from "./Ticker";

export default class Preloader extends EventEmitter {
    public resources: ProcessedImageResource[] = [];

    /**
     * Preloads the images, then fires a COMPLETE event.
     * @param images images to be loaded
     * @param imgPath path to be prepended beore image names
     */
    constructor(images: PreloadImageResource[], imgPath: string) {
        super();

        let promises: Promise<string>[] = [];
        images.forEach(image => {
            let promise = new Promise<string>((resolve, reject) => {
                let src = imgPath + image.src;
                let img = new Image();
                img.src = src;
                let splash: ImageBitmap;
                img.onload = () => {
                    createImageBitmap(img, 0, 0, img.naturalWidth, img.naturalHeight).then((imageBitmap) => {
                        this.resources.push({
                            name: image.name,
                            src: src,
                            width: imageBitmap.width,
                            height: imageBitmap.height,
                            bitmap: imageBitmap
                        });
                    });
                    resolve('image loaded');
                }
                img.onerror = function (e) {
                    reject(e);
                };
            });
            promises.push(promise);
        });

        Promise.all(promises).then(() => {
            this.emit(Events.COMPLETE);
        });
    }

    /**
     * Get the resource by its name
     * @param name name of the resource
     */
    public getByName(name: string): ProcessedImageResource {
        return this.resources.find(resource => resource.name === name);
    }
}

interface PreloadImageResource {
    name: string
    src: string,
}

interface ProcessedImageResource {
    name: string,
    src: string,
    width: number,
    height: number,
    bitmap: ImageBitmap
}