import GameObject from "./GameObject";
import Preloader from "./Preloader";
import GraphicImage from "./GraphicImage";
import Player from "./Player";
import Enemy from "./Enemy";

export default class Bullet extends GameObject {
    image: GraphicImage;
    speed = 30;

    /**
     * Creates a bullet which travel along the x axis.
     * @param preloader Used to get the bullet image
     * @param player Used to get starting coordinates
     */
    constructor(public preloader: Preloader, public player: Player) {
        super();

        this.image = new GraphicImage(preloader.getByName('bullet').bitmap);
        this.bounds = this.image.bounds;

        this.addChild(this.image);
    }

    onCollision(go: GameObject) {
        if (go instanceof Enemy) {
            this.destroy();
        }
    }

    reset() {
        this.opacity = 0;
        this.active = false;
        super.reset();
    }

    start() {
        this.opacity = 1;
        this.x = this.player.x;
        this.y = this.player.y;
        if (this.active) return;
        this.active = true;
        super.start();
    }

    update() {
        if (!this.active) return;
        this.x += this.speed;
        if (this.x > this.stage.bounds.width) {
            this.destroy();
        }
        super.update();
    }

    destroy() {
        this.reset();
        super.destroy();
    }
}