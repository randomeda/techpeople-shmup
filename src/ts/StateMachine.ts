import { EventEmitter } from "events";
import { Events } from "./Ticker";
import GameController from "./GameController";

export interface StateMachineConfig {
    [index: string]: State
}

export interface State {
    [index: string]: Transition;
}

export interface Transition {
    beforeFadeIn: () => void,
    to: string,
    delay?: number // in seconds
}

export default class StateMachine extends EventEmitter {
    private _delayTimeout: number;

    /**
     * State-machine-like class which specializez in transitioning screens.
     * @param gc game controller object
     * @param states states the machine is allowed to transition to.
     * @param currentState stores the current state
     */
    constructor(public gc: GameController, public states: StateMachineConfig, public currentState: string) {
        super();
    }

    /**
     * If the current state has a transition given by the parameter,
     * then the state is changed to the new state, and the transition
     * funciton is played.
     * @param transition transition name
     */
    transition(transition: string) {
        let t = this.states[this.currentState][transition];
        if (t) {
            this.gc.fader.stop();
            window.clearTimeout(this._delayTimeout);
            this.currentState = t.to;
            this.play(t);
        }
    }

    /**
     * Fires a complete event
     */
    complete() {
        this.emit(Events.COMPLETE);
    }

    /**
     * Runs the chosen transition and applies a delay between before the next transition if necessary
     * @param t tarnsition to be run
     */
    play(t: Transition) {
        // start prev fade out
        this.gc.fader.on(Events.FADE_IN, () => {
            // on prev fade out
            this.gc.fader.removeAllListeners();
            // setup logic goes here
            t.beforeFadeIn();

            // start to fade in new screen
            this.gc.fader.on(Events.FADE_OUT, () => {
                this.gc.fader.removeAllListeners();
                // wait for x seconds before next screen
                if (t.delay) {
                    this._delayTimeout = window.setTimeout(() => { this.complete() }, t.delay * 1000);
                } else {
                    this.complete();
                }
            });
            this.gc.fader.fadeOut();
        });
        this.gc.fader.fadeIn();
    }
}
