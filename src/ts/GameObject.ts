import Container from "./Container";
import { GridPosition } from "./CollisionDetector";
import { Poolable } from "./Pool";
import { Events } from "./Ticker";
import Grid from "./CollisionGrid";

/**
 * Base class for in-game objects. Turns collision detection on / off automatically.
 */
export default class GameObject extends Container implements Poolable {
    grid: Grid;
    positions: GridPosition[];
    active: boolean;

    private _onCollisionProxy = (go: GameObject) => {
        this.onCollision(go)
    };

    onCollision(go: GameObject) { }

    start() {
        this.on(Events.COLLISION, this._onCollisionProxy);
    }

    reset() {
        this.removeListener(Events.COLLISION, this._onCollisionProxy);
    }

    destroy() {
        this.parent.removeChild(this);
    }
}