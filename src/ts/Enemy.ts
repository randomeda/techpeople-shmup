import GraphicImage from "./GraphicImage";
import Preloader from "./Preloader";
import GameObject from "./GameObject";
import { Events } from "./Ticker";
import Animation from "./Animation";
import { Easings } from "./Easings";
import Player from "./Player";
import Bullet from "./Bullets";
import Pool from "./Pool";
import Explosion from "./Explosion";

export default class Enemy extends GameObject {
    image: GraphicImage;
    active: boolean = false;
    moveY = 100;

    private _originalY = Math.random() * 500 + 50;
    private _originalX = 800;
    private _moveYAnim: Animation;
    private _moveXAnim: Animation;

    /**
     * Creates a randomly moving enemy
     * @param explosion Pool used to instantiate explosions
     * @param preloader used to get images
     */
    constructor(public explosion: Pool<Explosion>, public preloader: Preloader) {
        super();

        // set up visuals
        this.image = new GraphicImage(this.preloader.getByName('enemy').bitmap);
        this.image.x = 0;
        this.image.y = 0;
        this.addChild(this.image);
        this.bounds = this.image.bounds;

        // set starting point
        this.x = this._originalX;
        this.y = this._originalY;

        // moves in sine motion vertically
        this._moveYAnim = new Animation(2, this, { moveY: 100 }, { from: { moveY: 0 }, easing: Easings.sin, loop: true });

        // moves linearly from right to left
        this._moveXAnim = new Animation(6, this, { x: -this.bounds.width }, { from: { x: this._originalX }, easing: Easings.linear });
        this._moveXAnim.on(Events.COMPLETE, () => this.reset());
    }

    onCollision(go: GameObject) {
        if (go instanceof Bullet || go instanceof Player) {
            this.destroy();
        }
    }

    reset() {
        this.removeEvents();
        this.active = false;
        this.x = this._originalX;
        super.reset();
    }

    start() {
        if (this.active) return;
        this.addEvents();
        this.active = true;
        this._originalY = Math.random() * 500 + 50;
        super.start();
    }

    addEvents() {
        this._moveYAnim.play();
        this._moveXAnim.play();
    }

    removeEvents() {
        this._moveYAnim.stop();
        this._moveXAnim.stop();
    }

    update() {
        if (!this.active) {
            return;
        }
        this.y = this._originalY + this.moveY;
        super.update();
    }

    explode() {
        this.explosion.activateOne(this.x + this.bounds.width / 2, this.y + this.bounds.height / 2);
    }

    destroy() {
        this.explode();
        this.reset();
        super.destroy();
    }
}