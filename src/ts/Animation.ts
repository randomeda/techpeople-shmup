import { Ticker, Events } from "./Ticker";
import { Easing, Easings } from "./Easings";
import { EventEmitter } from "events";

/**
 * aaaa
 */
export default class Animation extends EventEmitter {
    from: NumericPropertyObject;

    private _loop: boolean = false;
    private _easing: Easing = Easings.linear;
    private _startTime: number;
    private _duration: number;
    private _updateProxy = () => this.update();

    /**
     * Animates object properties over time. Can loop if necessary.
     * @param duration duration of the animation in seconds
     * @param target target object whos properties will be animated
     * @param to property values the target's properties will be animated to
     * @param config additional configuration.
     * loop - loops the animation.
     * from - specifies starting properties. if not specified, the current property values will be used instead.
     * easing - easing funciton
     */
    constructor(public duration: number, public target: Object, public to: NumericPropertyObject, public config: AnimationConfig = {}) {
        super();
        this.reset();
        this._easing = config.easing || Easings.linear;
        this._duration = this.duration * 1000;
        this._loop = config.loop;
    }

    /**
     * Resets starting time and properties to the original starting values. If no from object is given, the current
     * values will be used to create the from object.
     */
    reset() {
        if (this.config.from) {
            this.from = this.config.from;
            for (const prop of Object.keys(this.from)) {
                if (typeof this.from[prop] === 'number') {
                    this.target[prop] = this.from[prop];
                }
            }
        } else {
            this.from = {};
            for (const prop of Object.keys(this.to)) {
                if (typeof this.target[prop] === 'number') {
                    this.from[prop] = this.target[prop];
                }
            }
        }
        this._startTime = Date.now();
    }

    play() {
        this.reset();
        Ticker.on(Events.UPDATE, this._updateProxy);
    }

    stop() {
        Ticker.removeListener(Events.UPDATE, this._updateProxy);
    }

    update() {
        let currentTime = Date.now() - this._startTime;
        for (const prop of Object.keys(this.to)) {
            this.target[prop] = this.from[prop] + (this.to[prop] - this.from[prop]) * this._easing(Math.min(currentTime / this._duration, 1));
        }
        if (currentTime >= this._duration) {
            if (this._loop) {
                this.reset()
            } else {
                this.stop();
                this.emit(Events.COMPLETE);
            }
        }
    }
}

/**
 * Ensures that only numeric properties are animated
 */
interface NumericPropertyObject {
    [index: string]: number
}

/**
 * Defines the config parametes for the Animation class
 */
interface AnimationConfig {
    from?: NumericPropertyObject
    loop?: boolean
    easing?: Easing
}