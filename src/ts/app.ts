import GameController from "./GameController";
import { Ticker, Events } from "./Ticker";

// start application
const game = new GameController(<HTMLCanvasElement>document.getElementById('stage'));