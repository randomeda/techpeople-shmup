import DisplayObject, { Bounds } from "./DisplayObject";

export default class GraphicImage extends DisplayObject {

    /**
     * Creates an image object that can be used on the canvas.
     * @param image bitmap used on the canvas
     */
    constructor(public image: ImageBitmap) {
        super();
        this.bounds = { width: this.image.width, height: this.image.height };
    }

    update(): void {
        this.ctx.globalAlpha = this.opacity;
        this.ctx.drawImage(this.image, this.x, this.y);
    }
}