import Stage from "./Stage";
import Preloader from "./Preloader";
import SplashScreen from "./SplashScreen";
import MainScreen from "./MainScreen";
import { Ticker, Events } from "./Ticker";
import GameScreen from "./GameScreen";
import GameOverScreen from "./GameOverScreen";
import FadeScreen from "./FadeScreen";
import StateMachine, { StateMachineConfig } from "./StateMachine";

export default class GameController {
    splash: SplashScreen;
    main: MainScreen;
    game: GameScreen;
    gameOver: GameOverScreen;
    fader: FadeScreen;
    sm: StateMachine;

    /**
     * Loads all images and manages screens with a state machine.
     * @param canvas canvas
     */
    constructor(public canvas: HTMLCanvasElement) {
        //preload images
        let preloader = new Preloader([
            { name: 'splash', src: '/splash.jpg' },
            { name: 'main', src: '/main.jpg' },
            { name: 'logo', src: '/logo.png' },
            { name: 'blue comet', src: '/blue.png' },
            { name: 'purple comet', src: '/purple.png' },
            { name: 'stars', src: '/stars.png' },
            { name: 'nebula', src: '/nebula_darkblue.png' },
            { name: 'ship', src: '/ship.png' },
            { name: 'enemy', src: '/enemy_sm.png' },
            { name: 'bullet', src: '/bullet.png' },
        ], '/img')

        preloader.on(Events.COMPLETE, () => {
            let stage = new Stage(canvas);
            Ticker.on(Events.UPDATE, () => stage.update());

            this.splash = new SplashScreen(stage, preloader);
            this.main = new MainScreen(stage, preloader);
            this.game = new GameScreen(stage, preloader);
            this.gameOver = new GameOverScreen(stage);
            this.fader = new FadeScreen(stage);

            this.splash.visible = false;
            this.main.visible = false;
            this.game.visible = false;
            this.gameOver.visible = false;
            this.fader.visible = true;

            stage.addChild(this.splash);
            stage.addChild(this.main);
            stage.addChild(this.game);
            stage.addChild(this.gameOver);
            stage.addChild(this.fader);

            this.start();
        });
    }

    start() {

        let states: StateMachineConfig = {
            black: {
                next: {
                    beforeFadeIn: () => {
                        this.splash.visible = true;
                    },
                    delay: 2,
                    to: 'splash'
                }
            },
            splash: {
                next: {
                    beforeFadeIn: () => {
                        this.splash.visible = false;
                        this.main.show();
                    },
                    to: 'main'
                }
            },
            main: {
                loadGame: {
                    beforeFadeIn: () => {
                        this.main.hide();
                        this.game.show();
                    },
                    to: 'game'
                },
                exit: {
                    beforeFadeIn: () => {
                        this.main.hide();
                        this.splash.visible = true;
                    },
                    delay: 2,
                    to: 'splash'
                }
            },
            game: {
                gameOver: {
                    beforeFadeIn: () => {
                        this.game.hide();
                        this.gameOver.visible = true;
                    },
                    delay: 2,
                    to: 'gameOver'
                }
            },
            gameOver: {
                next: {
                    beforeFadeIn: () => {
                        this.gameOver.visible = false;
                        this.splash.visible = true;
                    },
                    delay: 2,
                    to: 'splash'
                }
            }
        }

        // set up transition events
        this.sm = new StateMachine(this, states, 'black');
        this.sm.transition('next'); // first transition
        this.sm.on(Events.COMPLETE, () => this.sm.transition('next')); // general transition between scenes
        this.main.on(Events.GAME_START, () => this.sm.transition('loadGame')); // clicked on a game button
        this.main.on(Events.EXIT, () => this.sm.transition('exit')); // clicked on exit button
        this.game.on(Events.GAME_OVER, () => this.sm.transition('gameOver')); // player died
    }
}