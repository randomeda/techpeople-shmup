import { EventEmitter } from "events";

class _Ticker extends EventEmitter {
    private static _instance: _Ticker;

    /**
     * Handles RequestAnimationFrames. Can be used to listen for the UPDATE event for animation purposes.
     * Singleton for easy access everywhere in the app.
     */
    private constructor() {
        super();
        let self = this;
        window.requestAnimationFrame(function gameLoop(timeFromStart: number) {
            window.requestAnimationFrame(gameLoop);
            self.emit(Events.UPDATE);
        });
    }

    public static get Instance() {
        // Do you need arguments? Make it a regular method instead.
        return this._instance || (this._instance = new this());
    }
}

export const Events = {
    // mouse events
    UPDATE: Symbol('update event'),
    MOUSEMOVE: Symbol('mousemove event'),
    MOUSEENTER: Symbol('mouseenter event'),
    MOUSELEAVE: Symbol('mouseleave event'),
    MOUSEDOWN: Symbol('mousedown event'),
    MOUSEUP: Symbol('mousedown event'),
    CLICK: Symbol('click event'),

    // specific events
    COMPLETE: Symbol('oncomplete event'),
    FADE_IN: Symbol('fade in finished event'),
    FADE_OUT: Symbol('fade out finised event'),
    GAME_START: Symbol('game start event'),
    GAME_OVER: Symbol('game over event'),
    EXIT: Symbol('exit event'),
    COLLISION: Symbol('collision event'),
}

export const Ticker = _Ticker.Instance;
Ticker.setMaxListeners(30);