import Container from "./Container";
import Rect from "./Rect";

export interface PointerPosition {
    x: number,
    y: number
}

export default class Stage extends Container {
    /**
     * Manages drawing to the canvas. All Display objects must be added to the stage
     * to be drawn to the canvas. It has a solid black background.
     * Listens for mouse events and calculates mouse position on the canvas.
     */
    public mouse: PointerPosition = {
        x: 0,
        y: 0
    }
    public mouseX: number = 0;
    public mouseY: number = 0;

    private bg: Rect;

    constructor(public canvas: HTMLCanvasElement) {
        super();
        this.ctx = canvas.getContext('2d');
        this._stage = this;
        this.bg = new Rect('#000', this.canvas.width, this.canvas.height);
        this.addChild(this.bg);
        this.canvas.addEventListener('mousemove', event => { this.calculateMousePos(event); this._onMouseMove(this.mouse); });
        this.canvas.addEventListener('mousedown', event => { this.calculateMousePos(event); this._onMouseDown(this.mouse); });
        this.canvas.addEventListener('mouseup', event => { this.calculateMousePos(event); this._onMouseUp(); });
        this.canvas.addEventListener('click', event => { this.calculateMousePos(event); this._onClick(this.mouse); });

        this.bounds = { width: this.canvas.width, height: this.canvas.height };
    }

    calculateMousePos(event: MouseEvent) {
        let canvasBoundingRect = this.stage.canvas.getBoundingClientRect();
        this.mouse.x = event.pageX - canvasBoundingRect.left;
        this.mouse.y = event.pageY - canvasBoundingRect.top;
    }

    update(): void {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        super.update();
    }
}