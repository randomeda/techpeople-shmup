import DisplayObject from "./DisplayObject";
import Animation from "./Animation";
import { Events } from "./Ticker";
import { Poolable } from "./Pool";
import Container from "./Container";

export default class Explosion extends DisplayObject implements Poolable {
    public lineWidth: number;
    public radius: number;
    public active = true;

    private _anim: Animation;
    private _startRadius = 20;
    private _endRadius = 150;
    private _startLineWidth = 20;
    private _endLineWidth = 0;

    /**
     * Creates an animated explosion. A white cricle will grow and fade out quickly.
     * @param _container explosions are added to this container
     */
    constructor(private _container: Container) {
        super();

        this._anim = new Animation(
            0.2,
            this,
            { lineWidth: this._endLineWidth, radius: this._endRadius, opacity: 0 },
            { from: { lineWidth: this._startLineWidth, radius: this._startRadius, opacity: 1 } }
        );

        this._anim.on(Events.COMPLETE, () => this.reset());
        this._container.addChild(this);
    }

    reset() {
        this.active = false;
        this._anim.stop();
    }

    /**
     * Plays the explosion at the given coordinates
     * @param x starting position
     * @param y starting position
     */
    start(x: number, y: number) {
        this.x = x;
        this.y = y;
        this.active = true;
        this._anim.play();
    }

    destroy() {
        this._anim.removeAllListeners();
        this._anim.stop();
        this.remove();
    }

    update() {
        this.ctx.beginPath();
        this.ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI, false);
        this.ctx.lineWidth = this.lineWidth;
        this.ctx.strokeStyle = '#fff';
        this.ctx.globalAlpha = this.opacity;
        this.ctx.stroke();
    }
}