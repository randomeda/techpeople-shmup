import Grid from "./CollisionGrid";
import Container from "./Container";
import GameObject from "./GameObject";
import { Events } from "./Ticker";

export interface GridPosition {
    x: number,
    y: number
}

export default class CollisionDetector {
    grids: Grid[] = []; // instantiated grids placed here

    /**
     * Collision detection is costly for large number of objects. This grid based detection
     * will cause less calculation because objects will only be calculated if they are in
     * the same part of the screen. The size of these parts are determined by the size and
     * levels parameters.
     * @param levels number of grids. every higher level grid will have twice as alrge cells as the lower lever grid.
     * @param size base grid cell size in pixels. the first level will have this cell size. the second level will have twice a large, etc.
     * @param container Container object which the grid is applied to.
     */
    constructor(public levels: number, public size: number = 150, public container: Container) {
        // create grids
        for (let i = 0; i < levels; i++) {
            // create grid
            let parent = i > 0 ? this.grids[i - 1] : null;
            this.grids.push(new Grid(size * Math.pow(2, i), parent));
        }
    }

    /**
     * Calculates objects to the grids, then checks for collisions.
     */
    update() {
        this.clear();
        let objects = this.container.children;
        for (let i = 0; i < objects.length; i++) {
            let go: GameObject;
            go = <GameObject>objects[i];
            this.calculate(go);
        }
        this.checkCollisions();
    }

    /**
     * Calculates the corners of the object and registers it to the appropriate grid cells;
     * @param go object to be calculated
     */
    calculate(go: GameObject) {
        let bounds = go.bounds;
        let currentGrid = this.grids[0];
        let size = this.grids[0].size;

        // CALCULATE GRID LEVEL
        // if grater than current grid, we switch to a greater grid (if exists)
        while ((bounds.width > size || bounds.height > size) && currentGrid.parent) {
            currentGrid = currentGrid.parent;
            size = currentGrid.size;
        }
        go.grid = currentGrid;

        // CALCULATE CELLS IN LEVEL GRID
        let left = Math.floor(go.x / currentGrid.size); // divide by grid size to make grid cells
        let right = Math.floor((go.x + bounds.width) / currentGrid.size);
        let top = Math.floor(go.y / currentGrid.size);
        let bottom = Math.floor((go.y + bounds.height) / currentGrid.size);
        let topLeft = { x: left, y: top };
        let topRight = { x: right, y: top };
        let bottomLeft = { x: left, y: bottom };
        let bottomRight = { x: right, y: bottom };

        go.positions = [];
        this.pushIfNotExists(go.positions, topLeft);
        this.pushIfNotExists(go.positions, topRight);
        this.pushIfNotExists(go.positions, bottomLeft);
        this.pushIfNotExists(go.positions, bottomRight);

        // PUT GameObject TO GRID
        currentGrid.addToCells(go);
    }

    /**
     * Puts the value in the array only if it does not exist.
     * This makes sure that if two or more corners of the bounding box are in the same grid cell,
     * then the object is only registered once in the cell, not twice or more.
     * @param array 
     * @param value 
     */
    pushIfNotExists(array: GridPosition[], value: GridPosition) {
        for (let i = 0; i < array.length; i++) {
            if (array[i].x === value.x && array[i].y === value.y) {
                return;
            }
        }
        array.push(value);
    }

    /**
     * Clear all grids of objects
     */
    clear() {
        let currentGrid = this.grids[0];
        currentGrid.clear();
        while (currentGrid.parent) {
            currentGrid = currentGrid.parent;
            currentGrid.clear();
        }
    }

    /**
     * Checks collisions for all objects
     */
    checkCollisions(): void {
        let children = this.container.children;
        for (let go of this.container.children) {
            if (go instanceof GameObject) {
                for (let position of go.positions) {
                    for (let mayCollideWith of go.grid.getObjectsInCell(position)) {
                        if (mayCollideWith.constructor !== go.constructor) {
                            this.testCollision(go, mayCollideWith);
                        }
                    }
                }
            }
        }
    }

    /**
     * Tests if the two object's bounding box are overlapping
     * @param go1 One of the objects who's bounding box will be tested
     * @param go2 The other object which is tested
     */
    testCollision(go1: GameObject, go2: GameObject) {
        if (
            go2.x + go2.bounds.width >= go1.x && // go2 right greater than go1 left
            go2.x <= go1.x + go1.bounds.width && // go2 left less than go1 right
            go2.y + go2.bounds.height >= go1.y && // go2 bottom greater than go1 top
            go2.y <= go1.y + go1.bounds.height // go2 top less than go1 bottom
        ) {
            go2.emit(Events.COLLISION, go1);
            go1.emit(Events.COLLISION, go2);
        }
    }

}
